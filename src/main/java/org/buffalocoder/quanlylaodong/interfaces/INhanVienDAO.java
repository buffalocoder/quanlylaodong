package org.buffalocoder.quanlylaodong.interfaces;

import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.types.TrangThaiNhanVienEnum;

public interface INhanVienDAO extends IDataAccessObject<NhanVien> {
    /**
     * Phương thức cập nhật mật khẩu cho nhân viên
     *
     * @param id         String
     * @param matKhauMoi String
     * @return boolean
     */
    boolean capNhatMatKhau(String id, String matKhauCu, String matKhauMoi) throws Exception;

    /**
     * Phương thức tìm nhân viên theo tên
     *
     * @param hoTen       String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findByHoTen(String hoTen, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm danh sách nhân viên theo trạng thái nhân viên có phân trang
     *
     * @param trangThaiNhanVien TrangThaiNhanVienEnum
     * @param page              int
     * @param itemPerPage       int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findByTinhTrang(TrangThaiNhanVienEnum trangThaiNhanVien, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm nhân viên theo chứng minh nhân dân
     *
     * @param cmnd        String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findByCMND(String cmnd, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm nhân viên theo số điện thoại
     *
     * @param soDienThoai String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findBySoDienThoai(String soDienThoai, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức lấy tổng số nhân viên của công trình
     *
     * @return int
     * @throws DAOException
     */
    int thongKeTongSoNhanVienCuaCongTrinh(String maCongTrinh) throws DAOException;

    /**
     * Phương thức lấy tổng số nhân viên xây dựng của công trình
     *
     * @return int
     * @throws DAOException
     */
    int thongKeLoaiNhanVienCuaCongTrinh(String maCongTrinh, String maPB) throws DAOException;
}
