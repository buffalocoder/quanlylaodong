package org.buffalocoder.quanlylaodong.interfaces;

import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.entities.PhanCong;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;

import java.util.List;

public interface IPhanCongDAO extends IDataAccessObject<PhanCong> {
    /**
     * Phương thức lấy danh sách nhân viên chưa tham gia công trình
     *
     * @param maCongTrinh String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findDanhSachChuaThamGiaCongTrinh(String maCongTrinh, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức lấy danh sách nhân viên đã tham gia công trình
     *
     * @param maCongTrinh String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findDanhSachDaThamGiaCongTrinh(String maCongTrinh, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức lấy danh sách nhân viên đã tham gia công trình
     *
     * @param maCongTrinh String
     * @return List<NhanVien>
     * @throws DAOException
     */
    List<NhanVien> findDanhSachDaThamGiaCongTrinh(String maCongTrinh) throws DAOException;

    /**
     * Phương thức tìm danh sách công trình nhân viên đang tham gia
     *
     * @param maNhanVien  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<ThamGiaCongTrinh>
     * @throws DAOException
     */
    FindResult<PhanCong> findDanhSachCongTrinhNhanVienDangThamGia(String maNhanVien, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm danh sách nhân viên chưa tham gia công trình trong phòng ban
     *
     * @param maCongTrinh String
     * @param maCongViec  String
     * @param maPhongBan  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<PhanCong>
     * @throws DAOException
     */
    FindResult<NhanVien> findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(String maCongTrinh, String maCongViec, String maPhongBan, int page, int itemPerPage)
            throws DAOException;

    /**
     * Phương thức tìm danh sách nhân viên đã tham gia công trình với công việc X
     *
     * @param maCongTrinh String
     * @param maCongViec  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findDanhSachDaThamGiaCongTrinhByCongViec(String maCongTrinh, String maCongViec, int page, int itemPerPage)
            throws DAOException;

    /**
     * Phương thức xóa phân công
     *
     * @param maCongViec String
     * @param maNhanVien String
     * @return boolean
     * @throws DAOException
     */
    boolean deleteByMaCongViecMaNhanVien(String maCongViec, String maNhanVien) throws DAOException;

    /**
     * Phương thức xóa phân công
     *
     * @param maNhanVien
     * @param maCongTrinh
     * @return boolean
     */
    boolean deleteByMaNhanVienMaCongTrinh(String maNhanVien, String maCongTrinh);

    /**
     * Phương thức tìm phân công theo mã công việc, mã nhân viên
     *
     * @param maCongViec String
     * @param maNhanVien String
     * @return PhanCong
     * @throws DAOException
     */
    PhanCong findByMaCongViecMaNhanVien(String maCongViec, String maNhanVien) throws DAOException;

    /**
     * Phương thức tìm danh sách tham gia công trình
     *
     * @param maCongTrinh String
     * @param maCongViec  String
     * @return List<PhanCong>
     * @throws DAOException
     */
    List<PhanCong> findDanhSachThamGiaCongTrinh(String maCongTrinh, String maCongViec) throws DAOException;
}
