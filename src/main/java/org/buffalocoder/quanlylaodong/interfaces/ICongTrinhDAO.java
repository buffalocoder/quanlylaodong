package org.buffalocoder.quanlylaodong.interfaces;

import org.buffalocoder.quanlylaodong.entities.CongTrinh;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.types.LoaiCongTrinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiCongTrinh;

import java.util.List;

public interface ICongTrinhDAO extends IDataAccessObject<CongTrinh> {
    /**
     * Phương thức lấy tất cả công trình
     *
     * @return List<CongTrinh>
     * @throws DAOException
     */
    List<CongTrinh> findAll() throws DAOException;

    /**
     * Phương thức tìm công trình theo tên công trình
     *
     * @param tenCongTrinh String
     * @param page         int
     * @param itemPerPage  int
     * @return FindResult<CongTrinh>
     */
    FindResult<CongTrinh> findByTenCongTrinh(String tenCongTrinh, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm công trình theo trạng thái công trình
     *
     * @param trangThaiCongTrinh {@link TrangThaiCongTrinh}
     * @param page               int
     * @param itemPerPage        int
     * @return FindResult<CongTrinh>
     * @throws DAOException
     */
    FindResult<CongTrinh> findByTrangThai(TrangThaiCongTrinh trangThaiCongTrinh, int page, int itemPerPage) throws DAOException;

    /**
     * Tìm công trình theo loại công trình
     *
     * @param loaiCongTrinh {@link LoaiCongTrinhEnum}
     * @param page          int
     * @param itemPerPage   int
     * @return FindResult<CongTrinh>
     * @throws DAOException
     */
    FindResult<CongTrinh> findByLoaiCongTrinh(LoaiCongTrinhEnum loaiCongTrinh, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm công trình trễ hạn
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<CongTrinh>
     * @throws DAOException
     */
    FindResult<CongTrinh> findDanhSachCongTrinhTreHan(int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức thống kê theo trạng thái công trình
     *
     * @param trangThaiCongTrinh {@link TrangThaiCongTrinh}
     * @return int
     * @throws DAOException
     */
    int thongKeTheoTrangThai(TrangThaiCongTrinh trangThaiCongTrinh) throws DAOException;

    /**
     * Phương thức thống kê tổng số lượng công trình
     *
     * @return int
     * @throws DAOException
     */
    int thongKeTongSoLuongCongTrinh() throws DAOException;

    /**
     * Phương thức thống kê số lượng công trình trễ hạn
     *
     * @return int
     * @throws DAOException
     */
    int thongKeCongTrinhTreHan() throws DAOException;

    /**
     * Phương thức thống kê số lượng công trình hoàn thành
     *
     * @return int
     * @throws DAOException
     */
    int thongKeCongTrinhHoanThanh() throws DAOException;

    /**
     * Phương thức thống kê số lượng công trình đang thi công
     *
     * @return int
     * @throws DAOException
     */
    int thongKeCongTrinhDangThiCong() throws DAOException;

    /**
     * Phương thức lấy tổng chi phí của công trình
     *
     * @return double
     * @throws DAOException
     */
    double thongKeChiPhiCongTrinh(String maCongTrinh) throws DAOException;
}
