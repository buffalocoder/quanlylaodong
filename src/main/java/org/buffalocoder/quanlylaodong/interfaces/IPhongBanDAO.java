package org.buffalocoder.quanlylaodong.interfaces;

import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.entities.PhongBan;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;

import java.util.List;

public interface IPhongBanDAO extends IDataAccessObject<PhongBan> {
    /**
     * Phương thức lấy tất cả phòng ban
     *
     * @return List<PhongBan>
     * @throws DAOException
     */
    List<PhongBan> findAll() throws DAOException;

    /**
     * Phương thức tìm danh sách nhân viên trong phòng ban
     *
     * @param maPhongBan  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    FindResult<NhanVien> findDanhSachNhanVienPhongBan(String maPhongBan, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm danh sách phòng ban bằng tên
     *
     * @param tenPhongBan String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<PhongBan>
     * @throws DAOException
     */
    FindResult<PhongBan> findByTenPhongBan(String tenPhongBan, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm danh sách phòng ban bằng số điện thoại
     *
     * @param soDienThoai String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<PhongBan>
     * @throws DAOException
     */
    FindResult<PhongBan> findBySoDienThoai(String soDienThoai, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức đếm tổng số nhân viên
     *
     * @param maPhongBan String
     * @return int
     * @throws DAOException
     */
    int tongSoNhanVien(String maPhongBan) throws DAOException;

    /**
     * Phương thức đếm tổng số nhân viên theo giới tính
     *
     * @param maPhongBan String
     * @param gioiTinh   boolean
     * @return int
     * @throws DAOException
     */
    int tongSoNhanVienTheoGioiTinh(String maPhongBan, boolean gioiTinh) throws DAOException;
}
