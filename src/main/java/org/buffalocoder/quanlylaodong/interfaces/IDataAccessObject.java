package org.buffalocoder.quanlylaodong.interfaces;

import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;

/**
 * Interface Data Access Object
 * Các phương thức CRUD cơ bản
 *
 * @param <T>
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 05-10-2019
 */
public interface IDataAccessObject<T> {
    /**
     * Phương thức tạo id mới
     *
     * @return String
     */
    String generateId();

    /**
     * Phương thức thêm 1 object
     *
     * @param t
     * @return boolean
     * @throws DAOException
     */
    boolean add(T t) throws DAOException;

    /**
     * Phương thức xóa 1 object bằng id
     *
     * @param id
     * @return boolean
     * @throws DAOException
     */
    boolean deleteById(Object id) throws DAOException;

    /**
     * Phương thức xóa 1 object
     *
     * @param t
     * @return boolean
     * @throws DAOException
     */
    boolean delete(T t) throws DAOException;

    /**
     * Phương thức update 1 object
     *
     * @param t
     * @return T
     * @throws DAOException
     */
    T update(T t) throws DAOException;

    /**
     * Phương thức tìm 1 object bằng mã
     *
     * @param id
     * @return T
     * @throws NotExistException
     * @throws DAOException
     */
    T find(Object id) throws NotExistException, DAOException;

    /**
     * Phương thức tìm tất cả theo trang
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<T>
     * @throws DAOException
     */
    FindResult<T> findAll(int page, int itemPerPage) throws DAOException;
}
