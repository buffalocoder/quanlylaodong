package org.buffalocoder.quanlylaodong.interfaces;

import org.buffalocoder.quanlylaodong.entities.CongViec;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;

import java.util.List;

public interface ICongViecDAO extends IDataAccessObject<CongViec> {
    /**
     * Phương thức lấy tất cả công việc
     *
     * @return List<CongViec>
     * @throws DAOException
     */
    List<CongViec> findAll() throws DAOException;

    /**
     * Phương thức tìm kiếm công việc theo công trình
     *
     * @param maCongTrinh String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<CongViec>
     * @throws DAOException
     */
    FindResult<CongViec> findByCongTrinh(String maCongTrinh, int page, int itemPerPage) throws DAOException;

    /**
     * Phương thức tìm kiếm công việc theo phòng ban
     *
     * @param maPhongBan  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<CongViec>
     * @throws DAOException
     */
    FindResult<CongViec> findByPhongBan(String maPhongBan, int page, int itemPerPage) throws DAOException;

    /**
     *
     */
    List<CongViec> findAllByCongTrinh(String maCongTrinh) throws DAOException;

    /**
     * Phương thức tính tổng chi phí của công trình theo công việc
     *
     * @param maCongTrinh String
     * @return double
     * @throws DAOException
     */
    double tinhTongChiPhiCongViec(String maCongTrinh) throws DAOException;
}
