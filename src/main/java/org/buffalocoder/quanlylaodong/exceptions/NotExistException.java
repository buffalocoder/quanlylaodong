package org.buffalocoder.quanlylaodong.exceptions;

/**
 * Bắt lỗi không tìm thấy obj
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 23-10-2019
 */
public class NotExistException extends Exception {
    public NotExistException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
