package org.buffalocoder.quanlylaodong.exceptions;

/**
 * Bắt lỗi ràng buộc dữ liệu
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 15-10-2019
 */
public class ValidateException extends Exception {
    public ValidateException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
