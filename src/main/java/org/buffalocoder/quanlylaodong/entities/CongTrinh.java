package org.buffalocoder.quanlylaodong.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;
import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.constant.TableNameConst;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.types.LoaiCongTrinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiCongTrinh;
import org.buffalocoder.quanlylaodong.utils.Validate;

import javax.persistence.*;
import java.sql.Date;

/**
 * Entity Công trình
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.2
 * @since : 05-10-2019
 */
@Entity
@Table(name = TableNameConst.CONG_TRINH)
@NamedNativeQueries({
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_LAST_ITEM,
                query = "SELECT TOP 1 * FROM " + TableNameConst.CONG_TRINH +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " DESC",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_ALL_NO_OFFSET,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH,
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_ALL,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_ALL_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_BY_TEN,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_TENCONGTRINH + " LIKE ?" +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_BY_TEN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_TENCONGTRINH + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_BY_TRANGTHAI,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_TRANGTHAI + " = ?" +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_BY_TRANGTHAI_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_TRANGTHAI + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_BY_LOAICONGTRINH,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_LOAICONGTRINH + " = ?" +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_BY_LOAICONGTRINH_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_LOAICONGTRINH + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHTREHAN,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_NGAYDUKIENHOANTHANH + " < GETDATE()" +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHTREHAN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_NGAYDUKIENHOANTHANH + " < GETDATE()"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHHOANTHANH,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_NGAYHOANTHANH + " IS NOT NULL" +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHHOANTHANH_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_NGAYHOANTHANH + " IS NOT NULL"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHDANGTHICONG,
                query = "SELECT * FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_NGAYHOANTHANH + " IS NULL AND " +
                        FieldNameTableConst.CONGTRINH_NGAYTHICONG + " IS NOT NULL " +
                        " ORDER BY " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongTrinh.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHDANGTHICONG_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_NGAYHOANTHANH + " IS NULL AND " +
                        FieldNameTableConst.CONGTRINH_NGAYTHICONG + " IS NOT NULL "
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_THONGKE_TINHTRANG_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.CONG_TRINH +
                        " WHERE " + FieldNameTableConst.CONGTRINH_TRANGTHAI + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGTRINH_THONGKE_TONGCHIPHI,
                query = "SELECT SUM(" + FieldNameTableConst.CONGVIEC_CHIPHI + ") FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?"
        )
})
public class CongTrinh extends RecursiveTreeObject<CongTrinh> {
    // region Properties
    @Id
    @Column(name = FieldNameTableConst.CONGTRINH_MACONGTRINH, columnDefinition = "VARCHAR(10)")
    private String maCongTrinh;

    @Column(name = FieldNameTableConst.CONGTRINH_NGAYCAPGIAYPHEP, columnDefinition = "DATETIME")
    private Date ngayCapGiayPhep;

    @Column(name = FieldNameTableConst.CONGTRINH_NGAYDUKIENHOANTHANH, columnDefinition = "DATETIME")
    private Date ngayDuKienHoanThanh;

    @Column(name = FieldNameTableConst.CONGTRINH_NGAYTHICONG, columnDefinition = "DATETIME")
    private Date ngayThiCong;

    @Column(name = FieldNameTableConst.CONGTRINH_NGAYHOANTHANH, columnDefinition = "DATETIME")
    private Date ngayHoanThanh;

    @Column(name = FieldNameTableConst.CONGTRINH_TENCONGTRINH, columnDefinition = "NVARCHAR(50)")
    private String tenCongTrinh;

    @Column(name = FieldNameTableConst.CONGTRINH_LOAICONGTRINH, columnDefinition = "INT")
    private LoaiCongTrinhEnum loaiCongTrinh;

    @Column(name = FieldNameTableConst.CONGTRINH_TRANGTHAI, columnDefinition = "INT")
    private TrangThaiCongTrinh trangThaiCongTrinh;

    @Column(name = FieldNameTableConst.CONGTRINH_DIACHI)
    @Embedded
    private DiaChi diaChi;
    // endregion

    // region Getter, Setter
    public Date getNgayHoanThanh() {
        return ngayHoanThanh;
    }

    public void setNgayHoanThanh(Date ngayHoanThanh) throws ValidateException {
        if (this.ngayDuKienHoanThanh == null && ngayHoanThanh != null) {
            throw new ValidateException("Vui lòng chọn ngày dự kiến hoàn thành trước khi nhập ngày hoàn thành");
        } else if (ngayHoanThanh != null && ngayHoanThanh.before(this.ngayDuKienHoanThanh)) {
            throw new ValidateException("Ngày hoàn thành phải sau hoặc bằng ngày dự kiến hoàn thành");
        } else if (ngayHoanThanh != null && (ngayHoanThanh.after(this.ngayDuKienHoanThanh) || (ngayHoanThanh.equals(this.ngayDuKienHoanThanh)))) {
            this.ngayHoanThanh = ngayHoanThanh;
        }
    }

    public LoaiCongTrinhEnum getLoaiCongTrinh() {
        return loaiCongTrinh;
    }

    public void setLoaiCongTrinh(LoaiCongTrinhEnum loaiCongTrinh) {
        this.loaiCongTrinh = loaiCongTrinh;
    }

    public String getMaCongTrinh() {
        return maCongTrinh;
    }

    public void setMaCongTrinh(String maCongTrinh) throws ValidateException {
        if (Validate.isPatternId(maCongTrinh, PatternIdConst.PREFIX_CONG_TRINH)) {
            this.maCongTrinh = maCongTrinh;
        } else throw new ValidateException("Mã công trình không đúng định dạng");
    }

    public Date getNgayCapGiayPhep() {
        return ngayCapGiayPhep;
    }

    public void setNgayCapGiayPhep(Date ngayCapGiayPhep) {
        if (ngayCapGiayPhep != null) {
            this.ngayCapGiayPhep = ngayCapGiayPhep;
        }
    }

    public Date getNgayDuKienHoanThanh() {
        return ngayDuKienHoanThanh;
    }

    public void setNgayDuKienHoanThanh(Date ngayDuKienHoanThanh) throws ValidateException {
        if (this.ngayThiCong == null && ngayDuKienHoanThanh != null) {
            throw new ValidateException("Vui lòng chọn ngày thi công trước khi nhập ngày dự kiến hoàn thành");
        } else if (ngayDuKienHoanThanh != null && ngayDuKienHoanThanh.before(this.ngayThiCong)) {
            throw new ValidateException("Ngày dự kiến hoàn thành phải sau ngày thi công");
        } else if (ngayDuKienHoanThanh != null && ngayDuKienHoanThanh.after(this.ngayThiCong)) {
            this.ngayDuKienHoanThanh = ngayDuKienHoanThanh;
        }
    }

    public Date getNgayThiCong() {
        return ngayThiCong;
    }

    public void setNgayThiCong(Date ngayThiCong) throws ValidateException {
        if (this.ngayCapGiayPhep == null && ngayThiCong != null) {
            throw new ValidateException("Vui lòng chọn ngày cấp giấy phép trước khi nhập ngày thi công");
        } else if (ngayThiCong != null && ngayThiCong.before(this.ngayCapGiayPhep)) {
            throw new ValidateException("Ngày thi công phải sau ngày cấp giấy phép");
        } else if (ngayThiCong != null && ngayThiCong.after(this.ngayCapGiayPhep)) {
            this.ngayThiCong = ngayThiCong;
        }
    }

    public String getTenCongTrinh() {
        return tenCongTrinh;
    }

    public void setTenCongTrinh(String tenCongTrinh) throws ValidateException {
        if (tenCongTrinh.trim().length() >= 5) {
            this.tenCongTrinh = tenCongTrinh;
        } else throw new ValidateException("Tên công trình phải lớn hơn 5 kí tự");
    }

    public TrangThaiCongTrinh getTrangThaiCongTrinh() {
        return trangThaiCongTrinh;
    }

    public void setTrangThaiCongTrinh(TrangThaiCongTrinh trangThaiCongTrinh) {
        this.trangThaiCongTrinh = trangThaiCongTrinh;
    }

    public DiaChi getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(DiaChi diaChi) {
        this.diaChi = diaChi;
    }
    // endregion

    // region Constructor
    public CongTrinh() {
    }

    public CongTrinh(String maCongTrinh, String tenCongTrinh, Date ngayCapGiayPhep, Date ngayThiCong,
                     Date ngayDuKienHoanThanh, Date ngayHoanThanh, LoaiCongTrinhEnum loaiCongTrinh, TrangThaiCongTrinh trangThaiCongTrinh,
                     DiaChi diaChi) throws ValidateException {
        this.setMaCongTrinh(maCongTrinh);
        this.setNgayCapGiayPhep(ngayCapGiayPhep);
        this.setNgayThiCong(ngayThiCong);
        this.setNgayDuKienHoanThanh(ngayDuKienHoanThanh);
        this.setTenCongTrinh(tenCongTrinh);
        this.setLoaiCongTrinh(loaiCongTrinh);
        this.setDiaChi(diaChi);
        this.setTrangThaiCongTrinh(trangThaiCongTrinh);
        this.setNgayHoanThanh(ngayHoanThanh);
    }

    public CongTrinh(String maCongTrinh, String tenCongTrinh, Date ngayCapGiayPhep, Date ngayThiCong,
                     Date ngayDuKienHoanThanh, LoaiCongTrinhEnum loaiCongTrinh, TrangThaiCongTrinh trangThaiCongTrinh, DiaChi diaChi) throws ValidateException {
        this.setMaCongTrinh(maCongTrinh);
        this.setNgayCapGiayPhep(ngayCapGiayPhep);
        this.setNgayThiCong(ngayThiCong);
        this.setNgayDuKienHoanThanh(ngayDuKienHoanThanh);
        this.setTenCongTrinh(tenCongTrinh);
        this.setLoaiCongTrinh(loaiCongTrinh);
        this.setTrangThaiCongTrinh(trangThaiCongTrinh);
        this.setDiaChi(diaChi);
    }

    public CongTrinh(String maCongTrinh, String tenCongTrinh, LoaiCongTrinhEnum loaiCongTrinh, TrangThaiCongTrinh trangThaiCongTrinh, DiaChi diaChi) throws ValidateException {
        this.setMaCongTrinh(maCongTrinh);
        this.setTenCongTrinh(tenCongTrinh);
        this.setLoaiCongTrinh(loaiCongTrinh);
        this.setTrangThaiCongTrinh(trangThaiCongTrinh);
        this.setDiaChi(diaChi);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CongTrinh congTrinh = (CongTrinh) o;

        return maCongTrinh != null ? maCongTrinh.equals(congTrinh.maCongTrinh) : congTrinh.maCongTrinh == null;
    }

    @Override
    public int hashCode() {
        return maCongTrinh != null ? maCongTrinh.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "CongTrinh{" +
                "maCongTrinh='" + maCongTrinh + '\'' +
                ", ngayCapGiayPhep=" + ngayCapGiayPhep +
                ", ngayDuKienHoanThanh=" + ngayDuKienHoanThanh +
                ", ngayThiCong=" + ngayThiCong +
                ", ngayHoanThanh=" + ngayHoanThanh +
                ", tenCongTrinh='" + tenCongTrinh + '\'' +
                ", loaiCongTrinh=" + loaiCongTrinh +
                ", trangThaiCongTrinh=" + trangThaiCongTrinh +
                ", diaChi=" + diaChi +
                '}';
    }
    // endregion
}
