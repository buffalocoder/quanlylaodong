package org.buffalocoder.quanlylaodong.entities;

import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;

import javax.persistence.Column;
import javax.persistence.Embeddable;


//test push
/**
 * Entity Công trình
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.1
 * @since : 23-10-2019
 */
@Embeddable
public class DiaChi {
    // region Properties
    @Column(name = FieldNameTableConst.DIACHI_DIACHICHITIET, columnDefinition = "NVARCHAR(100)")
    private String diaChiChiTiet;

    @Column(name = FieldNameTableConst.DIACHI_KHUVUC, columnDefinition = "NVARCHAR(50)")
    private String khuVuc;

    @Column(name = FieldNameTableConst.DIACHI_QUANHUYEN, columnDefinition = "NVARCHAR(50)")
    private String quanHuyen;

    @Column(name = FieldNameTableConst.DIACHI_TINHTHANHPHO, columnDefinition = "NVARCHAR(50)")
    private String tinhThanhPho;
    // endregion

    // region Getter, Setter
    public String getDiaChiChiTiet() {
        return diaChiChiTiet;
    }

    public void setDiaChiChiTiet(String diaChiChiTiet) {
        this.diaChiChiTiet = diaChiChiTiet;
    }

    public String getKhuVuc() {
        return khuVuc;
    }

    public void setKhuVuc(String khuVuc) {
        this.khuVuc = khuVuc;
    }

    public String getQuanHuyen() {
        return quanHuyen;
    }

    public void setQuanHuyen(String quanHuyen) {
        this.quanHuyen = quanHuyen;
    }

    public String getTinhThanhPho() {
        return tinhThanhPho;
    }

    public void setTinhThanhPho(String tinhThanhPho) {
        this.tinhThanhPho = tinhThanhPho;
    }

    public String getDiaChi() {
        return String.format("%s, %s, %s, %s",
                getDiaChiChiTiet(),
                getKhuVuc(),
                getQuanHuyen(),
                getTinhThanhPho()).trim();
    }
    // endregion

    // region Constructors
    public DiaChi(String diaChiChiTiet, String khuVuc, String quanHuyen, String tinhThanhPho) {
        this.diaChiChiTiet = diaChiChiTiet;
        this.khuVuc = khuVuc;
        this.quanHuyen = quanHuyen;
        this.tinhThanhPho = tinhThanhPho;
    }

    public DiaChi() {
    }
    // endregion

    // region Override method
    @Override
    public String toString() {
        return "DiaChi{" +
                "diaChiChiTiet='" + diaChiChiTiet + '\'' +
                ", khuVuc='" + khuVuc + '\'' +
                ", quanHuyen='" + quanHuyen + '\'' +
                ", tinhThanhPho='" + tinhThanhPho + '\'' +
                '}';
    }
    // endregion
}
