package org.buffalocoder.quanlylaodong.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;
import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.constant.TableNameConst;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.types.GioiTinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiNhanVienEnum;
import org.buffalocoder.quanlylaodong.utils.Validate;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import java.util.Locale;

/**
 * Entity Nhân viên
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.2
 * @since : 05-10-2019
 */
@Entity
@Table(name = TableNameConst.NHAN_VIEN)
@NamedNativeQueries({
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_LAST_ITEM,
                query = "SELECT TOP 1 * FROM " + TableNameConst.NHAN_VIEN +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " DESC",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_ALL,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_ALL_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_HOTEN,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_HOTEN + " LIKE ?" +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_HOTEN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_HOTEN + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_CMND,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_CMND + " LIKE ?" +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_CMND_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_CMND + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_SDT,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_SODIENTHOAI + " LIKE ?" +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_SDT_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_SODIENTHOAI + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_TRANGTHAI,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_TRANGTHAI + " = ?" +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_BY_TRANGTHAI_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.NHANVIEN_TRANGTHAI + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_ALL_OF_CONGTRINH,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN + " NV " +
                        " JOIN " + TableNameConst.BANG_PHAN_CONG + " BPC ON BPC." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " = NV." + FieldNameTableConst.NHANVIEN_MANHANVIEN + " JOIN " +
                        TableNameConst.BANG_CONG_VIEC + " BCV ON BCV." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " WHERE " +
                        FieldNameTableConst.CONGTRINH_MACONGTRINH + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.NHANVIEN_FIND_ALL_XAYDUNG_OF_CONGTRINH,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN + " NV " +
                        " JOIN " + TableNameConst.BANG_PHAN_CONG + " BPC ON BPC." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " = NV." + FieldNameTableConst.NHANVIEN_MANHANVIEN + " JOIN " +
                        TableNameConst.BANG_CONG_VIEC + " BCV ON BCV." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " WHERE " +
                        FieldNameTableConst.CONGTRINH_MACONGTRINH + " LIKE ? AND BCV." +
                        FieldNameTableConst.PHONGBAN_MAPHONGBAN + " LIKE ?"
        )
})
public class NhanVien extends RecursiveTreeObject<NhanVien> {
    // region Properties
    @Id
    @Column(name = FieldNameTableConst.NHANVIEN_MANHANVIEN, columnDefinition = "VARCHAR(10)")
    private String maNhanVien;

    @Column(name = FieldNameTableConst.NHANVIEN_ANHCANHAN, columnDefinition = "VARCHAR(50)")
    private String anhCaNhan;

    @Column(name = FieldNameTableConst.NHANVIEN_CMND, columnDefinition = "VARCHAR(11)")
    private String cmnd;

    @Column(name = FieldNameTableConst.NHANVIEN_DANTOC, columnDefinition = "NVARCHAR(30)")
    private String danToc;

    @Column(name = FieldNameTableConst.NHANVIEN_TRANGTHAI)
    private TrangThaiNhanVienEnum trangThai;

    /**
     * false = Nam
     * true = Nu
     */
    @Column(name = FieldNameTableConst.NHANVIEN_GIOITINH, columnDefinition = "BIT")
    private GioiTinhEnum gioiTinh;

    @Column(name = FieldNameTableConst.NHANVIEN_HOTEN, columnDefinition = "NVARCHAR(50)")
    private String hoTen;

    @Column(name = FieldNameTableConst.NHANVIEN_MATKHAU, columnDefinition = "VARCHAR(200)")
    private String matKhau;

    @Column(name = FieldNameTableConst.NHANVIEN_NGAYSINH, columnDefinition = "DATETIME")
    private Date ngaySinh;

    @Column(name = FieldNameTableConst.NHANVIEN_SODIENTHOAI, columnDefinition = "VARCHAR(10)")
    private String soDienThoai;

    @Column(name = FieldNameTableConst.NHANVIEN_DIACHI)
    @Embedded
    private DiaChi diaChi;

    @ManyToOne
    @JoinColumn(name = FieldNameTableConst.PHONGBAN_MAPHONGBAN)
    private PhongBan phongBan;
    // endregion

    // region Getter, Setter
    public PhongBan getPhongBan() {
        return phongBan;
    }

    public void setPhongBan(PhongBan phongBan) {
        this.phongBan = phongBan;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) throws ValidateException {
        if (Validate.isPatternId(maNhanVien, PatternIdConst.PREFIX_NHAN_VIEN)) {
            this.maNhanVien = maNhanVien;
        } else throw new ValidateException("Mã nhân viên không đúng định dạng");
    }

    public String getAnhCaNhan() {
        return anhCaNhan;
    }

    public void setAnhCaNhan(String anhCaNhan) {
        this.anhCaNhan = anhCaNhan;
    }

    public String getCmnd() {
        return cmnd;
    }

    public void setCmnd(String cmnd) throws ValidateException {
        if (Validate.isCMND(cmnd)) {
            this.cmnd = cmnd;
        } else throw new ValidateException("CMND không đúng định dạng");
    }

    public String getDanToc() {
        return danToc;
    }

    public void setDanToc(String danToc) {
        this.danToc = danToc;
    }

    public String isGioiTinh() {
        if (gioiTinh == GioiTinhEnum.NU) {
            return "Nữ";
        } else if (gioiTinh == GioiTinhEnum.NAM) {
            return "Nam";
        } else return "Không xác định";
    }

    public void setGioiTinh(GioiTinhEnum gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) throws ValidateException {
        if (hoTen.length() >= 5) {
            this.hoTen = hoTen.toUpperCase(new Locale("vi"));
        } else throw new ValidateException("Tên phải lớn hơn 5 kí tự");
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) throws ValidateException {
        if (matKhau.length() >= 8) {
            this.matKhau = BCrypt.hashpw(matKhau, BCrypt.gensalt(12));
        } else throw new ValidateException("Mật khẩu phải lớn hơn hoặc bằng 8 kí tự");
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) throws ValidateException {
        LocalDate currentDate = LocalDate.now();
        Date minNgaySinh = Date.valueOf(LocalDate.of(currentDate.getYear() - 60, Month.JANUARY, 1));
        Date maxNgaySinh = Date.valueOf(LocalDate.of(currentDate.getYear() - 18, Month.DECEMBER, 1));

        if (ngaySinh.before(maxNgaySinh) && ngaySinh.after(minNgaySinh)) {
            this.ngaySinh = ngaySinh;
        } else throw new ValidateException(String.format("Ngày sinh phải sau ngày %s và trước ngày %s",
                minNgaySinh.toString(), maxNgaySinh.toString()));
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) throws ValidateException {
        if (Validate.isSoDienThoai(soDienThoai)) {
            this.soDienThoai = soDienThoai;
        } else throw new ValidateException("Số điện thoại phải là 10 kí tự và bắt đầu là số 0");
    }

    public DiaChi getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(DiaChi diaChi) {
        this.diaChi = diaChi;
    }

    public TrangThaiNhanVienEnum getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(TrangThaiNhanVienEnum trangThai) {
        this.trangThai = trangThai;
    }

    public GioiTinhEnum getGioiTinh() {
        return gioiTinh;
    }
    // endregion

    // region Constructor
    public NhanVien() {
    }

    public NhanVien(String maNhanVien, String anhCaNhan, String cmnd, String danToc, GioiTinhEnum gioiTinh,
                    String hoTen, String matKhau, Date ngaySinh, String soDienThoai, DiaChi diaChi,
                    TrangThaiNhanVienEnum trangThai, PhongBan phongBan)
            throws ValidateException {
        this.setMaNhanVien(maNhanVien);
        this.setAnhCaNhan(anhCaNhan);
        this.setCmnd(cmnd);
        this.setDanToc(danToc);
        this.setGioiTinh(gioiTinh);
        this.setHoTen(hoTen);
        this.setMatKhau(matKhau);
        this.setNgaySinh(ngaySinh);
        this.setSoDienThoai(soDienThoai);
        this.setDiaChi(diaChi);
        this.setTrangThai(trangThai);
        this.setPhongBan(phongBan);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NhanVien nhanVien = (NhanVien) o;

        return maNhanVien != null ? maNhanVien.equals(nhanVien.maNhanVien) : nhanVien.maNhanVien == null;
    }

    @Override
    public int hashCode() {
        return maNhanVien != null ? maNhanVien.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "NhanVien{" +
                "maNhanVien='" + maNhanVien + '\'' +
                ", anhCaNhan='" + anhCaNhan + '\'' +
                ", cmnd='" + cmnd + '\'' +
                ", danToc='" + danToc + '\'' +
                ", trangThai=" + trangThai +
                ", gioiTinh=" + gioiTinh +
                ", hoTen='" + hoTen + '\'' +
                ", matKhau='" + matKhau + '\'' +
                ", ngaySinh=" + ngaySinh +
                ", soDienThoai='" + soDienThoai + '\'' +
                ", diaChi=" + diaChi +
                ", phongBan=" + phongBan +
                '}';
    }
    // endregion

    // region Methods
    public boolean checkPassword(String matKhau) {
        return BCrypt.checkpw(matKhau, this.getMatKhau());
    }
    // endregion
}
