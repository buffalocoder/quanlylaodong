package org.buffalocoder.quanlylaodong.entities;

import java.util.List;

/**
 * Entity FindResult<T>
 * Dùng cho tìm kiếm phân trang
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.2
 * @since : 05-10-2019
 */
public class FindResult<T> {
    // region Properties
    private List<T> listResult;
    private int totalPage;
    // endregion

    // region Getter, Setter
    public List<T> getListResult() {
        return listResult;
    }

    public void setListResult(List<T> listResult) {
        this.listResult = listResult;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
    // endregion

    // region Constructor
    public FindResult(List<T> listResult, int totalPage) {
        this.listResult = listResult;
        this.totalPage = totalPage;
    }
    // endregion

    // region Override method
    @Override
    public String toString() {
        return "FindResult{" +
                "listResult=" + listResult +
                ", totalPage=" + totalPage +
                '}';
    }
    // endregion
}
