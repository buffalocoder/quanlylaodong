package org.buffalocoder.quanlylaodong.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;
import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.constant.TableNameConst;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.utils.Validate;

import javax.persistence.*;

/**
 * Entity Phòng ban
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.1
 * @since : 05-10-2019
 */
@Entity
@Table(name = TableNameConst.PHONG_BAN)
@NamedNativeQueries({
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_LAST_ITEM,
                query = "SELECT TOP 1 * FROM " + TableNameConst.PHONG_BAN +
                        " ORDER BY " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " DESC",
                resultClass = PhongBan.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_ALL_NO_OFFSET,
                query = "SELECT * FROM " + TableNameConst.PHONG_BAN,
                resultClass = PhongBan.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_ALL,
                query = "SELECT * FROM " + TableNameConst.PHONG_BAN +
                        " ORDER BY " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = PhongBan.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_ALL_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.PHONG_BAN
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0" +
                        " ORDER BY " + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_BY_TEN,
                query = "SELECT * FROM " + TableNameConst.PHONG_BAN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_TENPHONGBAN + " LIKE ?" +
                        " ORDER BY " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = PhongBan.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_BY_TEN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.PHONG_BAN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_TENPHONGBAN + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_BY_SODIENTHOAI,
                query = "SELECT * FROM " + TableNameConst.PHONG_BAN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_SODIENTHOAI + " LIKE ?" +
                        " ORDER BY " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = PhongBan.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_BY_SODIENTHOAI_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.PHONG_BAN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_SODIENTHOAI + " LIKE ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_COUNT_ALL,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_GIOITINH_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN +
                        " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_GIOITINH + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0"
        )
})
public class PhongBan extends RecursiveTreeObject<PhongBan> {
    // region Properties
    @Id
    @Column(name = FieldNameTableConst.PHONGBAN_MAPHONGBAN, columnDefinition = "VARCHAR(10)")
    private String maPhongBan;

    @Column(name = FieldNameTableConst.PHONGBAN_TENPHONGBAN, columnDefinition = "NVARCHAR(50)")
    private String tenPhongBan;

    @Column(name = FieldNameTableConst.PHONGBAN_MOTA, columnDefinition = "NVARCHAR(100)")
    private String moTa;

    @Column(name = FieldNameTableConst.PHONGBAN_SODIENTHOAI, columnDefinition = "VARCHAR(10)")
    private String soDienThoai;
    // endregion

    // region Getter, Setter
    public String getMaPhongBan() {
        return maPhongBan;
    }

    public void setMaPhongBan(String maPhongBan) throws ValidateException {
        if (maPhongBan.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mã phòng ban");
        } else if (Validate.isPatternId(maPhongBan, PatternIdConst.PREFIX_PHONG_BAN)) {
            this.maPhongBan = maPhongBan;
        } else throw new ValidateException("Mã phòng ban không đúng định dạng");
    }

    public String getTenPhongBan() {
        return tenPhongBan;
    }

    public void setTenPhongBan(String tenPhongBan) throws ValidateException {
        if (tenPhongBan.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập tên phòng ban");
        } else if (tenPhongBan.length() >= 5) {
            this.tenPhongBan = tenPhongBan;
        } else throw new ValidateException("Tên phòng ban phải lớn hơn 5 kí tự");
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) throws ValidateException {
        if (soDienThoai.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập số điện thoại");
        } else if (Validate.isSoDienThoai(soDienThoai)) {
            this.soDienThoai = soDienThoai;
        } else throw new ValidateException("Số điện thoại phải là 10 kí tự và bắt đầu là số 0");
    }
    // endregion

    // region Constructor
    public PhongBan() {
    }

    public PhongBan(String maPhongBan, String tenPhongBan, String moTa, String soDienThoai) throws ValidateException {
        this.setMaPhongBan(maPhongBan);
        this.setTenPhongBan(tenPhongBan);
        this.setMoTa(moTa);
        this.setSoDienThoai(soDienThoai);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhongBan phongBan = (PhongBan) o;

        return maPhongBan != null ? maPhongBan.equals(phongBan.maPhongBan) : phongBan.maPhongBan == null;
    }

    @Override
    public int hashCode() {
        return maPhongBan != null ? maPhongBan.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PhongBan{" +
                "maPhongBan='" + maPhongBan + '\'' +
                ", tenPhongBan='" + tenPhongBan + '\'' +
                ", moTa='" + moTa + '\'' +
                ", soDienThoai='" + soDienThoai + '\'' +
                '}';
    }
    // endregion
}
