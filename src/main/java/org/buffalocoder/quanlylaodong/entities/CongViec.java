package org.buffalocoder.quanlylaodong.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;
import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.constant.TableNameConst;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.utils.Validate;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = TableNameConst.BANG_CONG_VIEC)
@NamedNativeQueries({
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_LAST_ITEM,
                query = "SELECT TOP 1 * FROM " + TableNameConst.BANG_CONG_VIEC +
                        " ORDER BY " + FieldNameTableConst.CONGVIEC_MACONGVIEC + " DESC",
                resultClass = CongViec.class

        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_ALL_NO_OFFSET,
                query = "SELECT * FROM " + TableNameConst.BANG_CONG_VIEC,
                resultClass = CongViec.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_ALL,
                query = "SELECT * FROM " + TableNameConst.BANG_CONG_VIEC +
                        " ORDER BY " + FieldNameTableConst.CONGVIEC_MACONGVIEC + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongViec.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_ALL_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.BANG_CONG_VIEC
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_BY_CONGTRINH,
                query = "SELECT * FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?" +
                        " ORDER BY " + FieldNameTableConst.CONGVIEC_MACONGVIEC + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongViec.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_BY_CONGTRINH_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_BY_PHONGBAN,
                query = "SELECT * FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " = ?" +
                        " ORDER BY " + FieldNameTableConst.CONGVIEC_MACONGVIEC + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = CongViec.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_BY_PHONGBAN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_FIND_ALL_BY_CONGTRINH,
                query = "SELECT * FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?",
                resultClass = CongViec.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.CONGVIEC_TONG_CHI_PHI,
                query = "SELECT SUM (" + FieldNameTableConst.CONGVIEC_CHIPHI + ") FROM " + TableNameConst.BANG_CONG_VIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?"
        )
})
public class CongViec extends RecursiveTreeObject<CongViec> {
    // region Properties
    @Id
    @Column(name = FieldNameTableConst.CONGVIEC_MACONGVIEC, columnDefinition = "VARCHAR(10)")
    private String maCongViec;

    @Column(name = FieldNameTableConst.CONGVIEC_TENCONGVIEC, columnDefinition = "NVARCHAR(100)")
    private String tenCongViec;

    @Column(name = FieldNameTableConst.CONGVIEC_SOLUONGNHANVIENTOIDA, columnDefinition = "TINYINT")
    private int soLuongNhanVienToiDa;

    @Column(name = FieldNameTableConst.CONGVIEC_CHIPHI, columnDefinition = "REAL")
    private double chiPhi;

    @Column(name = FieldNameTableConst.CONGVIEC_NGAYDUKIENHOANTHANH, columnDefinition = "DATETIME")
    private Date ngayDuKienHoanThanh;

    @Column(name = FieldNameTableConst.CONGVIEC_NGAYHOANTHANH, columnDefinition = "DATETIME")
    private Date ngayHoanThanh;

    @ManyToOne
    @JoinColumn(name = FieldNameTableConst.CONGTRINH_MACONGTRINH, referencedColumnName = FieldNameTableConst.CONGTRINH_MACONGTRINH)
    private CongTrinh congTrinh;

    @ManyToOne
    @JoinColumn(name = FieldNameTableConst.PHONGBAN_MAPHONGBAN, referencedColumnName = FieldNameTableConst.PHONGBAN_MAPHONGBAN)
    private PhongBan phongBan;
    // endregion

    // region Getter, Setter
    public String getMaCongViec() {
        return maCongViec;
    }

    public void setMaCongViec(String maCongViec) throws ValidateException {
        if (Validate.isPatternId(maCongViec, PatternIdConst.PREFIX_CONG_VIEC)) {
            this.maCongViec = maCongViec;
        } else throw new ValidateException("Mã công việc không đúng định dạng");
    }

    public String getTenCongViec() {
        return tenCongViec;
    }

    public void setTenCongViec(String tenCongViec) throws ValidateException {
        if (tenCongViec.trim().length() >= 5) {
            this.tenCongViec = tenCongViec;
        } else throw new ValidateException("Tên công việc phải lớn hơn 5 kí tự");
    }

    public int getSoLuongNhanVienToiDa() {
        return soLuongNhanVienToiDa;
    }

    public void setSoLuongNhanVienToiDa(int soLuongNhanVienToiDa) throws ValidateException {
        if (soLuongNhanVienToiDa > 0) {
            this.soLuongNhanVienToiDa = soLuongNhanVienToiDa;
        } else throw new ValidateException("Số lượng nhân viên tối đa phải lớn hơn 0");
    }

    public double getChiPhi() {
        return chiPhi;
    }

    public void setChiPhi(double chiPhi) throws ValidateException {
        if (chiPhi >= 0) {
            this.chiPhi = chiPhi;
        } else throw new ValidateException("Chi phí phải lớn hơn hoặc bằng 0");
    }

    public Date getNgayDuKienHoanThanh() {
        return ngayDuKienHoanThanh;
    }

    public void setNgayDuKienHoanThanh(Date ngayDuKienHoanThanh) {
        this.ngayDuKienHoanThanh = ngayDuKienHoanThanh;
    }

    public Date getNgayHoanThanh() {
        return ngayHoanThanh;
    }

    public void setNgayHoanThanh(Date ngayHoanThanh) {
        this.ngayHoanThanh = ngayHoanThanh;
    }

    public CongTrinh getCongTrinh() {
        return congTrinh;
    }

    public void setCongTrinh(CongTrinh congTrinh) {
        this.congTrinh = congTrinh;
    }

    public PhongBan getPhongBan() {
        return phongBan;
    }

    public void setPhongBan(PhongBan phongBan) {
        this.phongBan = phongBan;
    }
    // endregion

    // region Constructors
    public CongViec() {
    }

    public CongViec(String maCongViec, String tenCongViec, int soLuongNhanVienToiDa, double chiPhi, Date ngayDuKienHoanThanh, Date ngayHoanThanh, CongTrinh congTrinh, PhongBan phongBan) {
        this.maCongViec = maCongViec;
        this.tenCongViec = tenCongViec;
        this.soLuongNhanVienToiDa = soLuongNhanVienToiDa;
        this.chiPhi = chiPhi;
        this.ngayDuKienHoanThanh = ngayDuKienHoanThanh;
        this.ngayHoanThanh = ngayHoanThanh;
        this.congTrinh = congTrinh;
        this.phongBan = phongBan;
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CongViec congViec = (CongViec) o;

        return maCongViec != null ? maCongViec.equals(congViec.maCongViec) : congViec.maCongViec == null;
    }

    @Override
    public int hashCode() {
        return maCongViec != null ? maCongViec.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "CongViec{" +
                "maCongViec='" + maCongViec + '\'' +
                ", tenCongViec='" + tenCongViec + '\'' +
                ", soLuongNhanVienToiDa=" + soLuongNhanVienToiDa +
                ", chiPhi=" + chiPhi +
                ", ngayDuKienHoanThanh=" + ngayDuKienHoanThanh +
                ", ngayHoanThanh=" + ngayHoanThanh +
                ", congTrinh=" + congTrinh +
                ", phongBan=" + phongBan +
                '}';
    }
    // endregion
}
