package org.buffalocoder.quanlylaodong.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;
import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.TableNameConst;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;


/**
 * Entity Phân công
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.1
 * @since : 05-10-2019
 */
@Entity
@Table(name = TableNameConst.BANG_PHAN_CONG)
@NamedNativeQueries({
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN + " WHERE " + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " NOT IN ( SELECT " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " FROM " + TableNameConst.BANG_PHAN_CONG +
                        " BPC JOIN " + TableNameConst.BANG_CONG_VIEC + " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = BCV." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?) AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0 " + "ORDER BY " +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN + " WHERE " + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " NOT IN ( SELECT " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " FROM " + TableNameConst.BANG_PHAN_CONG +
                        " BPC JOIN " + TableNameConst.BANG_CONG_VIEC + " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = BCV." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?) AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0 "
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA,
                query = "SELECT DISTINCT NV.* FROM " + TableNameConst.BANG_PHAN_CONG + " T " +
                        " RIGHT JOIN " + TableNameConst.NHAN_VIEN + " NV ON T." + FieldNameTableConst.NHANVIEN_MANHANVIEN
                        + " = NV." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " JOIN " + TableNameConst.BANG_CONG_VIEC + " BCV ON BCV." + FieldNameTableConst.PHONGBAN_MAPHONGBAN +
                        " = NV." + FieldNameTableConst.PHONGBAN_MAPHONGBAN +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0 " + "ORDER BY " +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.BANG_PHAN_CONG + " BPC " +
                        " JOIN " + TableNameConst.BANG_CONG_VIEC + " BCV ON BCV." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0 "
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_CONGTRINHDANGTHAMGIA,
                query = "SELECT DISTINCT * FROM " + TableNameConst.BANG_PHAN_CONG + " BPC " +
                        " JOIN " + TableNameConst.BANG_CONG_VIEC + " BCV ON BCV." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " WHERE " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " = ? ORDER BY " +
                        FieldNameTableConst.CONGTRINH_MACONGTRINH + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = PhanCong.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_CONGTRINHDANGTHAMGIA_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.BANG_PHAN_CONG + " WHERE " +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_DELETE_BY_NHANVIEN_CONGTRINH,
                query = "DELETE FROM " + TableNameConst.BANG_PHAN_CONG + " WHERE "
                        + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND "
                        + FieldNameTableConst.NHANVIEN_MANHANVIEN + " = ?",
                resultClass = PhanCong.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_BY_MACONGTRINH,
                query = "SELECT * FROM " + TableNameConst.BANG_PHAN_CONG + " WHERE "
                        + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ?",
                resultClass = PhanCong.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC,
                query = "SELECT N.* FROM " + TableNameConst.BANG_PHAN_CONG + " BPC JOIN " + TableNameConst.BANG_CONG_VIEC +
                        " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = BCV." +
                        FieldNameTableConst.CONGVIEC_MACONGVIEC + " JOIN " + TableNameConst.NHAN_VIEN + " N ON BPC." +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " = N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        " BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = ?  AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0 " + "ORDER BY N." +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.BANG_PHAN_CONG + " BPC JOIN " + TableNameConst.BANG_CONG_VIEC +
                        " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = BCV." +
                        FieldNameTableConst.CONGVIEC_MACONGVIEC + " JOIN " + TableNameConst.NHAN_VIEN + " N ON BPC." +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " = N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        " BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = ? AND N." + FieldNameTableConst.NHANVIEN_TRANGTHAI +
                        " = 0"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN,
                query = "SELECT * FROM " + TableNameConst.NHAN_VIEN + " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN +
                        " = ? AND " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " NOT IN ( SELECT N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " FROM " + TableNameConst.BANG_PHAN_CONG + " BPC JOIN " + TableNameConst.BANG_CONG_VIEC +
                        " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = BCV." +
                        FieldNameTableConst.CONGVIEC_MACONGVIEC + " JOIN " + TableNameConst.NHAN_VIEN + " N ON BPC." +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " = N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        " BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = ?) AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0" + " ORDER BY " +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY",
                resultClass = NhanVien.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN_COUNT,
                query = "SELECT COUNT(*) FROM " + TableNameConst.NHAN_VIEN + " WHERE " + FieldNameTableConst.PHONGBAN_MAPHONGBAN +
                        " = ? AND " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " NOT IN ( SELECT N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " FROM " + TableNameConst.BANG_PHAN_CONG + " BPC JOIN " + TableNameConst.BANG_CONG_VIEC +
                        " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = BCV." +
                        FieldNameTableConst.CONGVIEC_MACONGVIEC + " JOIN " + TableNameConst.NHAN_VIEN + " N ON BPC." +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " = N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        " BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = ?) AND " +
                        FieldNameTableConst.NHANVIEN_TRANGTHAI + " = 0"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_DELETE_BY_MACONGVIEC_MANHANVIEN,
                query = "DELETE FROM " + TableNameConst.BANG_PHAN_CONG + " WHERE " + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = ? AND " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " = ?"
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_BY_MACONGVIEC_MANHANVIEN,
                query = "SELECT * FROM " + TableNameConst.BANG_PHAN_CONG + " WHERE " + FieldNameTableConst.CONGVIEC_MACONGVIEC +
                        " = ? AND " + FieldNameTableConst.NHANVIEN_MANHANVIEN + " = ?",
                resultClass = PhanCong.class
        ),
        @NamedNativeQuery(
                name = NamedNativeQueriesConst.PHANCONG_FIND_DANHSACH_BY_CONGVIEC,
                query = "SELECT BPC.* FROM " + TableNameConst.BANG_PHAN_CONG + " BPC JOIN " + TableNameConst.BANG_CONG_VIEC +
                        " BCV ON BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = BCV." +
                        FieldNameTableConst.CONGVIEC_MACONGVIEC + " JOIN " + TableNameConst.NHAN_VIEN + " N ON BPC." +
                        FieldNameTableConst.NHANVIEN_MANHANVIEN + " = N." + FieldNameTableConst.NHANVIEN_MANHANVIEN +
                        " WHERE " + FieldNameTableConst.CONGTRINH_MACONGTRINH + " = ? AND " +
                        " BPC." + FieldNameTableConst.CONGVIEC_MACONGVIEC + " = ?",
                resultClass = PhanCong.class
        )
})
public class PhanCong extends RecursiveTreeObject<PhanCong> {
    // region Properties
    @EmbeddedId
    private PKCongViecNhanVien pkCongViecNhanVien;

    @Column(name = FieldNameTableConst.PHANCONG_NGAYBATDAU, columnDefinition = "DATETIME")
    private Date ngayBatDau;

    @Column(name = FieldNameTableConst.PHANCONG_NGAYKETTHUC, columnDefinition = "DATETIME")
    private Date ngayKetThuc;
    // endregion

    // region Getter, Setter
    public PKCongViecNhanVien getPkCongViecNhanVien() {
        return pkCongViecNhanVien;
    }

    public void setPkCongViecNhanVien(PKCongViecNhanVien pkCongViecNhanVien) {
        this.pkCongViecNhanVien = pkCongViecNhanVien;
    }

    public Date getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(Date ngayBatDau) throws ValidateException {
        Date currentDate = Date.valueOf(LocalDate.now());

        if (ngayBatDau.equals(currentDate) || ngayBatDau.after(currentDate)) {
            this.ngayBatDau = ngayBatDau;
        } else throw new ValidateException("Ngày bắt đầu phải sau hoặc bằng ngày hiện tại");
    }

    public Date getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(Date ngayKetThuc) throws ValidateException {
        if (ngayKetThuc.after(this.ngayBatDau)) {
            this.ngayKetThuc = ngayKetThuc;
        } else throw new ValidateException("Ngày kết thúc phải sau ngày bắt đầu");
    }
    // endregion

    // region Constructor
    public PhanCong() {
    }

    public PhanCong(PKCongViecNhanVien pkCongViecNhanVien, Date ngayBatDau, Date ngayKetThuc) throws ValidateException {
        this.setPkCongViecNhanVien(pkCongViecNhanVien);
        this.setNgayBatDau(ngayBatDau);
        this.setNgayKetThuc(ngayKetThuc);
    }

    public PhanCong(CongViec congViec, NhanVien nhanVien, Date ngayBatDau, Date ngayKetThuc) throws ValidateException {
        PKCongViecNhanVien pkCongViecNhanVien = new PKCongViecNhanVien(congViec, nhanVien);
        this.setPkCongViecNhanVien(pkCongViecNhanVien);
        this.setNgayBatDau(ngayBatDau);
        this.setNgayKetThuc(ngayKetThuc);
    }

    public PhanCong(CongViec congViec, NhanVien nhanVien) throws ValidateException {
        PKCongViecNhanVien pkCongViecNhanVien = new PKCongViecNhanVien(congViec, nhanVien);
        this.setPkCongViecNhanVien(pkCongViecNhanVien);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhanCong that = (PhanCong) o;

        return pkCongViecNhanVien != null ? pkCongViecNhanVien.equals(that.pkCongViecNhanVien) : that.pkCongViecNhanVien == null;
    }

    @Override
    public int hashCode() {
        return pkCongViecNhanVien != null ? pkCongViecNhanVien.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ThamGiaCongTrinh{" +
                "pkThamGiaCongTrinh=" + pkCongViecNhanVien +
                ", ngayBatDau=" + ngayBatDau +
                ", ngayKetThuc=" + ngayKetThuc +
                '}';
    }

    // endregion
}
