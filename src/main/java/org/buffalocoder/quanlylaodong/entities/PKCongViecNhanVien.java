package org.buffalocoder.quanlylaodong.entities;

import org.buffalocoder.quanlylaodong.constant.FieldNameTableConst;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Embeddable Entity Primary Key Công trình + Nhân viên
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.1
 * @since : 05-10-2019
 */
@Embeddable
public class PKCongViecNhanVien implements Serializable {
    @ManyToOne
    @JoinColumn(name = FieldNameTableConst.CONGVIEC_MACONGVIEC, referencedColumnName = FieldNameTableConst.CONGVIEC_MACONGVIEC)
    private CongViec congViec;

    @ManyToOne
    @JoinColumn(name = FieldNameTableConst.NHANVIEN_MANHANVIEN, referencedColumnName = FieldNameTableConst.NHANVIEN_MANHANVIEN)
    private NhanVien nhanVien;

    // region Getter, Setter
    public CongViec getCongViec() {
        return congViec;
    }

    public void setCongViec(CongViec congViec) {
        this.congViec = congViec;
    }

    public NhanVien getNhanVien() {
        return nhanVien;
    }

    public void setNhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
    }
    // endregion

    // region Constructor
    public PKCongViecNhanVien() {
    }

    public PKCongViecNhanVien(CongViec congViec, NhanVien nhanVien) {
        this.setNhanVien(nhanVien);
        this.setCongViec(congViec);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKCongViecNhanVien that = (PKCongViecNhanVien) o;

        if (congViec != null ? !congViec.equals(that.congViec) : that.congViec != null) return false;
        return nhanVien != null ? nhanVien.equals(that.nhanVien) : that.nhanVien == null;
    }

    @Override
    public int hashCode() {
        int result = congViec != null ? congViec.hashCode() : 0;
        result = 31 * result + (nhanVien != null ? nhanVien.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PKCongTrinhNhanVien{" +
                "congViec=" + congViec +
                ", nhanVien=" + nhanVien +
                '}';
    }
    // endregion
}
