package org.buffalocoder.quanlylaodong;

import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.utils.JpaPersistenceHelper;
import org.buffalocoder.quanlylaodong.views.LoginScene;

import javax.swing.*;
public class Main {
    public static void main(String[] args) {
        try {
            JpaPersistenceHelper.getInstance();
            LoginScene.main(args);
        } catch (DAOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
}
