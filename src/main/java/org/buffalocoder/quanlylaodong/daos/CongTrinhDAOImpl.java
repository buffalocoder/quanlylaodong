package org.buffalocoder.quanlylaodong.daos;

import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.entities.CongTrinh;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.ICongTrinhDAO;
import org.buffalocoder.quanlylaodong.types.LoaiCongTrinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiCongTrinh;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Công trình DAO
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 05-10-2019
 */
public class CongTrinhDAOImpl extends DataAccessObjectImpl<CongTrinh> implements ICongTrinhDAO {
    /**
     * Constructor có tham số
     *
     * @param entityManager
     */
    public CongTrinhDAOImpl(EntityManager entityManager) {
        super(entityManager, CongTrinh.class);
    }

    /**
     * Constructor không tham số
     */
    public CongTrinhDAOImpl() throws DAOException {
        super(CongTrinh.class);
    }

    /**
     * Tạo id mới
     *
     * @return String
     */
    @Override
    public String generateId() {
        int lastId = 0;

        try {
            CongTrinh congTrinh = (CongTrinh) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_LAST_ITEM)
                    .getResultList().get(0);

            lastId = Integer.parseInt(congTrinh.getMaCongTrinh()
                    .substring(PatternIdConst.PREFIX_CONG_TRINH.length(),
                            congTrinh.getMaCongTrinh().length()));
        } catch (Exception ignored) {
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_CONG_TRINH, ++lastId);
    }

    /**
     * Phương thức tìm tất cả theo trang
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<T>
     * @throws DAOException
     */
    @Override
    public FindResult<CongTrinh> findAll(int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_ALL_COUNT)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongTrinh> congTrinhs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_ALL, this.genericType)
                        .setParameter(1, offset)
                        .setParameter(2, itemPerPage)
                        .getResultList();

                return new FindResult<>(congTrinhs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức lấy tất cả công trình
     *
     * @return List<CongTrinh>
     * @throws DAOException
     */
    @Override
    public List<CongTrinh> findAll() throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_ALL_NO_OFFSET, this.genericType)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm công trình theo tên công trình
     *
     * @param tenCongTrinh String
     * @param page         int
     * @param itemPerPage  int
     * @return FindResult<CongTrinh>
     */
    @Override
    public FindResult<CongTrinh> findByTenCongTrinh(String tenCongTrinh, int page, int itemPerPage) throws DAOException {
        try {
            tenCongTrinh = "%" + tenCongTrinh + "%";
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_BY_TEN_COUNT)
                    .setParameter(1, tenCongTrinh)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongTrinh> congTrinhs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_BY_TEN, this.genericType)
                        .setParameter(1, tenCongTrinh)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(congTrinhs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm công trình theo trạng thái
     *
     * @param trangThaiCongTrinh {@link TrangThaiCongTrinh}
     * @param page               int
     * @param itemPerPage        int
     * @return FindResult<CongTrinh>
     * @throws DAOException
     */
    @Override
    public FindResult<CongTrinh> findByTrangThai(TrangThaiCongTrinh trangThaiCongTrinh, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_BY_TRANGTHAI_COUNT)
                    .setParameter(1, trangThaiCongTrinh.ordinal())
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongTrinh> congTrinhs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_BY_TRANGTHAI, this.genericType)
                        .setParameter(1, trangThaiCongTrinh.ordinal())
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(congTrinhs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Tìm công trình theo loại công trình
     *
     * @param loaiCongTrinh {@link LoaiCongTrinhEnum}
     * @param page          int
     * @param itemPerPage   int
     * @return FindResult<CongTrinh>
     * @throws DAOException
     */
    @Override
    public FindResult<CongTrinh> findByLoaiCongTrinh(LoaiCongTrinhEnum loaiCongTrinh, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_BY_LOAICONGTRINH_COUNT)
                    .setParameter(1, loaiCongTrinh.ordinal())
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongTrinh> congTrinhs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_BY_LOAICONGTRINH, this.genericType)
                        .setParameter(1, loaiCongTrinh.ordinal())
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(congTrinhs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm công trình trễ hạn
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<CongTrinh>
     * @throws DAOException
     */
    @Override
    public FindResult<CongTrinh> findDanhSachCongTrinhTreHan(int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHTREHAN_COUNT)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongTrinh> congTrinhs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHTREHAN, this.genericType)
                        .setParameter(1, offset)
                        .setParameter(2, itemPerPage)
                        .getResultList();

                return new FindResult<>(congTrinhs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức thống kê theo trạng thái công trình
     *
     * @param trangThaiCongTrinh {@link TrangThaiCongTrinh}
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeTheoTrangThai(TrangThaiCongTrinh trangThaiCongTrinh) throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_THONGKE_TINHTRANG_COUNT)
                    .setParameter(1, trangThaiCongTrinh.ordinal())
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Phương thức thống kê tổng số lượng công trình
     *
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeTongSoLuongCongTrinh() throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_ALL_COUNT)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Phương thức thống kê số lượng công trình trễ hạn
     *
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeCongTrinhTreHan() throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHTREHAN_COUNT)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Phương thức thống kê số lượng công trình hoàn thành
     *
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeCongTrinhHoanThanh() throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHHOANTHANH_COUNT)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Phương thức thống kê số lượng công trình đang thi công
     *
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeCongTrinhDangThiCong() throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_FIND_DANHSACHDANGTHICONG_COUNT)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public double thongKeChiPhiCongTrinh(String maCongTrinh) throws DAOException {
        try {
            return (Double) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGTRINH_THONGKE_TONGCHIPHI)
                    .setParameter(1, maCongTrinh)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
