package org.buffalocoder.quanlylaodong.daos;

import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;
import org.buffalocoder.quanlylaodong.interfaces.IDataAccessObject;
import org.buffalocoder.quanlylaodong.utils.JpaPersistenceHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Data Access Object Implement
 * Chứa các phương thức CRUD cơ bản của mọi lớp DAO
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @implSpec: IDataAccessObject
 * @since : 05-10-2019
 */
public class DataAccessObjectImpl<T> implements IDataAccessObject<T> {
    protected EntityManager entityManager;
    protected EntityTransaction entityTransaction;
    protected Class<T> genericType;

    /**
     * Constructor có tham số
     *
     * @param entityManager
     * @param genericType
     */
    public DataAccessObjectImpl(EntityManager entityManager, Class<T> genericType) {
        this.entityManager = entityManager;
        this.entityTransaction = entityManager.getTransaction();
        this.genericType = genericType;
    }

    /**
     * Constructor có tham số
     *
     * @param genericType
     */
    public DataAccessObjectImpl(Class<T> genericType) throws DAOException {
        this.entityManager = JpaPersistenceHelper.getInstance().getEntityManager();
        this.entityTransaction = this.entityManager.getTransaction();
        this.genericType = genericType;
    }

    /**
     * Phương thức tạo id
     *
     * @return String
     */
    @Override
    public String generateId() {
        return null;
    }

    /**
     * Phương thức thêm 1 đối tượng vào DB
     *
     * @param t
     * @return boolean
     */
    @Override
    public boolean add(T t) throws DAOException {
        try {
            this.entityTransaction.begin();
            this.entityManager.persist(t);
            this.entityTransaction.commit();
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }

        return true;
    }

    /**
     * Phương thức xoá 1 đối tượng trong DB
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteById(Object id) throws DAOException {
        try {
            T t = this.find(id);

            if (t != null) {
                this.entityTransaction.begin();
                this.entityManager.remove(t);
                this.entityTransaction.commit();
            }
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }

        return true;
    }

    /**
     * Phương thức xoá 1 đối tượng trong DB
     *
     * @param t
     * @return boolean
     */
    @Override
    public boolean delete(T t) throws DAOException {
        try {
            if (t != null) {
                this.entityTransaction.begin();
                this.entityManager.remove(t);
                this.entityTransaction.commit();
            }
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }

        return true;
    }

    /**
     * Phương thức update 1 đối tượng dựa trên id
     *
     * @param t
     * @return T
     */
    @Override
    public T update(T t) throws DAOException {
        try {
            this.entityTransaction.begin();
            this.entityManager.merge(t);
            this.entityTransaction.commit();

            return t;
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }
    }

    /**
     * Phương thức tìm 1 đối tượng dựa trên id
     *
     * @param id
     * @return T
     */
    @Override
    public T find(Object id) throws NotExistException, DAOException {
        T t = null;

        try {
            if (String.class.equals(id.getClass())) {
                t = this.entityManager.find(this.genericType, String.valueOf(id));
            } else if (Integer.class.equals(id.getClass())) {
                t = this.entityManager.find(this.genericType, id);
            }
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }

        return t;
    }

    /**
     * Phương thức tìm tất cả theo trang
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<T>
     * @throws DAOException
     */
    @Override
    public FindResult<T> findAll(int page, int itemPerPage) throws DAOException {
        return null;
    }
}
