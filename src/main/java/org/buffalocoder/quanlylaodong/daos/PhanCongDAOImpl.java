package org.buffalocoder.quanlylaodong.daos;

import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.entities.PhanCong;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.IPhanCongDAO;

import javax.persistence.EntityManager;
import java.util.List;

public class PhanCongDAOImpl extends DataAccessObjectImpl<PhanCong> implements IPhanCongDAO {
    /**
     * Constructor có tham số
     *
     * @param entityManager
     */
    public PhanCongDAOImpl(EntityManager entityManager) {
        super(entityManager, PhanCong.class);
    }

    /**
     * Constructor có tham số
     */
    public PhanCongDAOImpl() throws DAOException {
        super(PhanCong.class);
    }

    /**
     * Phương thức lấy danh sách nhân viên chưa tham gia công trình
     *
     * @param maCongTrinh String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     */
    @Override
    public FindResult<NhanVien> findDanhSachChuaThamGiaCongTrinh(String maCongTrinh, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA_COUNT)
                    .setParameter(1, maCongTrinh)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA, NhanVien.class)
                        .setParameter(1, maCongTrinh)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức lấy danh sách nhân viên đã tham gia công trình
     *
     * @param maCongTrinh String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     */
    @Override
    public FindResult<NhanVien> findDanhSachDaThamGiaCongTrinh(String maCongTrinh, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA_COUNT)
                    .setParameter(1, maCongTrinh)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> thamGiaCongTrinhs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA, NhanVien.class)
                        .setParameter(1, maCongTrinh)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(thamGiaCongTrinhs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức lấy danh sách nhân viên đã tham gia công trình
     *
     * @param maCongTrinh String
     * @return List<NhanVien>
     * @throws DAOException
     */
    @Override
    public List<NhanVien> findDanhSachDaThamGiaCongTrinh(String maCongTrinh) throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_DANHSACHNHANVIEN, NhanVien.class)
                    .setParameter(1, maCongTrinh)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách công trình nhân viên đang tham gia
     *
     * @param maNhanVien  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<ThamGiaCongTrinh>
     * @throws DAOException
     */
    @Override
    public FindResult<PhanCong> findDanhSachCongTrinhNhanVienDangThamGia(String maNhanVien, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_CONGTRINHDANGTHAMGIA_COUNT)
                    .setParameter(1, maNhanVien)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<PhanCong> phanCongs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_CONGTRINHDANGTHAMGIA, this.genericType)
                        .setParameter(1, maNhanVien)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(phanCongs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức xóa phân công
     *
     * @param maNhanVien
     * @param maCongTrinh
     * @return boolean
     */
    @Override
    public boolean deleteByMaNhanVienMaCongTrinh(String maNhanVien, String maCongTrinh) {
        try {
            this.entityTransaction.begin();
            int update = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_DELETE_BY_NHANVIEN_CONGTRINH, PhanCong.class)
                    .setParameter(1, maCongTrinh)
                    .setParameter(2, maNhanVien)
                    .executeUpdate();
            System.out.println(update);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Phương thức tìm danh sách tham gia công trình bằng mã công trình
     *
     * @param maCongTrinh
     * @return List<ThamGiaCongTrinh>
     */
    public List<PhanCong> findByMaCongTrinh(String maCongTrinh) {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_BY_MACONGTRINH, PhanCong.class)
                    .setParameter(1, maCongTrinh)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách nhân viên đã tham gia công trình với công việc X
     *
     * @param maCongTrinh String
     * @param maCongViec  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findDanhSachDaThamGiaCongTrinhByCongViec(String maCongTrinh, String maCongViec, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC_COUNT)
                    .setParameter(1, maCongTrinh)
                    .setParameter(2, maCongViec)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC, NhanVien.class)
                        .setParameter(1, maCongTrinh)
                        .setParameter(2, maCongViec)
                        .setParameter(3, offset)
                        .setParameter(4, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách nhân viên chưa tham gia công trình trong phòng ban
     *
     * @param maCongTrinh String
     * @param maCongViec  String
     * @param maPhongBan  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<PhanCong>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(String maCongTrinh, String maCongViec, String maPhongBan, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN_COUNT)
                    .setParameter(1, maPhongBan)
                    .setParameter(2, maCongTrinh)
                    .setParameter(3, maCongViec)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN, NhanVien.class)
                        .setParameter(1, maPhongBan)
                        .setParameter(2, maCongTrinh)
                        .setParameter(3, maCongViec)
                        .setParameter(4, offset)
                        .setParameter(5, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức xóa phân công
     *
     * @param maCongViec String
     * @param maNhanVien String
     * @return boolean
     * @throws DAOException
     */
    @Override
    public boolean deleteByMaCongViecMaNhanVien(String maCongViec, String maNhanVien) throws DAOException {
        try {
            this.entityTransaction.begin();
            int updated = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_DELETE_BY_MACONGVIEC_MANHANVIEN)
                    .setParameter(1, maCongViec)
                    .setParameter(2, maNhanVien)
                    .executeUpdate();
            this.entityTransaction.commit();

            return updated > 0;
        } catch (Exception e) {
            this.entityTransaction.rollback();
        }

        return false;
    }

    /**
     * Phương thức tìm phân công theo mã công việc, mã nhân viên
     *
     * @param maCongViec String
     * @param maNhanVien String
     * @return PhanCong
     * @throws DAOException
     */
    @Override
    public PhanCong findByMaCongViecMaNhanVien(String maCongViec, String maNhanVien) throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_BY_MACONGVIEC_MANHANVIEN, this.genericType)
                    .setParameter(1, maCongViec)
                    .setParameter(2, maNhanVien)
                    .getSingleResult();
        } catch (Exception e) {
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách tham gia công trình
     *
     * @param maCongTrinh String
     * @param maCongViec  String
     * @return List<PhanCong>
     * @throws DAOException
     */
    @Override
    public List<PhanCong> findDanhSachThamGiaCongTrinh(String maCongTrinh, String maCongViec) throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHANCONG_FIND_DANHSACH_BY_CONGVIEC, this.genericType)
                    .setParameter(1, maCongTrinh)
                    .setParameter(2, maCongViec)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
