package org.buffalocoder.quanlylaodong.daos;

import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.entities.CongViec;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.ICongViecDAO;

import javax.persistence.EntityManager;
import java.util.List;

public class CongViecDAOImpl extends DataAccessObjectImpl<CongViec> implements ICongViecDAO {
    /**
     * Constructor có tham số
     *
     * @param entityManager
     */
    public CongViecDAOImpl(EntityManager entityManager) {
        super(entityManager, CongViec.class);
    }

    /**
     * Constructor có tham số
     */
    public CongViecDAOImpl() throws DAOException {
        super(CongViec.class);
    }

    /**
     * Tạo id mới
     *
     * @return String
     */
    @Override
    public String generateId() {
        int lastId = 0;

        try {
            CongViec congViec = (CongViec) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_LAST_ITEM)
                    .getResultList().get(0);

            lastId = Integer.parseInt(congViec.getMaCongViec()
                    .substring(PatternIdConst.PREFIX_CONG_VIEC.length(),
                            congViec.getMaCongViec().length()));
        } catch (Exception ignored) {
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_CONG_VIEC, ++lastId);
    }

    /**
     * Phương thức tìm tất cả theo trang
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<T>
     * @throws DAOException
     */
    @Override
    public FindResult<CongViec> findAll(int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_ALL_COUNT)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongViec> congViecs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_ALL, this.genericType)
                        .setParameter(1, offset)
                        .setParameter(2, itemPerPage)
                        .getResultList();

                return new FindResult<>(congViecs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức lấy tất cả công việc
     *
     * @return List<CongViec>
     * @throws DAOException
     */
    @Override
    public List<CongViec> findAll() throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_ALL_NO_OFFSET, this.genericType)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm kiếm công việc theo công trình
     *
     * @param maCongTrinh String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<CongViec>
     * @throws DAOException
     */
    @Override
    public FindResult<CongViec> findByCongTrinh(String maCongTrinh, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_BY_CONGTRINH_COUNT)
                    .setParameter(1, maCongTrinh)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongViec> congViecs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_BY_CONGTRINH, this.genericType)
                        .setParameter(1, maCongTrinh)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(congViecs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm kiếm công việc theo phòng ban
     *
     * @param maPhongBan  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<CongViec>
     * @throws DAOException
     */
    @Override
    public FindResult<CongViec> findByPhongBan(String maPhongBan, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_BY_PHONGBAN_COUNT)
                    .setParameter(1, maPhongBan)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<CongViec> congViecs = this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_BY_PHONGBAN, this.genericType)
                        .setParameter(1, maPhongBan)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(congViecs, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<CongViec> findAllByCongTrinh(String maCongTrinh) throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_FIND_ALL_BY_CONGTRINH, this.genericType)
                    .setParameter(1, maCongTrinh)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tính tổng chi phí của công trình theo công việc
     *
     * @param maCongTrinh String
     * @return double
     * @throws DAOException
     */
    @Override
    public double tinhTongChiPhiCongViec(String maCongTrinh) throws DAOException {
        try {
            return (double) this.entityManager.createNamedQuery(NamedNativeQueriesConst.CONGVIEC_TONG_CHI_PHI)
                    .setParameter(1, maCongTrinh)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }
}
