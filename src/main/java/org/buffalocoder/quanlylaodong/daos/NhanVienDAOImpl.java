package org.buffalocoder.quanlylaodong.daos;

import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.constant.TableNameConst;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;
import org.buffalocoder.quanlylaodong.types.TrangThaiNhanVienEnum;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Nhân viên DAO
 *
 * @author: Nguyễn Chí Cường, Đặng Lê Minh Trường
 * @version: 1.0
 * @since : 05-10-2019
 */
public class NhanVienDAOImpl extends DataAccessObjectImpl<NhanVien> implements INhanVienDAO {

    /**
     * Constructor có tham số
     *
     * @param entityManager
     */
    public NhanVienDAOImpl(EntityManager entityManager) {
        super(entityManager, NhanVien.class);
    }

    /**
     * Constructor không tham số
     */
    public NhanVienDAOImpl() throws DAOException {
        super(NhanVien.class);
    }

    /**
     * Tạo id mới
     *
     * @return String
     */
    @Override
    public String generateId() {
        int lastId = 0;

        try {
            NhanVien nhanVien = (NhanVien) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_LAST_ITEM)
                    .getResultList().get(0);

            lastId = Integer.parseInt(nhanVien.getMaNhanVien()
                    .substring(PatternIdConst.PREFIX_NHAN_VIEN.length(),
                            nhanVien.getMaNhanVien().length()));
        } catch (Exception ignored) {
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_NHAN_VIEN, ++lastId);
    }

    /**
     * Phương thức tìm tất cả theo trang
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<T>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findAll(int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_ALL_COUNT)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_ALL, this.genericType)
                        .setParameter(1, offset)
                        .setParameter(2, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm nhân viên theo tên
     *
     * @param hoTen       String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findByHoTen(String hoTen, int page, int itemPerPage) throws DAOException {
        try {
            hoTen = "%" + hoTen + "%";

            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_HOTEN_COUNT)
                    .setParameter(1, hoTen)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_HOTEN, this.genericType)
                        .setParameter(1, hoTen)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách nhân viên theo trạng thái nhân viên có phân trang
     *
     * @param trangThaiNhanVien TrangThaiNhanVienEnum
     * @param page              int
     * @param itemPerPage       int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findByTinhTrang(TrangThaiNhanVienEnum trangThaiNhanVien, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_TRANGTHAI_COUNT)
                    .setParameter(1, trangThaiNhanVien.ordinal())
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_TRANGTHAI, this.genericType)
                        .setParameter(1, trangThaiNhanVien.ordinal())
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm nhân viên theo chứng minh nhân dân
     *
     * @param cmnd        String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findByCMND(String cmnd, int page, int itemPerPage) throws DAOException {
        try {
            cmnd = "%" + cmnd + "%";

            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_CMND_COUNT)
                    .setParameter(1, cmnd)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_CMND, this.genericType)
                        .setParameter(1, cmnd)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm nhân viên theo số điện thoại
     *
     * @param soDienThoai String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findBySoDienThoai(String soDienThoai, int page, int itemPerPage) throws DAOException {
        try {
            soDienThoai = "%" + soDienThoai + "%";

            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_SDT_COUNT)
                    .setParameter(1, soDienThoai)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_BY_SDT, this.genericType)
                        .setParameter(1, soDienThoai)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức cập nhật mật khẩu cho nhân viên
     *
     * @param id
     * @param matKhauMoi
     * @return boolean
     */
    @Override
    public boolean capNhatMatKhau(String id, String matKhauCu, String matKhauMoi) throws Exception {
        NhanVien nhanVien = find(id);
        int ketQua = 0;

        if (nhanVien == null) {
            throw new NotExistException("Nhân viên không tồn tại");
        }

        if (nhanVien.checkPassword(matKhauCu)) {
            String query = String.format("UPDATE %s SET MATKHAU = '%s' WHERE MANHANVIEN = '%s'",
                    TableNameConst.NHAN_VIEN, BCrypt.hashpw(matKhauMoi, BCrypt.gensalt(12)), id);
            System.out.println(query);

            try {
                this.entityTransaction.begin();
                ketQua = this.entityManager.createNativeQuery(query).executeUpdate();
                System.out.println(ketQua);
                this.entityTransaction.commit();
            } catch (Exception e) {
                this.entityTransaction.rollback();
                return false;
            }
        } else throw new ValidateException("Mật khẩu hiện tại không đúng");

        return ketQua != 0;
    }

    /**
     * Phương thức thống kê tổng số lượng nhân viên trong công trình
     *
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeTongSoNhanVienCuaCongTrinh(String maCongTrinh) throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_ALL_OF_CONGTRINH)
                    .setParameter(1, maCongTrinh)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Phương thức thống kê tổng số lượng nhân viên xây dựng trong công trình
     *
     * @return int
     * @throws DAOException
     */
    @Override
    public int thongKeLoaiNhanVienCuaCongTrinh(String maCongTrinh, String maPB) {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.NHANVIEN_FIND_ALL_XAYDUNG_OF_CONGTRINH)
                    .setParameter(1, maCongTrinh)
                    .setParameter(2, maPB)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }
}
