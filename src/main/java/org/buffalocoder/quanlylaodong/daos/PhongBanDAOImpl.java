package org.buffalocoder.quanlylaodong.daos;

import org.buffalocoder.quanlylaodong.constant.NamedNativeQueriesConst;
import org.buffalocoder.quanlylaodong.constant.PatternIdConst;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.entities.PhongBan;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.IPhongBanDAO;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Phòng ban DAO
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.1
 * @since : 05-10-2019
 */
public class PhongBanDAOImpl extends DataAccessObjectImpl<PhongBan> implements IPhongBanDAO {
    /**
     * Constructor có tham số
     *
     * @param entityManager
     */
    public PhongBanDAOImpl(EntityManager entityManager) {
        super(entityManager, PhongBan.class);
    }

    /**
     * Constructor không tham số
     */
    public PhongBanDAOImpl() throws DAOException {
        super(PhongBan.class);
    }

    /**
     * Tạo id mới
     *
     * @return String
     */
    @Override
    public String generateId() {
        int lastId = 0;

        try {
            PhongBan phongBan = (PhongBan) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_LAST_ITEM)
                    .getResultList().get(0);

            lastId = Integer.parseInt(phongBan.getMaPhongBan()
                    .substring(PatternIdConst.PREFIX_PHONG_BAN.length(),
                            phongBan.getMaPhongBan().length()));
        } catch (Exception ignored) {
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_PHONG_BAN, ++lastId);
    }

    /**
     * Phương thức tìm tất cả theo trang
     *
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<T>
     * @throws DAOException
     */
    @Override
    public FindResult<PhongBan> findAll(int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_ALL_COUNT)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<PhongBan> phongBans = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_ALL, this.genericType)
                        .setParameter(1, offset)
                        .setParameter(2, itemPerPage)
                        .getResultList();

                return new FindResult<>(phongBans, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức lấy tất cả phòng ban
     *
     * @return List<PhongBan>
     * @throws DAOException
     */
    @Override
    public List<PhongBan> findAll() throws DAOException {
        try {
            return this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_ALL_NO_OFFSET, this.genericType)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách nhân viên trong phòng ban
     *
     * @param maPhongBan  String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<NhanVien>
     * @throws DAOException
     */
    @Override
    public FindResult<NhanVien> findDanhSachNhanVienPhongBan(String maPhongBan, int page, int itemPerPage) throws DAOException {
        try {
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN_COUNT)
                    .setParameter(1, maPhongBan)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<NhanVien> nhanViens = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN, NhanVien.class)
                        .setParameter(1, maPhongBan)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(nhanViens, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách phòng ban bằng tên
     *
     * @param tenPhongBan String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<PhongBan>
     * @throws DAOException
     */
    @Override
    public FindResult<PhongBan> findByTenPhongBan(String tenPhongBan, int page, int itemPerPage) throws DAOException {
        try {
            tenPhongBan = "%" + tenPhongBan + "%";
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_BY_TEN_COUNT)
                    .setParameter(1, tenPhongBan)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<PhongBan> phongBans = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_BY_TEN, this.genericType)
                        .setParameter(1, tenPhongBan)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(phongBans, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Phương thức tìm danh sách phòng ban bằng số điện thoại
     *
     * @param soDienThoai String
     * @param page        int
     * @param itemPerPage int
     * @return FindResult<PhongBan>
     * @throws DAOException
     */
    @Override
    public FindResult<PhongBan> findBySoDienThoai(String soDienThoai, int page, int itemPerPage) throws DAOException {
        try {
            soDienThoai = "%" + soDienThoai + "%";
            int total = (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_BY_SODIENTHOAI_COUNT)
                    .setParameter(1, soDienThoai)
                    .getSingleResult();

            int totalPage = (int) Math.ceil((double) total / (double) itemPerPage);

            if (totalPage > 0) {
                int offset = (Math.max(page, 0)) * itemPerPage;
                List<PhongBan> phongBans = this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_BY_SODIENTHOAI, this.genericType)
                        .setParameter(1, soDienThoai)
                        .setParameter(2, offset)
                        .setParameter(3, itemPerPage)
                        .getResultList();

                return new FindResult<>(phongBans, totalPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Constructor có tham số
     *
     * @param entityManager
     * @param genericType
     */
    public PhongBanDAOImpl(EntityManager entityManager, Class<PhongBan> genericType) {
        super(entityManager, genericType);
    }

    /**
     * Phương thức đếm tổng số nhân viên
     *
     * @param maPhongBan String
     * @return int
     * @throws DAOException
     */
    @Override
    public int tongSoNhanVien(String maPhongBan) throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_COUNT_ALL)
                    .setParameter(1, maPhongBan)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Phương thức đếm tổng số nhân viên theo giới tính
     *
     * @param maPhongBan String
     * @param gioiTinh   boolean
     * @return int
     * @throws DAOException
     */
    @Override
    public int tongSoNhanVienTheoGioiTinh(String maPhongBan, boolean gioiTinh) throws DAOException {
        try {
            return (Integer) this.entityManager.createNamedQuery(NamedNativeQueriesConst.PHONGBAN_FIND_DANHSACHNHANVIEN_GIOITINH_COUNT)
                    .setParameter(1, maPhongBan)
                    .setParameter(2, gioiTinh)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }
}
