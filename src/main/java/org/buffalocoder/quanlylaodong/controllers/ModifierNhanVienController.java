package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.buffalocoder.quanlylaodong.daos.CongTrinhDAOImpl;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhanCongDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhongBanDAOImpl;
import org.buffalocoder.quanlylaodong.entities.*;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.interfaces.IPhanCongDAO;
import org.buffalocoder.quanlylaodong.interfaces.IPhongBanDAO;
import org.buffalocoder.quanlylaodong.types.GioiTinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiNhanVienEnum;
import org.buffalocoder.quanlylaodong.utils.ImageModifier;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;

public class ModifierNhanVienController implements Initializable {
    @FXML
    private JFXTextField txtMaNhanVien, txtTenNhanVien, txtCMND, txtDanToc, txtMatKhau, txtSoDienThoai;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTextArea txtDiaChi;
    @FXML
    private ToggleGroup gender;
    @FXML
    private JFXRadioButton radioNam, radioNu;
    @FXML
    private JFXTextField txtDiaChiChiTiet, txtKhuVuc, txtQuanHuyen, txtTinhThanhPho,txtGhiChu;
    @FXML
    private JFXPasswordField txtPasswords;
    @FXML
    private JFXDatePicker dateNgaySinh;
    @FXML
    private Label lblHoTen, labelPhongBan;
    @FXML
    private ImageView imgAvatar;
    @FXML
    private TreeTableColumn<PhanCong, String> colMaCongTrinh, colTenCongTrinh, colNgayThamGia, colNgayKetThuc,colViTri;
    @FXML
    private JFXTreeTableView<PhanCong> tblCongTrinh;
    @FXML
    private JFXTextField txtMaPhongBan;
    @FXML
    private JFXTextField txtTenPhongBan;
    @FXML
    private Pagination pagination;
    @FXML
    private JFXComboBox<String> cbPhongBan, cbTrangThai;
    @FXML
    private Tab tabTaiKhoan;


    private Pagination paginationNhanVien;
    private AlertController alertController;
    private ObservableList<NhanVien> nhanViens;
    private NhanVien nhanVienSelected;
    private NhanVienDAOImpl nhanVienDAO;
    private IPhongBanDAO phongBanDAO;
    private double x, y;
    private boolean gioiTinh = false;
    private GioiTinhEnum gioiTinhEnum;
    private JFXTreeTableView<NhanVien> tblNhanVien;
    private CongTrinhDAOImpl congTrinhDAOImpl;
    private IPhanCongDAO ThamGiaCongTrinhDAO;
    private String imgPath;
    private ObservableList<CongTrinh> congTrinhs = FXCollections.observableArrayList();
    private ObservableList<PhanCong> phanCongs = FXCollections.observableArrayList();
    private FindResult<CongTrinh> congTrinhFindResult;
    private FindResult<PhanCong> thamGiaCongTrinhFindResult;
    private FindResult<NhanVien> nhanVienFindResult;


    public void setPaginationNhanVien(Pagination paginationNhanVien) {
        this.paginationNhanVien = paginationNhanVien;
    }

    public void setTblNhanVien(JFXTreeTableView<NhanVien> tblNhanVien) {
        this.tblNhanVien = tblNhanVien;
    }

    // region Event
    @FXML
    private void onThemNhanVien(ActionEvent event) throws Exception {
        DiaChi diaChi = new DiaChi(txtDiaChiChiTiet.getText(), txtKhuVuc.getText(), txtQuanHuyen.getText(), txtTinhThanhPho.getText());
        String maPB = cbPhongBan.getValue();
        String[] maPBcut = maPB.split("-");
        PhongBan phongBan = phongBanDAO.find(maPBcut[0]);

        if (dateNgaySinh.getValue() == null) {
            alertController.alertInfo("chưa chọn ngày sinh");
            return;
        }

        if (txtTenNhanVien.getText().equals("")) {
            alertController.alertInfo("Chưa nhập tên nhân viên");
            return;
        }

        if (radioNam.isSelected()) {
            gioiTinhEnum = GioiTinhEnum.NAM;
            gioiTinh = true;
        }
        if (radioNu.isSelected()) {
            gioiTinhEnum = GioiTinhEnum.NU;
        }

        if(!radioNam.isSelected()&&!radioNu.isSelected()){
            gioiTinhEnum = GioiTinhEnum.NAM;
            gioiTinh = true;
        }

        File image = new File(imgPath);
        if (image.exists() && image.isFile()) {
            BufferedImage bufferedImage = ImageIO.read(image);
            ImageModifier.getInstance().write(bufferedImage, txtMaNhanVien.getText().trim());
        }

        if(txtPasswords.getText().equals("")){
            alertController.alertInfo("Chưa nhập mật khẩu");
            return;
        }
        try {
            NhanVien nhanVien = new NhanVien(
                    txtMaNhanVien.getText().trim(),
                    txtMaNhanVien.getText().trim(),
                    txtCMND.getText(),
                    txtDanToc.getText(),
                    gioiTinhEnum,
                    txtTenNhanVien.getText(),
                    txtPasswords.getText(),
                    Date.valueOf(dateNgaySinh.getValue()),
                    txtSoDienThoai.getText(),
                    diaChi,
                    TrangThaiNhanVienEnum.DANG_LAM
                    , phongBan
            );

            if (nhanVienDAO.add(nhanVien)) {
                this.nhanViens.add(nhanVien);
                transferData();

                onClose(event);
                nhanVienFindResult = this.nhanVienDAO.findAll(0, 20);
                if (paginationNhanVien.getPageCount() < nhanVienFindResult.getTotalPage()) {
                    paginationNhanVien.setPageCount(nhanVienFindResult.getTotalPage());
                    paginationNhanVien.currentPageIndexProperty();
                    paginationNhanVien.setCurrentPageIndex(nhanVienFindResult.getTotalPage());
                } else {
                    paginationNhanVien.currentPageIndexProperty();
                    paginationNhanVien.setCurrentPageIndex(paginationNhanVien.getPageCount());
                }

            }
        } catch (ValidateException | DAOException e) {
            alertController.alertError(e.getMessage());
        }
    }

    @FXML
    private void onCapNhatNhanVien(ActionEvent event) throws Exception {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1999, Calendar.AUGUST, 10);



        String trangThai =cbTrangThai.getValue();
        TrangThaiNhanVienEnum trangThaiNhanVienEnum = null;
        if(trangThai.equals("Đang làm"))  trangThaiNhanVienEnum =TrangThaiNhanVienEnum.DANG_LAM;
        else if (trangThai.equals("Đã nghỉ")) trangThaiNhanVienEnum = TrangThaiNhanVienEnum.NGHI_VIEC;

        if (radioNam.isSelected()) {
            gioiTinhEnum = GioiTinhEnum.NAM;
            gioiTinh = true;
        }
        if (radioNu.isSelected()) {
            gioiTinhEnum = GioiTinhEnum.NU;
        }

        String maPB = cbPhongBan.getValue();
        String[] maPBcut = maPB.split("-");
        PhongBan phongBan = phongBanDAO.find(maPBcut[0]);


        DiaChi diaChi = new DiaChi(txtDiaChiChiTiet.getText(), txtKhuVuc.getText(), txtQuanHuyen.getText(), txtTinhThanhPho.getText());
        try {
            NhanVien nhanVien = new NhanVien(
                    txtMaNhanVien.getText().trim(),
                    imgPath,
                    txtCMND.getText(),
                    txtDanToc.getText(),
                    gioiTinhEnum,
                    txtTenNhanVien.getText(),
                    "123456789",
                    Date.valueOf(dateNgaySinh.getValue()),
                    txtSoDienThoai.getText(),
                    diaChi,
                    trangThaiNhanVienEnum
                    , phongBan);
            NhanVien nhanVienUpdate = nhanVienDAO.update(nhanVien);
            this.nhanViens.set(nhanViens.indexOf(nhanVienSelected), nhanVienUpdate);
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);
            tblNhanVien.setRoot(nhanVienTreeItem);
            onClose(event);
        } catch (ValidateException | DAOException e) {
            alertController.alertError(e.getMessage());
        }
    }

    @FXML
    private void onDragged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void onPressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    public void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onChooseFiles() throws Exception {
        FileChooser fc = new FileChooser();
        imgPath = "";
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("File", "*.*"));
        List<File> f = fc.showOpenMultipleDialog(null);
        for (File file : f) {
            imgPath = file.getPath();
        }
    }
    // endregion

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        try {
            phongBanDAO = new PhongBanDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        try {
            if (cbPhongBan != null) {
                phongBanDAO.findAll().forEach(n -> {
                    String giaTrin = String.format(n.getMaPhongBan() + "-" + n.getTenPhongBan());
                    cbPhongBan.getItems().add(giaTrin);
                    //cbPhongBan.getItems().add(n.getMaPhongBan());
                    //String giaTrin= String.format(n.getMaPhongBan()+"-"+n.getTenPhongBan());
                    //cbPhongBan.setValue(n.getMaPhongBan());
                    cbPhongBan.setValue(giaTrin);

                });
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        try {
            congTrinhDAOImpl = new CongTrinhDAOImpl();
            nhanVienDAO = new NhanVienDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        try {
            ThamGiaCongTrinhDAO = new PhanCongDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        if (txtMaNhanVien != null) {
            txtMaNhanVien.setText(nhanVienSelected == null ? this.nhanVienDAO.generateId() : nhanVienSelected.getMaNhanVien());
        }
    }

    public void setNhanViens(ObservableList<NhanVien> nhanViens) {
        this.nhanViens = nhanViens;
    }

    public void setNhanVienSelected(NhanVien nhanVienSelected, boolean isEdit, boolean details) {
        this.nhanVienSelected = nhanVienSelected;

        if (isEdit) {
            tabTaiKhoan.setText("");
            tabTaiKhoan.setDisable(true);
            //if()
            cbTrangThai.getItems().addAll("Đang làm", "Đã nghỉ");
            cbTrangThai.setValue("Đang làm");
            System.out.println(String.valueOf(nhanVienSelected.getTrangThai()));

                String tinhTrangNhanVien = String.valueOf(nhanVienSelected.getTrangThai());
                if(tinhTrangNhanVien.equals(String.valueOf(TrangThaiNhanVienEnum.DANG_LAM))) tinhTrangNhanVien= "Đang làm";
                else if (tinhTrangNhanVien.equals(String.valueOf(TrangThaiNhanVienEnum.NGHI_VIEC))) tinhTrangNhanVien= "Đã nghỉ";

                System.out.println("-------------------------");
                System.out.println(tinhTrangNhanVien);
                cbTrangThai.setValue(tinhTrangNhanVien);
//
//                cbTrangThai.setValue(tinhTrangNhanVien);
//            if(nhanVienSelected.getTrangThai()!=null){
//                String tinhTrangNhanVien = String.valueOf(nhanVienSelected.getTrangThai());
//                if(tinhTrangNhanVien.equals(TrangThaiNhanVienEnum.DANG_LAM)) tinhTrangNhanVien= "Đang làm";
//                else if (tinhTrangNhanVien.equals(TrangThaiNhanVienEnum.NGHI_VIEC)) tinhTrangNhanVien= "Đã nghỉ";
//
//                cbTrangThai.setValue(tinhTrangNhanVien);
//            }

        }
        if (details) {
            System.out.println(nhanVienSelected);

            txtGhiChu.setText(String.valueOf(nhanVienSelected.getTrangThai()));
            colMaCongTrinh.setCellValueFactory(cellData ->
                    new SimpleStringProperty(cellData.getValue().getValue().getPkCongViecNhanVien().getCongViec().getCongTrinh().getMaCongTrinh()));
            colTenCongTrinh.setCellValueFactory(cellData ->
                    new SimpleStringProperty(cellData.getValue().getValue().getPkCongViecNhanVien().getCongViec().getCongTrinh().getTenCongTrinh()));
            colViTri.setCellValueFactory(cellData ->
                    new SimpleStringProperty(cellData.getValue().getValue().getPkCongViecNhanVien().getCongViec().getPhongBan().getTenPhongBan()));
            colNgayThamGia.setCellValueFactory(cellData ->
                    new SimpleStringProperty(String.valueOf(cellData.getValue().getValue().getPkCongViecNhanVien().getCongViec().getCongTrinh().getNgayThiCong())));
            colNgayKetThuc.setCellValueFactory(cellData ->
                    new SimpleStringProperty(String.valueOf(cellData.getValue().getValue().getPkCongViecNhanVien().getCongViec().getCongTrinh().getNgayDuKienHoanThanh())));
            try {
                if (ThamGiaCongTrinhDAO.findDanhSachCongTrinhNhanVienDangThamGia(nhanVienSelected.getMaNhanVien(), 0, 10) != null) {
                    pagination.setPageCount(ThamGiaCongTrinhDAO.findDanhSachCongTrinhNhanVienDangThamGia(nhanVienSelected.getMaNhanVien(), 0, 10).getTotalPage());
                    pagination.setPageFactory(this::createPage);
                } else {
                    pagination.setPageCount(1);
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }
        } else {
            txtPasswords.setText(nhanVienSelected.getMatKhau());
        }

        try {
            txtMaNhanVien.setText(nhanVienSelected.getMaNhanVien());
            txtTenNhanVien.setText(nhanVienSelected.getHoTen());
            txtCMND.setText(nhanVienSelected.getCmnd());
            imgPath = nhanVienSelected.getAnhCaNhan();
            if (imgAvatar != null) {
                imgAvatar.setImage(ImageModifier.getInstance().read(imgPath));
            }
            DiaChi diaChi = nhanVienSelected.getDiaChi();
            String diaChiChiTiet = String.valueOf(diaChi.getDiaChiChiTiet());
            String khuVuc = String.valueOf(diaChi.getKhuVuc());
            String quanHuyen = String.valueOf(diaChi.getQuanHuyen());
            String thanhPho = String.valueOf(diaChi.getTinhThanhPho());
            if (!isEdit) {
                txtDiaChi.setText(diaChiChiTiet + " " + khuVuc + " " + quanHuyen + " " + thanhPho);
                lblHoTen.setText(nhanVienSelected.getHoTen());
            } else {
                txtDiaChiChiTiet.setText(diaChiChiTiet);
                txtKhuVuc.setText(khuVuc);
                txtQuanHuyen.setText(quanHuyen);
                txtTinhThanhPho.setText(thanhPho);
            }
            txtDanToc.setText(nhanVienSelected.getDanToc());
            txtSoDienThoai.setText(nhanVienSelected.getSoDienThoai());
            if (nhanVienSelected.isGioiTinh().equals("Nam")) {
                radioNam.setSelected(true);
                gioiTinhEnum = GioiTinhEnum.NAM;

            }
            if (nhanVienSelected.isGioiTinh().equals("Nữ")) {
                radioNu.setSelected(true);
                gioiTinhEnum = GioiTinhEnum.NU;
            }
            dateNgaySinh.setValue(nhanVienSelected.getNgaySinh().toLocalDate());
            PhongBan phongBan = phongBanDAO.find(nhanVienSelected.getPhongBan().getMaPhongBan());
            String phongBanNhanVien = String.format(phongBan.getMaPhongBan() + "-" + phongBan.getTenPhongBan());
            cbPhongBan.setValue(phongBanNhanVien);
            txtTenPhongBan.setText(phongBan.getTenPhongBan());
            labelPhongBan.setText(phongBan.getTenPhongBan());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void transferData() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/NhanVienScene.fxml"));
            fxmlLoader.load();
            NhanVienController nhanVienController = fxmlLoader.getController();
            nhanVienController.updateDataTable(this.nhanViens);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        return false;
    }

    private Node createPage(int pageIndex) {
        try {
            phanCongs = FXCollections.observableArrayList(ThamGiaCongTrinhDAO.findDanhSachCongTrinhNhanVienDangThamGia(nhanVienSelected.getMaNhanVien(), pageIndex, 20).getListResult());
            TreeItem<PhanCong> congTrinhTreeItem = new RecursiveTreeItem<>(this.phanCongs, RecursiveTreeObject::getChildren);
            if (tblCongTrinh.getRoot() != null) {
                tblCongTrinh.getRoot().getChildren().clear();
            }
            tblCongTrinh.setRoot(congTrinhTreeItem);
            tblCongTrinh.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblCongTrinh;
    }

    public void updateDataTable() throws DAOException {
//        congTrinhFindResult=congTrinhDAOImpl.findByMaNhanVien(nhanVienSelected.getMaNhanVien(),1,10);
        //congTrinhFindResult = congTrinhDAOImpl.findByTenCongTrinh("vnc",1,20);
//        System.out.println(nhanVienSelected.getMaNhanVien());
//        if(congTrinhFindResult==null){
//            System.out.println("true");
//        }
//        congTrinhFindResult.getListResult().forEach(System.out::println);
//       // congTrinhs = FXCollections.observableArrayList();
//        this.congTrinhs =  FXCollections.observableArrayList(congTrinhFindResult.getListResult());
//        TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
//
//        if (tblCongTrinh.getRoot() != null) {
//            tblCongTrinh.getRoot().getChildren().clear();
//        }
//        tblCongTrinh.setRoot(congTrinhTreeItem);
//        tblCongTrinh.setShowRoot(false);
    }


    private Node createPageMain(int pageIndex) {

        try {
            nhanVienFindResult = this.nhanVienDAO.findAll(pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(this.nhanVienDAO.findAll(pageIndex, 20).getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }
}
