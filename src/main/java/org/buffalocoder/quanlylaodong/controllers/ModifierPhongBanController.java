package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhongBanDAOImpl;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.entities.PhongBan;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Điều khiển luồng hoạt động thêm, sửa phòng ban
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 15-10-2019
 */
public class ModifierPhongBanController implements Initializable {
    private double x, y;
    private AlertController alertController;
    private ObservableList<PhongBan> phongBans;
    private ObservableList<NhanVien> nhanViens = FXCollections.observableArrayList();
    private PhongBan phongBanSelected;
    private PhongBanDAOImpl phongBanDAOImpl;
    private INhanVienDAO nhanVienDAO;
    private boolean isAdd;
    private JFXTreeTableView tblPhongBan;
    private FindResult<NhanVien> nhanVienFindResult;
    private FindResult<PhongBan> phongBanFindResult;
    private Pagination paginationPhongBan;

    @FXML
    private Pagination Pagination1;
    @FXML
    private JFXTreeTableView tblNhanVien;
    @FXML
    private TreeTableColumn colMaNhanVien, colHoTen, colGioiTinh, colSoDienThoai, colCMND, colNgaySinh;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTextField txtMaPhongBan, txtTenPhongBan, txtSoDienThoai;
    @FXML
    private JFXTextArea txtMoTa;
    @FXML
    private FontAwesomeIcon icon;
    @FXML
    private JFXButton btnModifier;
    @FXML
    private Label lblTitle;
    @FXML
    private JFXTextField txtTongNhanVien;
    @FXML
    private JFXTextField txtTongNhanVienNam;
    @FXML
    private JFXTextField txtTongNhanVienNu;


    public void setPaginationPhongBan(Pagination paginationPhongBan) {
        this.paginationPhongBan = paginationPhongBan;
    }

    public void setTblPhongBan(JFXTreeTableView tblPhongBan) {
        this.tblPhongBan = tblPhongBan;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            phongBanDAOImpl = new PhongBanDAOImpl();
            nhanVienDAO = new NhanVienDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        alertController = new AlertController(stackPane);

        txtMaPhongBan.setText(phongBanSelected == null ? this.phongBanDAOImpl.generateId() : phongBanSelected.getMaPhongBan());
        isAdd = true;
    }

    // region Event

    /**
     * Phương thức xử lý khi kéo cửa sổ
     *
     * @param event
     */
    @FXML
    private void onDragged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào cửa sổ
     *
     * @param event
     */
    @FXML
    private void onPressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    /**
     * Phương thức đóng cửa sổ
     *
     * @param actionEvent
     */
    @FXML
    public void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Xử lý thêm, cập nhật phòng ban
     *
     * @param actionEvent
     */
    @FXML
    private void onSave(ActionEvent actionEvent) {
        if (isAdd) {
            themPhongBan(actionEvent);
        } else {
            capNhatPhongBan(actionEvent);
        }
    }
    // endregion

    /**
     * Phương thức nhận danh sách phòng ban từ controller cha
     *
     * @param phongBans
     */
    public void setPhongBans(ObservableList<PhongBan> phongBans) {
        this.phongBans = phongBans;
    }

    /**
     * Phương thức truyền phòng ban được chọn từ controller cha
     *
     * @param phongBanSelected
     * @param isEdit
     */
    public void setPhongBanSelected(PhongBan phongBanSelected, boolean isEdit, boolean detail) {
        this.phongBanSelected = phongBanSelected;

        if (phongBanSelected != null) {
            if (isEdit) {
                icon.setGlyphName("EDIT");
                btnModifier.setText("Lưu");
            }

            isAdd = false;
            if (!detail) {
                lblTitle.setText(isEdit ? "Cập nhật thông tin phòng ban" : "Thông tin phòng ban");
                txtMoTa.setText(phongBanSelected.getMoTa());
            } else {
                colMaNhanVien.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("maNhanVien"));
                colHoTen.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("hoTen"));
                colSoDienThoai.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("soDienThoai"));
                colCMND.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("cmnd"));
                colNgaySinh.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, LocalDate>("ngaySinh"));
                colGioiTinh.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, Boolean>("gioiTinh"));


                try {
                    txtTongNhanVien.setText(String.valueOf(phongBanDAOImpl.tongSoNhanVien(phongBanSelected.getMaPhongBan())));
                    txtTongNhanVienNam.setText(String.valueOf(phongBanDAOImpl.tongSoNhanVienTheoGioiTinh(phongBanSelected.getMaPhongBan(), true)));
                    txtTongNhanVienNu.setText(String.valueOf(phongBanDAOImpl.tongSoNhanVienTheoGioiTinh(phongBanSelected.getMaPhongBan(), false)));
                    nhanVienFindResult = phongBanDAOImpl.findDanhSachNhanVienPhongBan(phongBanSelected.getMaPhongBan(), 0, 20);


                    if (phongBanDAOImpl.findDanhSachNhanVienPhongBan(phongBanSelected.getMaPhongBan(), 0, 20) == null) {
                        Pagination1.setPageCount(1);
                    } else {
                        Pagination1.setPageCount(nhanVienFindResult.getTotalPage());
                        Pagination1.setPageFactory(this::createPageNhanVienThuocPhongBan);
                    }

                } catch (DAOException E) {
                    E.printStackTrace();
                }
            }
            txtMaPhongBan.setText(phongBanSelected.getMaPhongBan());
            txtTenPhongBan.setText(phongBanSelected.getTenPhongBan());
            txtSoDienThoai.setText(phongBanSelected.getSoDienThoai());
            //txtMoTa.setText(phongBanSelected.getMoTa());
        }
    }

    /**
     * Phương thức thêm phòng ban
     *
     * @param mouseEvent
     */
    public void themPhongBan(ActionEvent mouseEvent) {
        try {

            if (txtTenPhongBan.getText().equals("")) {
                alertController.alertInfo("Chưa nhập tên phòng ban");
                return;
            }

            PhongBan phongBan = new PhongBan(
                    txtMaPhongBan.getText().trim(),
                    txtTenPhongBan.getText().trim(),
                    txtMoTa.getText().trim(),
                    txtSoDienThoai.getText().trim()
            );

            if (phongBanDAOImpl.add(phongBan)) {
                this.phongBans.add(phongBan);
                transferData();
                onClose(mouseEvent);

                phongBanFindResult = phongBanDAOImpl.findAll(0, 20);
                if (paginationPhongBan.getPageCount() < phongBanFindResult.getTotalPage()) {
                    paginationPhongBan.setPageCount(phongBanFindResult.getTotalPage());
                    paginationPhongBan.currentPageIndexProperty();
                    paginationPhongBan.setCurrentPageIndex(phongBanFindResult.getTotalPage());
                } else {
                    paginationPhongBan.currentPageIndexProperty();
                    paginationPhongBan.setCurrentPageIndex(paginationPhongBan.getPageCount());
                }
            }
        } catch (ValidateException | DAOException e) {
            alertController.alertError(e.getMessage());
        }
    }

    /**
     * Phương thức cập nhật phòng ban
     *
     * @param mouseEvent
     */
    public void capNhatPhongBan(ActionEvent mouseEvent) {
        try {
            PhongBan phongBan = new PhongBan(
                    phongBanSelected.getMaPhongBan(),
                    txtTenPhongBan.getText().trim(),
                    txtMoTa.getText().trim(),
                    txtSoDienThoai.getText().trim()
            );

            PhongBan phongBanUpdated = phongBanDAOImpl.update(phongBan);
            if (phongBanUpdated != null) {
                this.phongBans.set(phongBans.indexOf(phongBanSelected), phongBanUpdated);
                TreeItem<PhongBan> phongBanTreeItem = new RecursiveTreeItem<>(this.phongBans, RecursiveTreeObject::getChildren);
                tblPhongBan.setRoot(phongBanTreeItem);
                onClose(mouseEvent);
            }
        } catch (ValidateException | DAOException e) {
            alertController.alertError(e.getMessage());
        }

    }

    /**
     * Truyền data về controller cha
     */
    private void transferData() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/PhongBanScene.fxml"));
            fxmlLoader.load();
            QuanLyPhongBanController quanLyPhongBanController = fxmlLoader.getController();
            quanLyPhongBanController.updateDataTable(this.phongBans);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Node createPageNhanVienThuocPhongBan(int pageIndex) {

        try {
            nhanVienFindResult = phongBanDAOImpl.findDanhSachNhanVienPhongBan(phongBanSelected.getMaPhongBan(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }


    private Node createPageNhanVienThuocPhongBanNull(int pageIndex) {

        try {
            nhanVienFindResult = phongBanDAOImpl.findDanhSachNhanVienPhongBan(phongBanSelected.getMaPhongBan(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }


}
