package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.StackPane;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;
import org.buffalocoder.quanlylaodong.utils.StorageAccount;
import org.mindrot.jbcrypt.BCrypt;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author: Đặng Lê Minh Trường, Nguyễn Chí Cường
 * @version: 1.1
 */
public class CapNhatMatKhauController implements Initializable {
    private AlertController alertController;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTextField txtMatKhauCu, txtMatKhauMoi, txtNhapLaiMatKhauMoi;
    INhanVienDAO nhanVienDAO;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        try {
            nhanVienDAO = new NhanVienDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        if (txtMatKhauCu.getText().trim().isEmpty()) {
            alertController.alertError("Vui lòng nhập mật khẩu hiện tại");
            return false;
        } else if (txtMatKhauMoi.getText().trim().isEmpty()) {
            alertController.alertError("Vui lòng nhập mật khẩu mới");
            return false;
        } else if (txtNhapLaiMatKhauMoi.getText().trim().isEmpty()) {
            alertController.alertError("Vui lòng nhập lại mật khẩu mới");
            return false;
        } else if (!txtMatKhauMoi.getText().trim().equals(txtNhapLaiMatKhauMoi.getText().trim())) {
            alertController.alertError("Mật khẩu mới và xác nhận không giống nhau");
            return false;
        }

        return true;
    }

    // region Event
    @FXML
    public void onCapNhatMatKhau(ActionEvent actionEvent) {
        if (validate() != true) return;

        String matKhauCu = txtMatKhauCu.getText().trim();
        String matKhauMoi = txtMatKhauMoi.getText().trim();

        try {
            NhanVien nhanVien = StorageAccount.getInstance().getNhanVien();

            if(!BCrypt.checkpw(txtMatKhauCu.getText(),nhanVien.getMatKhau())){
                alertController.alertInfo("Mật khẩu nhập không đúng");
                return;
            }
//            System.out.println(nhanVien);
//            System.out.println(matKhauCu);
//            System.out.println(matKhauMoi);

            boolean isCapNhat = nhanVienDAO.capNhatMatKhau(nhanVien.getMaNhanVien(), matKhauCu, matKhauMoi);
            alertController.alertInfo(isCapNhat ? "Đổi mật khẩu thành công" : "Đổi mật khẩu không thành công");
        } catch (Exception e) {
            alertController.alertError(e.getMessage());
        }
    }
    /// endregion
}
