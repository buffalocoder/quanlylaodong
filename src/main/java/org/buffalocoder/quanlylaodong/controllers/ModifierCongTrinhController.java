package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlylaodong.daos.*;
import org.buffalocoder.quanlylaodong.entities.*;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.interfaces.ICongViecDAO;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;
import org.buffalocoder.quanlylaodong.interfaces.IPhongBanDAO;
import org.buffalocoder.quanlylaodong.types.LoaiCongTrinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiCongTrinh;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class ModifierCongTrinhController implements Initializable {
    private double x, y;
    private CongTrinh congTrinhSelected;
    private AlertController alertController;
    private ObservableList<CongTrinh> congTrinhs;
    private CongTrinhDAOImpl congTrinhDAOImpl;
    private ICongViecDAO congViecDAO;
    private INhanVienDAO nhanVienDAO;
    private IPhongBanDAO phongBanDAO;
    private boolean isAdd;
    private JFXTreeTableView<CongTrinh> tblCongTrinh;
    private ObservableList<NhanVien> nhanViens = FXCollections.observableArrayList();
    private ObservableList<CongViec> congViecs = FXCollections.observableArrayList();
    private ObservableList<CongViec> congViecsHientai = FXCollections.observableArrayList();
    private PhanCongDAOImpl thamGiaCongTrinhDAO;
    private FindResult<NhanVien> nhanVienFindResult;
    private FindResult<CongViec> congViecFindResult;
    private List<CongViec> congViecList;
    private Pagination paginationCongTrinh;
    private FindResult<CongTrinh> congTrinhFindResult;

    @FXML
    private Tab tabCongViec, tabCongTrinh;
    @FXML
    private Pagination paginationCongViec;
    @FXML
    private JFXTreeTableView<CongViec> tblCongViec;
    @FXML
    private TreeTableColumn colTenCongViec, colSoluongNhanVien, colChiPhi, colDuKien, colHoanThanh;
    @FXML
    private TreeTableColumn<CongViec, String> colMaPhongBan;
    @FXML
    private JFXTreeTableView<NhanVien> tblNhanVien;
    @FXML
    private Pagination pagination;

    @FXML
    private TreeTableColumn colMaNhanVien, colHoTen, colGioiTinh, colSoDienThoai, colCMND;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTextField txtMaCongTrinh, txtTenCongTrinh,
            txtLoaiCongTrinh, txtDiaChiChiTiet,
            txtPhuong, txtQuanHuyen, txtThanhPho, txtTrangThai,
            txtSoluongThamGia, txtTenCongViec, txtSoLuongCongViec, txtChiPhi, txtChiPhiCongTrinh;
    @FXML
    private JFXTextArea txtDiaChi;
    @FXML
    private JFXDatePicker dateNgayThiCong, dateNgayCapPhep, dateNgayHoanThanh, dateDuKien, dateHoanThanh;
    @FXML
    private JFXDatePicker dateNgayDuKienHoanThanh;
    @FXML
    private JFXButton btnModifier, btnModifier1;
    @FXML
    private Label lblTitle;
    @FXML
    private FontAwesomeIcon icon;
    @FXML
    private JFXComboBox<String> cbLoaiCongTrinh, cbTrangThai, cbPhongBan;


    public void setPaginationCongTrinh(Pagination paginationCongTrinh) {
        this.paginationCongTrinh = paginationCongTrinh;
    }

    public void setPaginationCongViec(Pagination paginationCongViec) {
        this.paginationCongViec = paginationCongViec;
    }

    public void setTblCongTrinh(JFXTreeTableView<CongTrinh> tblCongTrinh) {
        this.tblCongTrinh = tblCongTrinh;

    }

    public void setCongTrinhSelected() {
        tabCongViec.setDisable(true);
        tabCongViec.setText("");
    }

    public void setCongTrinhSelected(CongTrinh congTrinhSelected) {
        this.congTrinhSelected = congTrinhSelected;
        dateNgayDuKienHoanThanh.setValue(congTrinhSelected.getNgayDuKienHoanThanh().toLocalDate());
    }

    public void setCongTrinhSelected(CongTrinh congTrinhSelected, boolean isEdit, boolean isDetail) {
        this.congTrinhSelected = congTrinhSelected;


        System.out.println(congTrinhSelected);
        colTenCongViec.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, String>("tenCongViec"));
        colSoluongNhanVien.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, Integer>("soLuongNhanVienToiDa"));
        colChiPhi.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, Integer>("chiPhi"));
        colDuKien.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, LocalDate>("ngayDuKienHoanThanh"));
        colHoanThanh.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, LocalDate>("ngayHoanThanh"));
        colMaPhongBan.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getPhongBan().getTenPhongBan()));
        try {
            congViecFindResult = congViecDAO.findByCongTrinh(congTrinhSelected.getMaCongTrinh(), 0, 20);
            if (congViecFindResult != null) {
                paginationCongViec.setPageCount(congViecFindResult.getTotalPage());
                paginationCongViec.setPageFactory(this::createPageCongViec);
            } else {
                paginationCongViec.setPageCount(1);
                paginationCongViec.setPageFactory(this::createPageCongViecnull);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        if (isDetail) {
            colMaNhanVien.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("maNhanVien"));
            colHoTen.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("hoTen"));
            colSoDienThoai.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("soDienThoai"));
            colCMND.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("cmnd"));
            colGioiTinh.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, Boolean>("gioiTinh"));

            try {
                nhanVienFindResult = thamGiaCongTrinhDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(), 0, 20);
                txtChiPhiCongTrinh.setText(String.valueOf(congViecDAO.tinhTongChiPhiCongViec(congTrinhSelected.getMaCongTrinh())));
                if (nhanVienFindResult != null) {
                    pagination.setPageCount(nhanVienFindResult.getTotalPage());
                    pagination.setPageFactory(this::createPageByMaCongTrinh);
                    txtSoluongThamGia.setText(String.valueOf(nhanVienFindResult.getListResult().size()));
                } else {
                    pagination.setPageCount(1);
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }
            //loadTableNhanVienDaThamGia();
        }


        if (congTrinhSelected != null) {
            if (isEdit) {
                icon.setGlyphName("EDIT");
                btnModifier.setText("Lưu");
                btnModifier1.setText("Lưu");
                tabCongTrinh.setText("Sửa công trình");
            }
            isAdd = false;
            lblTitle.setText(isEdit ? "Cập nhật thông tin công trình" : "Thông tin công trình");

            txtMaCongTrinh.setText(congTrinhSelected.getMaCongTrinh());
            txtTenCongTrinh.setText(congTrinhSelected.getTenCongTrinh());

            if (congTrinhSelected.getNgayCapGiayPhep() != null) {
                dateNgayCapPhep.setValue(congTrinhSelected.getNgayCapGiayPhep().toLocalDate());
            }

            if (congTrinhSelected.getNgayThiCong() != null) {
                dateNgayThiCong.setValue(congTrinhSelected.getNgayThiCong().toLocalDate());
            }

            if (congTrinhSelected.getNgayDuKienHoanThanh() != null) {
                dateNgayHoanThanh.setValue(congTrinhSelected.getNgayDuKienHoanThanh().toLocalDate());
            }

            if (congTrinhSelected.getLoaiCongTrinh() != null) {
                String loaiCongTrinh = String.valueOf(congTrinhSelected.getLoaiCongTrinh());
                if (loaiCongTrinh.equals(String.valueOf(LoaiCongTrinhEnum.CONG_TRINH_DAN_DUNG)))
                    loaiCongTrinh = "Dân Dụng";
                else if (loaiCongTrinh.equals(String.valueOf(LoaiCongTrinhEnum.CONG_TRINH_GIAO_THONG)))
                    loaiCongTrinh = "Giao thông";
                else if (loaiCongTrinh.equals(String.valueOf(LoaiCongTrinhEnum.CONG_TRINH_CONG_NGHIEP)))
                    loaiCongTrinh = "Công Ngiệp";
                else if (loaiCongTrinh.equals(String.valueOf(LoaiCongTrinhEnum.CONG_TRINH_HA_TANG_KI_THUAT)))
                    loaiCongTrinh = "Hạ tầng kĩ thuật";
                else if (loaiCongTrinh.equals(String.valueOf(LoaiCongTrinhEnum.CONG_TRINH_NONG_NGHIEP_VA_PHAT_TRIEN_NONG_THON)))
                    loaiCongTrinh = "Nông nghiệp";
                else if (loaiCongTrinh.equals(String.valueOf(LoaiCongTrinhEnum.CONG_TRINH_XAY_DUNG_THEO_QUY_MO_KET_CAU)))
                    loaiCongTrinh = "Xây dựng theo quy mô kết cấu";


                cbLoaiCongTrinh.setValue(loaiCongTrinh);
            }
            if (congTrinhSelected.getTrangThaiCongTrinh() != null) {
                String trangThai = String.valueOf(congTrinhSelected.getTrangThaiCongTrinh());
                if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.DANG_THI_CONG))) trangThai = "Đang thi công";
                else if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.HOAN_THANH))) trangThai = "Hoàn thành";
                else if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.TRE_HAN))) trangThai = "Trễ hạn";
                else if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.TAM_DUNG))) trangThai = "Tạm dừng";
                cbTrangThai.setValue(trangThai);
            }
            if (congTrinhSelected.getDiaChi() != null) {
                DiaChi diaChi = congTrinhSelected.getDiaChi();
                String diaChiChiTiet = String.valueOf(diaChi.getDiaChiChiTiet());
                String khuVuc = String.valueOf(diaChi.getKhuVuc());
                String quanHuyen = String.valueOf(diaChi.getQuanHuyen());
                String thanhPho = String.valueOf(diaChi.getTinhThanhPho());
                if (!isEdit) {
                    txtDiaChi.setText(diaChiChiTiet + " " + khuVuc + " " + quanHuyen + " " + thanhPho);
                } else {
                    txtDiaChiChiTiet.setText(diaChiChiTiet);
                    txtPhuong.setText(khuVuc);
                    txtQuanHuyen.setText(quanHuyen);
                    txtThanhPho.setText(thanhPho);
                }
            }

        }

    }

    public void setCongTrinhs(ObservableList<CongTrinh> congTrinhs) {
        this.congTrinhs = congTrinhs;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            congTrinhDAOImpl = new CongTrinhDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        alertController = new AlertController(stackPane);
        try {
            thamGiaCongTrinhDAO = new PhanCongDAOImpl();
            nhanVienDAO = new NhanVienDAOImpl();
            congViecDAO = new CongViecDAOImpl();
            phongBanDAO = new PhongBanDAOImpl();
            phongBanDAO = new PhongBanDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        isAdd = true;
        cbLoaiCongTrinh.getItems().addAll("Dân Dụng", "Giao thông", "Công nghiệp", "Hạ tầng kĩ thuật", "Nông nghiệp", "Xây dựng theo quy mô kết cấu");
        cbLoaiCongTrinh.setValue("Dân Dụng");

        if (cbPhongBan != null) {
            try {
                phongBanDAO.findAll().forEach(n -> {
                    cbPhongBan.getItems().add(n.getTenPhongBan());
                    cbPhongBan.setValue(n.getTenPhongBan());
                });
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
        cbTrangThai.getItems().addAll("Đang thi công", "Hoàn thành", "Trễ hạn", "Tạm dừng");
        cbTrangThai.setValue("Đang thi công");
        if (txtMaCongTrinh != null) {
            txtMaCongTrinh.setText(congTrinhSelected == null ? this.congTrinhDAOImpl.generateId() : congTrinhSelected.getMaCongTrinh());
        }


    }

    // region Event
    @FXML
    private void onDraged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void onPressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    private void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onSave(ActionEvent actionEvent) {
        if (isAdd) {
            themCongTrinh(actionEvent);
        } else {
            capNhatCongTrinh(actionEvent);
        }
    }

    @FXML
    public void onGiaHan(ActionEvent actionEvent) {
    }
    // endregion

    private void transferData() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/QuanLyCongTrinhScene.fxml"));
            fxmlLoader.load();
            QuanLyCongTrinhController quanLyCongTrinhController = fxmlLoader.getController();
            quanLyCongTrinhController.updateDataTable(this.congTrinhs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức thêm phòng ban
     *
     * @param mouseEvent
     */
    public void themCongTrinh(ActionEvent mouseEvent) {
        DiaChi diaChi = new DiaChi("1", "1", "1", "1");
        String loaiCongTrinh = cbLoaiCongTrinh.getValue();



        System.out.println(loaiCongTrinh);
        LoaiCongTrinhEnum loaiCongTrinhEnum = null;
        if (loaiCongTrinh.equals("Dân Dụng")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_DAN_DUNG;
        else if (loaiCongTrinh.equals("Giao thông")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_GIAO_THONG;
        else if (loaiCongTrinh.equals("Công nghiệp")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_CONG_NGHIEP;
        else if (loaiCongTrinh.equals("Hạ tầng kĩ thuật"))
            loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_HA_TANG_KI_THUAT;
        else if (loaiCongTrinh.equals("Nông nghiệp"))
            loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_NONG_NGHIEP_VA_PHAT_TRIEN_NONG_THON;
        else if (loaiCongTrinh.equals("Xây dựng theo quy mô kết cấu"))
            loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_XAY_DUNG_THEO_QUY_MO_KET_CAU;


        if (txtTenCongTrinh.getText().equals("")) {
            alertController.alertInfo("Chưa nhập tên công trình");
            return;
        }


        if (dateNgayCapPhep.getValue() == null) {
            alertController.alertError("Chưa nhập ngày cấp phép");
            return;
        }

        if (dateNgayHoanThanh.getValue() == null) {
            alertController.alertError("Chưa nhập ngày dự kiến hoàn thành");
            return;
        }

        if (dateNgayThiCong.getValue() == null) {
            alertController.alertError("Chưa nhập ngày thi công");
            return;
        }


        String trangThai = cbTrangThai.getValue();
        TrangThaiCongTrinh trangThaiCongTrinh = null;
        if (trangThai.equals("Đang thi công")) trangThaiCongTrinh = TrangThaiCongTrinh.DANG_THI_CONG;
        else if (trangThai.equals("Hoàn thành")) trangThaiCongTrinh = TrangThaiCongTrinh.HOAN_THANH;
        else if (trangThai.equals("Trễ hạn")) trangThaiCongTrinh = TrangThaiCongTrinh.TRE_HAN;
        else if (trangThai.equals("Tạm dừng")) trangThaiCongTrinh = TrangThaiCongTrinh.TAM_DUNG;
        try {
            CongTrinh congTrinh = new CongTrinh(
                    txtMaCongTrinh.getText().trim(),
                    txtTenCongTrinh.getText().trim(),
                    Date.valueOf(dateNgayCapPhep.getValue()),
                    Date.valueOf(dateNgayThiCong.getValue()),
                    Date.valueOf(dateNgayHoanThanh.getValue()),
                    loaiCongTrinhEnum,
                    trangThaiCongTrinh
                    , diaChi

            );

            System.out.println(congTrinh);

            if (congTrinhDAOImpl.add(congTrinh)) {
                List<CongViec> congViecDangco;
                congViecDangco = congViecDAO.findAll();
//                congViecDangco.forEach(System.out::println);
//                System.out.println("--------------------------");
//                congViecs.forEach(System.out::println);
//                System.out.println("----------------------------------");
                congViecs.forEach(congViec -> {
                    if (!congViecDangco.contains(congViec)) {
                        try {
                            congViecDAO.add(congViec);
                        } catch (DAOException E) {
                            E.printStackTrace();
                        }
                    }
                });
                this.congTrinhs.add(congTrinh);
                transferData();
                onClose(mouseEvent);
                congTrinhFindResult = congTrinhDAOImpl.findAll(0, 20);
                if (paginationCongTrinh.getPageCount() < congTrinhFindResult.getTotalPage()) {
                    paginationCongTrinh.setPageCount(congTrinhFindResult.getTotalPage());
                    paginationCongTrinh.currentPageIndexProperty();
                    paginationCongTrinh.setCurrentPageIndex(congTrinhFindResult.getTotalPage());
                } else {
                    paginationCongTrinh.currentPageIndexProperty();
                    paginationCongTrinh.setCurrentPageIndex(paginationCongTrinh.getPageCount());
                }

            }
        } catch (ValidateException | DAOException e) {
            alertController.alertError(e.getMessage());
        }
    }

    /**
     * Phương thức cập nhật phòng ban
     *
     * @param mouseEvent
     */
    public void capNhatCongTrinh(ActionEvent mouseEvent) {
        try {
            String loaiCongTrinh = cbLoaiCongTrinh.getValue();
            LoaiCongTrinhEnum loaiCongTrinhEnum = null;
            if (loaiCongTrinh.equals("Dân Dụng")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_DAN_DUNG;
            else if (loaiCongTrinh.equals("Giao thông")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_GIAO_THONG;
            else if (loaiCongTrinh.equals("Công nghiệp")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_CONG_NGHIEP;
            else if (loaiCongTrinh.equals("Hạ tầng kĩ thuật"))
                loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_HA_TANG_KI_THUAT;
            else if (loaiCongTrinh.equals("Nông nghiệp"))
                loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_NONG_NGHIEP_VA_PHAT_TRIEN_NONG_THON;
            else if (loaiCongTrinh.equals("Xây dựng theo quy mô kết cấu"))
                loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_XAY_DUNG_THEO_QUY_MO_KET_CAU;


            System.out.println(loaiCongTrinhEnum);

            String trangThai = cbTrangThai.getValue();
            TrangThaiCongTrinh trangThaiCongTrinh = null;
            if (trangThai.equals("Đang thi công")) trangThaiCongTrinh = TrangThaiCongTrinh.DANG_THI_CONG;
            else if (trangThai.equals("Hoàn thành")) trangThaiCongTrinh = TrangThaiCongTrinh.HOAN_THANH;
            else if (trangThai.equals("Trễ hạn")) trangThaiCongTrinh = TrangThaiCongTrinh.TRE_HAN;
            else if (trangThai.equals("Tạm dừng")) trangThaiCongTrinh = TrangThaiCongTrinh.TAM_DUNG;
            CongTrinh congTrinh = new CongTrinh(
                    txtMaCongTrinh.getText().trim(),
                    txtTenCongTrinh.getText().trim(),
                    Date.valueOf(dateNgayCapPhep.getValue()),
                    Date.valueOf(dateNgayThiCong.getValue()),
                    Date.valueOf(dateNgayHoanThanh.getValue()),
                    loaiCongTrinhEnum,
                    trangThaiCongTrinh
                    , new DiaChi(txtDiaChiChiTiet.getText(), txtPhuong.getText(), txtQuanHuyen.getText(), txtThanhPho.getText())
            );

            CongTrinh congTrinhUpdated = congTrinhDAOImpl.update(congTrinh);
            List<CongViec> congViecDangco;
            congViecDangco = congViecDAO.findAllByCongTrinh(congTrinhSelected.getMaCongTrinh());
//            congViecDangco.forEach(System.out::println);
//            System.out.println("--------------------------");
//            congViecs.forEach(System.out::println);
//            System.out.println("----------------------------------");
            congViecs.forEach(congViec -> {
                if (!congViecDangco.contains(congViec)) {
                    try {
                        congViecDAO.add(congViec);
                    } catch (DAOException E) {
                        E.printStackTrace();
                    }
                }
            });

            //System.out.println("công việc bị xóa ");
            congViecDangco.forEach(congViec1 -> {
                if (!congViecs.contains(congViec1)) {
//                    System.out.println("-----------------------------");
                    // System.out.println(congViec1);
                    try {
                        congViecDAO.delete(congViec1);
                    } catch (DAOException E) {
                        E.printStackTrace();
                    }
                }
            });


            congViecs.forEach(congViec -> {
                try {
                    congViecDAO.update(congViec);
                } catch (DAOException e) {
                    e.printStackTrace();
                }
            });


            if (congTrinhUpdated != null) {
                this.congTrinhs.set(congTrinhs.indexOf(congTrinhSelected), congTrinhUpdated);
                //transferData();
                TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
                tblCongTrinh.setRoot(congTrinhTreeItem);
                onClose(mouseEvent);


            }
        } catch (ValidateException | DAOException e) {
            alertController.alertError(e.getMessage());
        }

    }

    private void loadTableNhanVienDaThamGia() {

//        System.out.println("công trình dc chọn");
//        System.out.println(congTrinhSelected);
//        List<NhanVien> nhanVienList = new ArrayList<>();
//        nhanVienList = thamGiaCongTrinhDAO.getListNhanVienDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh());
//        this.listNhanVienDaThamGia = FXCollections.observableList(nhanVienList);
//
//
//        TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienDaThamGia, RecursiveTreeObject::getChildren);
//
//        if (tblNhanVien.getRoot() != null) {
//            tblNhanVien.getRoot().getChildren().clear();
//        }
//        tblNhanVien.setRoot(nhanVienTreeItem);
//        tblNhanVien.setShowRoot(false);
    }

    /**
     * thêm công việc vào bảng
     *
     * @param actionEvent
     */
    @FXML
    private void ThemCongViec(ActionEvent actionEvent) {



        if (txtTenCongViec.getText().equals("")) {
            alertController.alertError("Nhập tên công viện");
            return;
        } else if (txtSoLuongCongViec.getText().equals("")) {
            alertController.alertError("nhập số lương");
            return;
        }

        System.out.println(Pattern.matches("[0-9]", "8"));
        System.out.println(txtSoLuongCongViec.getText());
        System.out.println((Pattern.matches("[0-9]{1,3}$", txtSoLuongCongViec.getText())));

        if (!(Pattern.matches("[0-9]{1,3}$", txtSoLuongCongViec.getText()))) {
            alertController.alertError("Số lượng phải nhập số và không được lớn hơn số nhân viên hiện có");
            return;
        }

        //System.out.println((Pattern.matches("[a-z]", txtSoLuongCongViec.getText())));

        if (dateDuKien.getValue() == null) {
            alertController.alertError("Nhập ngày dự kiến hoàn thành");
            return;
        }
//        if (dateHoanThanh.getValue() == null) {
//            alertController.alertError("Nhập ngày hoàn thành");
//            return;
//        }
        if (txtChiPhi.getText().equals("")) {
            alertController.alertError("Nhập chi phí");
            return;
        }
//
        if (!txtChiPhi.getText().matches("[0-9]{1,12}$")) {
            alertController.alertError("Chi phí phải nhập số");
            return;
        }


        try {
            //System.out.println(phongBanDAO.findByTenPhongBan(cbPhongBan.getValue(), 0, 20).getListResult().get(0));
            PhongBan phongBan = phongBanDAO.findByTenPhongBan(cbPhongBan.getValue(), 0, 20).getListResult().get(0);

            //System.out.println("số lượng");
            int soluongNhanVien = phongBanDAO.tongSoNhanVien(phongBan.getMaPhongBan());
            //System.out.println(soluongNhanVien);
            if (Integer.parseInt(txtSoLuongCongViec.getText()) > soluongNhanVien) {
                alertController.alertError("Số lượng nhân viên cần lớn hơn số nhân viên đang có trong phòng ban");
                return;
            }


            CongViec congViec =null;
            if(dateHoanThanh.getValue()==null){
                 congViec = new CongViec(congViecDAO.generateId(),
                        txtTenCongViec.getText(),
                        Integer.parseInt(txtSoLuongCongViec.getText()),
                        Double.parseDouble(txtChiPhi.getText()),
                        Date.valueOf(dateDuKien.getValue()),
                        null,
                        congTrinhSelected,
                        phongBan);
            }else {
                 congViec = new CongViec(congViecDAO.generateId(),
                        txtTenCongViec.getText(),
                        Integer.parseInt(txtSoLuongCongViec.getText()),
                        Double.parseDouble(txtChiPhi.getText()),
                        Date.valueOf(dateDuKien.getValue()),
                        Date.valueOf(dateHoanThanh.getValue()),
                        congTrinhSelected,
                        phongBan);
            }

            //System.out.println(cong);
            System.out.println(congViec);
            congViecs.add(congViec);
            //congViecs.forEach(System.out::println);

            txtTenCongViec.setText("");
            txtSoLuongCongViec.setText("");
            dateDuKien.setValue(null);
            dateHoanThanh.setValue(null);
            txtChiPhi.setText("");
            cbPhongBan.setValue("");
        } catch (DAOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void SuaCongViec(ActionEvent actionEvent) {
        //System.out.println("sửa công việc");
        CongViec congViecSelected = getItemSelected();
        if (congViecSelected == null) return;


        //try {
        //PhongBan phongBan = phongBanDAO.findByTenPhongBan(cbPhongBan.getValue(), 0, 20).getListResult().get(0);
        // System.out.println(phongBan);
        CongViec congViecUpdate = new CongViec(congViecSelected.getMaCongViec(),
                txtTenCongViec.getText(),
                Integer.parseInt(txtSoLuongCongViec.getText()),
                Double.parseDouble(txtChiPhi.getText()),
                Date.valueOf(dateDuKien.getValue()),
                Date.valueOf(dateHoanThanh.getValue()),
                congTrinhSelected,
                congViecSelected.getPhongBan());

        //System.out.println(congViecUpdate);

        //System.out.println(congTrinhs.indexOf(congViecSelected));
        congViecs.set(congViecs.indexOf(congViecSelected), congViecUpdate);
        TreeItem<CongViec> congViecTreeItem = new RecursiveTreeItem<>(this.congViecs, RecursiveTreeObject::getChildren);
        tblCongViec.setRoot(congViecTreeItem);

//        }catch (DAOException e){
//            e.printStackTrace();
//        }
    }

    @FXML
    private void xoaTrang(ActionEvent actionEvent) {
        txtTenCongViec.setText("");
        txtSoLuongCongViec.setText("");
        cbPhongBan.setValue("");
        txtChiPhi.setText("");
        dateDuKien.setValue(null);
        dateHoanThanh.setValue(null);
    }

    /**
     * xóa công việc
     *
     * @param actionEvent
     */
    @FXML
    private void XoaCongViec(ActionEvent actionEvent) {
        System.out.println("xóa công việc");
        CongViec congViec = getItemSelected();
        if (congViec == null) return;

//        System.out.println(congViec);
//        System.out.println(congViecs.indexOf(congViec));

        //congViecs.forEach(System.out::println);
        if (congViecs.remove(congViec)) {
            System.out.println("đã xóa ");
        }
        //congViecs.forEach(System.out::println);
    }

    /**
     * gia hạn công trình
     *
     * @param actionEvent
     */
    @FXML
    private void GiaHanCongTrinh(ActionEvent actionEvent) {
        try {
            System.out.println(congTrinhSelected);
            congTrinhSelected.setNgayDuKienHoanThanh(Date.valueOf(dateNgayDuKienHoanThanh.getValue()));
            System.out.println(dateNgayDuKienHoanThanh.getValue());
            CongTrinh congTrinhUpdated = congTrinhDAOImpl.update(congTrinhSelected);

            if (congTrinhUpdated != null) {
                this.congTrinhs.set(congTrinhs.indexOf(congTrinhSelected), congTrinhUpdated);
                //transferData();
                TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
                tblCongTrinh.setRoot(congTrinhTreeItem);
                onClose(actionEvent);
            }
        } catch (DAOException | ValidateException e) {
            alertController.alertError(e.getMessage());
        }
    }

    /**
     * phân trang theo công việc
     *
     * @param pageIndex
     * @return
     */
    private Node createPageByMaCongTrinh(int pageIndex) {

        try {
            nhanVienFindResult = thamGiaCongTrinhDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }

    /**
     * phân trang cho công việc
     *
     * @param pageIndex
     * @return
     */
    private Node createPageCongViec(int pageIndex) {
        try {
            congViecFindResult = congViecDAO.findByCongTrinh(congTrinhSelected.getMaCongTrinh(), pageIndex, 20);
            congViecs = FXCollections.observableArrayList(congViecFindResult.getListResult());
            TreeItem<CongViec> congViecTreeItem = new RecursiveTreeItem<>(this.congViecs, RecursiveTreeObject::getChildren);

            if (tblCongViec.getRoot() != null) {
                tblCongViec.getRoot().getChildren().clear();
            }
            tblCongViec.setRoot(congViecTreeItem);
            tblCongViec.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblCongViec;
    }

    /**
     * phân trang cho những danh sách chưa có
     *
     * @param pageIndex
     * @return
     */
    private Node createPageCongViecnull(int pageIndex) {

        congViecList = new ArrayList<>();
        //congViecFindResult = congViecDAO.findByCongTrinh(congTrinhSelected.getMaCongTrinh(), pageIndex, 20);
        congViecs = FXCollections.observableArrayList(congViecList);
        TreeItem<CongViec> congViecTreeItem = new RecursiveTreeItem<>(this.congViecs, RecursiveTreeObject::getChildren);

        if (tblCongViec.getRoot() != null) {
            tblCongViec.getRoot().getChildren().clear();
        }
        tblCongViec.setRoot(congViecTreeItem);
        tblCongViec.setShowRoot(false);

        return tblCongViec;
    }

    /**
     * chọn công việc
     *
     * @return
     */
    private CongViec getItemSelected() {
        TreeTableView.TreeTableViewSelectionModel<CongViec> congViecTreeTableViewSelectionModel = tblCongViec.getSelectionModel();
        if (congViecTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một công trình");
            return null;
        }

        int rowIndex = congViecTreeTableViewSelectionModel.getSelectedIndex();
        CongViec congViec = congViecTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
        return congViec;
    }

    /**
     * hiện thông tin khi nhấn vào bảng
     *
     * @param event
     */
    @FXML
    private void clickTable(MouseEvent event) {
        CongViec congViec = getItemSelected();
        //CongViec congViec =congViecTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
        txtTenCongViec.setText(congViec.getTenCongViec());
        txtSoLuongCongViec.setText(String.valueOf(congViec.getSoLuongNhanVienToiDa()));
        cbPhongBan.setValue(congViec.getTenCongViec());
        dateDuKien.setValue(congViec.getNgayDuKienHoanThanh().toLocalDate());
        dateHoanThanh.setValue(congViec.getNgayHoanThanh().toLocalDate());

        txtChiPhi.setText(String.valueOf(congViec.getChiPhi()));
    }


}
