package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlylaodong.daos.CongTrinhDAOImpl;
import org.buffalocoder.quanlylaodong.daos.CongViecDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhanCongDAOImpl;
import org.buffalocoder.quanlylaodong.entities.CongTrinh;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.ICongTrinhDAO;
import org.buffalocoder.quanlylaodong.interfaces.ICongViecDAO;
import org.buffalocoder.quanlylaodong.interfaces.IPhanCongDAO;
import org.buffalocoder.quanlylaodong.types.LoaiCongTrinhEnum;
import org.buffalocoder.quanlylaodong.types.TrangThaiCongTrinh;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Class:CongTrinhController
 *
 * @author: Đặng Lê Minh Trường, Nguyễn Chí Cường
 * @version: 1.1
 * @description: Dùng để giao tiếp giữa các class giao diện với các lớp DAO
 * @since : 05-10-2019
 */
public class QuanLyCongTrinhController implements Initializable {
    private ICongTrinhDAO congTrinhDAOImpl = null;
    private IPhanCongDAO phanCongDAO;
    private ICongViecDAO congViecDAO;
    private ObservableList<CongTrinh> congTrinhs = FXCollections.observableArrayList();
    private AlertController alertController;
    private Scene scene;
    private Stage stage;
    private Parent root;


    @FXML
    private Pagination Pagination;

    @FXML
    private JFXComboBox<String> cbTimKiem;
    @FXML
    private StackPane stackPane;
    @FXML
    private Pane paneTrangThai;
    @FXML
    private JFXTreeTableView<CongTrinh> tblCongTrinh;
    @FXML
    private TreeTableColumn colMaCongTrinh, colTenCongTrinh, colNgayCapPhep,
            colNgayDuKienHoanThanh, colNgayThiCong, colLoaiCongTrinh, colTrangThai;
    @FXML
    private TreeTableColumn<CongTrinh, String> colDiaChi;
    @FXML
    private TextField txtTimKiem;

    private int soTrang = 5;

    private final int soPhanTu = 20;

    private String keyTimKiem;

    private int trang = 1;
    private FindResult<CongTrinh> congTrinhFindResult;
    private FindResult<NhanVien> nhanVienFindResult;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            congTrinhDAOImpl = new CongTrinhDAOImpl();
            phanCongDAO = new PhanCongDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        congTrinhs = FXCollections.observableArrayList();
        try {
            congViecDAO = new CongViecDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        cbTimKiem.getItems().addAll("Mã công trình", "Tên công trình", "Trạng thái", "Loại công trình");
        cbTimKiem.setValue("Mã công trình");

        txtTimKiem.setOnKeyTyped(e -> {
            keyTimKiem = txtTimKiem.getText();
        });

        alertController = new AlertController(stackPane);


        // init table
        colMaCongTrinh.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, String>("maCongTrinh"));
        colTenCongTrinh.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, String>("tenCongTrinh"));
        colNgayCapPhep.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, LocalDate>("ngayCapGiayPhep"));
        colNgayDuKienHoanThanh.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, LocalDate>("ngayDuKienHoanThanh"));
        colNgayThiCong.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, LocalDate>("ngayThiCong"));
        colLoaiCongTrinh.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, String>("loaiCongTrinh"));
        colDiaChi.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getDiaChi().getDiaChi()));
        colTrangThai.setCellValueFactory(new TreeItemPropertyValueFactory<CongTrinh, String>("trangThaiCongTrinh"));
        // muốn test phân trang thì dùng cái này
        try {
            congTrinhFindResult = congTrinhDAOImpl.findAll(1, 20);
            soTrang = congTrinhFindResult.getTotalPage();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        if (congTrinhFindResult != null) {
            Pagination.setPageCount(soTrang);
            Pagination.setPageFactory(this::createPage);
        } else {
            Pagination.setPageCount(1);
        }
        if (congTrinhFindResult.getListResult().isEmpty()) {
            Pagination.setPageCount(1);
        }


        //updateDataTable(null);
    }

    // region Event

    /**
     * Phương thức bật màn hình thêm công trình
     *
     * @param event
     */
    public void onTimKiem(ActionEvent event) throws DAOException {
        String keyword = keyTimKiem;
        List<CongTrinh> congTrinhs = null;
        try {
            if (keyword.isEmpty()) {
                congTrinhFindResult = congTrinhDAOImpl.findAll(0, 20);
                Pagination.setPageCount(congTrinhFindResult.getTotalPage() + 1);
                Pagination.setPageFactory(this::createPage);
            } else if (cbTimKiem.getValue().equals("Mã công trình")) {
                Pagination.setPageCount(1);
                congTrinhs = new ArrayList<>();
                CongTrinh congTrinh = congTrinhDAOImpl.find(keyword);
                if (congTrinh != null) congTrinhs.add(congTrinh);
                if (congTrinhs.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy công trình");
                } else {
                    ObservableList<CongTrinh> congTrinhObservableList = FXCollections.observableList(congTrinhs);
                    updateDataTable(congTrinhObservableList);
                }
            } else if (cbTimKiem.getValue().equals("Tên công trình")) {
//                congTrinhs = congTrinhDAOImpl.findTenCongTrinh(keyword);
                congTrinhFindResult = congTrinhDAOImpl.findByTenCongTrinh(keyword, 1, 20);
                if (congTrinhFindResult == null) {
                    alertController.alertInfo("Không tìm thấy công trình");
                } else {
                    if (congTrinhFindResult.getListResult().isEmpty()) {
                        Pagination.setPageCount(1);
                    } else {
                        Pagination.setPageCount(congTrinhFindResult.getTotalPage());
                    }
                    Pagination.setPageFactory(this::createPageByName);
                }
            } else if (cbTimKiem.getValue().equals("Trạng thái")) {
                TrangThaiCongTrinh trangThaiCongTrinh = null;
                System.out.println(keyTimKiem);
                if (keyTimKiem.equals("Đang thi công")) trangThaiCongTrinh = TrangThaiCongTrinh.DANG_THI_CONG;
                else if (keyTimKiem.equals("Hoàn thành")) trangThaiCongTrinh = TrangThaiCongTrinh.HOAN_THANH;
                else if (keyTimKiem.equals("Trễ hạn")) trangThaiCongTrinh = TrangThaiCongTrinh.TRE_HAN;
                else if (keyTimKiem.equals("Tạm dừng")) trangThaiCongTrinh = TrangThaiCongTrinh.TAM_DUNG;
                System.out.println(trangThaiCongTrinh.ordinal());
                congTrinhFindResult = congTrinhDAOImpl.findByTrangThai(trangThaiCongTrinh, 0, 20);
                if (congTrinhFindResult == null) {
                    alertController.alertInfo("Không tìm thấy công trình");
                } else {
                    if (congTrinhFindResult.getListResult().isEmpty()) {
                        Pagination.setPageCount(1);
                    } else {
                        Pagination.setPageCount(congTrinhFindResult.getTotalPage());
                    }
                    Pagination.setPageFactory(this::createPageByStatus);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức chuyển tìm kiếm sang trạng thái
     *
     * @param event
     * @return
     */
    @FXML
    public void onTimKiemTrangThai(ActionEvent event) {
        if (cbTimKiem.getValue().equals("Trạng thái")) {
            paneTrangThai.getChildren().remove(0);
            ComboBox<String> cb = new ComboBox<>();
            cb.getItems().addAll("Đang thi công", "Tạm dừng", "Hoàn thành", "Trễ hạn");
            cb.setValue("Đang thi công");
            keyTimKiem = cb.getValue();
            cb.setPrefWidth(156);
            cb.setOnAction(e -> {
                try {
                    keyTimKiem = cb.getValue();
                    TrangThaiCongTrinh trangThaiCongTrinh = null;
                    if (keyTimKiem.equals("Đang thi công")) trangThaiCongTrinh = TrangThaiCongTrinh.DANG_THI_CONG;
                    else if (keyTimKiem.equals("Hoàn thành")) trangThaiCongTrinh = TrangThaiCongTrinh.HOAN_THANH;
                    else if (keyTimKiem.equals("Trễ hạn")) trangThaiCongTrinh = TrangThaiCongTrinh.TRE_HAN;
                    else if (keyTimKiem.equals("Tạm dừng")) trangThaiCongTrinh = TrangThaiCongTrinh.TAM_DUNG;
                    System.out.println(trangThaiCongTrinh.ordinal());
                    congTrinhFindResult = congTrinhDAOImpl.findByTrangThai(trangThaiCongTrinh, 0, 20);
                    if (congTrinhFindResult == null) {
                        alertController.alertInfo("Không tìm thấy công trình");
                    } else {
                        if (congTrinhFindResult.getListResult().isEmpty()) {
                            Pagination.setPageCount(1);
                        } else {
                            Pagination.setPageCount(congTrinhFindResult.getTotalPage());
                        }
                        Pagination.setPageFactory(this::createPageByStatus);
                    }
                } catch (DAOException EX) {
                    EX.printStackTrace();
                }

            });
            paneTrangThai.getChildren().add(cb);
        } else if (cbTimKiem.getValue().equals("Loại công trình")) {

            paneTrangThai.getChildren().remove(0);
            ComboBox<String> cb1 = new ComboBox<>();
            cb1.getItems().addAll("Dân Dụng", "Giao thông", "Công Ngiệp", "Hạ tầng kĩ thuật", "Nông nghiệp", "Xây dựng theo quy mô kết cấu");
            cb1.setValue("Dân Dụng");
            keyTimKiem = cb1.getValue();
            cb1.setPrefWidth(156);
            cb1.setOnAction(e -> {

                try {
                    keyTimKiem = cb1.getValue();
                    // System.out.println(keyTimKiem);
                    LoaiCongTrinhEnum loaiCongTrinhEnum = null;
                    if (keyTimKiem.equals("Dân Dụng")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_DAN_DUNG;
                    else if (keyTimKiem.equals("Giao thông"))
                        loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_GIAO_THONG;
                    else if (keyTimKiem.equals("Công Ngiệp"))
                        loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_CONG_NGHIEP;
                    else if (keyTimKiem.equals("Hạ tầng kĩ thuật"))
                        loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_HA_TANG_KI_THUAT;
                    else if (keyTimKiem.equals("Nông nghiệp"))
                        loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_NONG_NGHIEP_VA_PHAT_TRIEN_NONG_THON;
                    else if (keyTimKiem.equals("Xây dựng theo quy mô kết cấu"))
                        loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_XAY_DUNG_THEO_QUY_MO_KET_CAU;
                    congTrinhFindResult = congTrinhDAOImpl.findByLoaiCongTrinh(loaiCongTrinhEnum, 0, 20);

                    if (congTrinhFindResult == null) {
                        alertController.alertInfo("Không tìm thấy công trình");
                    } else {
                        if (congTrinhFindResult.getListResult().isEmpty()) {
                            Pagination.setPageCount(1);
                        } else {
                            Pagination.setPageCount(congTrinhFindResult.getTotalPage());
                        }
                        Pagination.setPageFactory(this::createPageByLoaiCongTrinh);
                    }
                } catch (DAOException ex) {
                    ex.printStackTrace();
                }
            });
            paneTrangThai.getChildren().add(cb1);
        } else {
            paneTrangThai.getChildren().remove(0);
            TextField txtTimKiem = new TextField();
            txtTimKiem.setPrefWidth(156);
            txtTimKiem.setPrefHeight(31);
            txtTimKiem.setOnKeyTyped(e -> {
                keyTimKiem = txtTimKiem.getText();
            });
            paneTrangThai.getChildren().add(txtTimKiem);
        }


    }


    /**
     * Phương thức bật màn hình thêm công trình
     *
     * @param actionEvent
     */
    @FXML
    public void onThemCongTrinh(ActionEvent actionEvent) {
        modifierCongTrinh(actionEvent, false);
    }

    /**
     * Phương thức bật màn hình xóa công trình
     *
     * @param actionEvent
     */
    @FXML
    public void onXoaCongTrinh(ActionEvent actionEvent) {
        CongTrinh congTrinh = getItemSelected();

        try {
            nhanVienFindResult = phanCongDAO.findDanhSachDaThamGiaCongTrinh(congTrinh.getMaCongTrinh(), 0, 20);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        System.out.println(nhanVienFindResult);
        //System.out.println(nhanVienFindResult.getListResult());
        if (nhanVienFindResult != null) {
            //kiem tra neu cong trinh da thi cong thi khong duoc xoa
            alertController.alertError(String.format("Công trình %s đang có công nhân đang thi công. Không thể xóa công trình này",
                    congTrinh.getTenCongTrinh()));
            return;

        }

        if (nhanVienFindResult == null) {

            // kiem tra neu cong trinh da thi cong thi khong duoc xoa
            if (congTrinh.getTrangThaiCongTrinh() == TrangThaiCongTrinh.DANG_THI_CONG) {
                alertController.alertError(String.format("Công trình %s đang thi công. Không thể xóa công trình này",
                        congTrinh.getTenCongTrinh()));
                return;
            }


            try {
//                        if (thamGiaCongTrinhs.size() > 0) {
//                            for (PhanCong thamGiaCongTrinh : thamGiaCongTrinhs) {
//                                thamGiaCongTrinhDAO.delete(thamGiaCongTrinh);
//                            }
//                        }

                this.congTrinhDAOImpl.delete(congTrinh);
                this.congTrinhs.remove(congTrinh);
                updateDataTable(this.congTrinhs);
            } catch (DAOException e) {
                alertController.alertError(e.getMessage());
            }


            //kiem tra co nhan vien vao lam cong trinh nay chua
            try {
                phanCongDAO = new PhanCongDAOImpl();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            //List<PhanCong> thamGiaCongTrinhs = thamGiaCongTrinhDAO.findByMaCongTrinh(congTrinh.getMaCongTrinh());

            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            dialogLayout.setHeading(new Text("Cảnh báo"));

            dialogLayout.setBody(new Text(
                    String.format("Bạn có muốn xóa công trình %s này không ?",
                            congTrinh.getTenCongTrinh())));
            // ,thamGiaCongTrinhs.size() > 0 ? "Thao tác này sẽ xóa dữ liệu phân công và bảng chấm công của nhân viên" : "")));

            JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

            JFXButton btn_Ok = new JFXButton();
            btn_Ok.setText("Đồng ý");
            btn_Ok.setOnAction(event -> {
//                   try {
////                        if (thamGiaCongTrinhs.size() > 0) {
////                            for (PhanCong thamGiaCongTrinh : thamGiaCongTrinhs) {
////                                thamGiaCongTrinhDAO.delete(thamGiaCongTrinh);
////                            }
////                        }
//
////                        this.congTrinhDAOImpl.delete(congTrinh);
////                        this.congTrinhs.remove(congTrinh);
////                        updateDataTable(this.congTrinhs);
//                    } catch (DAOException e) {
//                        alertController.alertError(e.getMessage());
//                    }
                // System.out.println("xóa");
                dialog.close();
            });
            JFXButton btn_Cancel = new JFXButton();
            btn_Cancel.setText("Huỷ");
            btn_Cancel.setOnAction(e -> dialog.close());
            dialogLayout.setActions(btn_Ok, btn_Cancel);

            dialog.show();
        }

    }

    /**
     * Phương thức cập nhật thông tin công trình
     *
     * @param actionEvent
     */
    @FXML
    public void onSuaCongTrinh(ActionEvent actionEvent) {
        modifierCongTrinh(actionEvent, true);
    }

    /**
     * Phương thức bật màn hình gia hạn công trình
     *
     * @param actionEvent
     */
    @FXML
    public void onGiaHan(ActionEvent actionEvent) {
    }


    /**
     * Phương thức bật màn hình phân công
     *
     * @param actionEvent
     */
    @FXML
    public void onPhanCongCongTrinh(ActionEvent actionEvent) throws DAOException {
        CongTrinh congTrinh = getItemSelected();
        if (congTrinh == null) return;

        if (congViecDAO.findAllByCongTrinh(congTrinh.getMaCongTrinh()).isEmpty()) {
            alertController.alertError("Công trình chưa có công việc để phân công ");
            return;
        }

        try {


            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/PhanCongScene.fxml"));
            root = fxmlLoader.load();

            PhanCongController phanCongController = fxmlLoader.getController();
            try {
                phanCongController.setCongTrinhSelected(congTrinh);
            } catch (DAOException e) {
                e.printStackTrace();
            }

            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 1280, 800);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức bật màn hình xem chi tiết công trình
     *
     * @param actionEvent
     */
    @FXML
    private void onXemChiTiet(ActionEvent actionEvent) {
        CongTrinh congTrinh = getItemSelected();

        if (congTrinh == null) return;

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/XemCTCongTrinhScene.fxml"));
            root = fxmlLoader.load();

            ModifierCongTrinhController modifierCongTrinhController = fxmlLoader.getController();
            modifierCongTrinhController.setCongTrinhSelected(congTrinh, false, true);


            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 900, 702);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // endregion

    /**
     * Xử lý màn hình thêm, sửa công trình
     *
     * @param actionEvent
     * @param isEdit
     */
    private void modifierCongTrinh(ActionEvent actionEvent, boolean isEdit) {
        CongTrinh congTrinh = null;
        if (isEdit) {
            congTrinh = getItemSelected();

            if (congTrinh == null) return;
        }

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/ModifierCongTrinhScene.fxml"));
            root = fxmlLoader.load();

            ModifierCongTrinhController modifierCongTrinhController = fxmlLoader.getController();

            //modifierCongTrinhController.setCongTrinhSelected();
            if (!isEdit) {
                modifierCongTrinhController.setCongTrinhs(this.congTrinhs);
                modifierCongTrinhController.setCongTrinhSelected();
                modifierCongTrinhController.setTblCongTrinh(tblCongTrinh);
                modifierCongTrinhController.setPaginationCongTrinh(Pagination);
            }

            if (isEdit) {
                modifierCongTrinhController.setCongTrinhSelected(congTrinh, isEdit, false);
                modifierCongTrinhController.setTblCongTrinh(tblCongTrinh);
                modifierCongTrinhController.setCongTrinhs(this.congTrinhs);
            }

            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 1000, 837);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức lấy phần tử đang được chọn trên bảng
     *
     * @return
     */
    private CongTrinh getItemSelected() {
        TreeTableView.TreeTableViewSelectionModel<CongTrinh> congTrinhTreeTableViewSelectionModel = tblCongTrinh.getSelectionModel();
        if (congTrinhTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một công trình");
            return null;
        }
        int rowIndex = congTrinhTreeTableViewSelectionModel.getSelectedIndex();
        return congTrinhTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
    }

    /**
     * Phương thức cập nhật data table
     *
     * @param congTrinhs
     */
    public void updateDataTable(ObservableList<CongTrinh> congTrinhs) {
//        this.congTrinhs = (congTrinhs == null) ? FXCollections.observableArrayList(this.congTrinhDAOImpl.find()) : congTrinhs;
        this.congTrinhs = congTrinhs;
        TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);

        if (tblCongTrinh.getRoot() != null) {
            tblCongTrinh.getRoot().getChildren().clear();
        }
        tblCongTrinh.setRoot(congTrinhTreeItem);
        tblCongTrinh.setShowRoot(false);
    }

    /**
     * phương thức phân trang
     *
     * @param pageIndex
     * @return
     */
    private Node createPage(int pageIndex) {
        try {
            congTrinhFindResult = congTrinhDAOImpl.findAll(pageIndex, 20);
            congTrinhs = FXCollections.observableArrayList(congTrinhFindResult.getListResult());
            TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
            if (tblCongTrinh.getRoot() != null) {
                tblCongTrinh.getRoot().getChildren().clear();
            }
            tblCongTrinh.setRoot(congTrinhTreeItem);
            tblCongTrinh.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();

        }
        return tblCongTrinh;
    }

    /**
     * Gia hạn công trình
     *
     * @param actionEvent
     */
    @FXML
    private void onGiaHanCongTrinh(ActionEvent actionEvent) {

        CongTrinh congTrinh = getItemSelected();

        if (congTrinh == null) return;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/GiaHanCongTrinhScene.fxml"));
            root = fxmlLoader.load();

            ModifierCongTrinhController modifierCongTrinhController = fxmlLoader.getController();
            modifierCongTrinhController.setCongTrinhSelected(congTrinh);
            modifierCongTrinhController.setCongTrinhs(this.congTrinhs);


            scene = new Scene(root, 600, 400);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Phương thức phân trang theo tên
     *
     * @param pageIndex
     * @return
     */
    private Node createPageByName(int pageIndex) {
        try {

            String tenCongTrinh = txtTimKiem.getText();
            congTrinhFindResult = congTrinhDAOImpl.findByTenCongTrinh(tenCongTrinh, pageIndex, 20);
            congTrinhs = FXCollections.observableArrayList(congTrinhFindResult.getListResult());
            TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
            if (tblCongTrinh.getRoot() != null) {
                tblCongTrinh.getRoot().getChildren().clear();
            }
            tblCongTrinh.setRoot(congTrinhTreeItem);
            tblCongTrinh.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();

        }
        return tblCongTrinh;
    }

    /**
     * Phương thức phân trang theo trạng tháo
     *
     * @param pageIndex
     * @return
     */
    private Node createPageByStatus(int pageIndex) {
        try {

            TrangThaiCongTrinh trangThaiCongTrinh = null;
            if (keyTimKiem.equals("Đang thi công")) trangThaiCongTrinh = TrangThaiCongTrinh.DANG_THI_CONG;
            else if (keyTimKiem.equals("Hoàn thành")) trangThaiCongTrinh = TrangThaiCongTrinh.HOAN_THANH;
            else if (keyTimKiem.equals("Trễ hạn")) trangThaiCongTrinh = TrangThaiCongTrinh.TRE_HAN;
            else if (keyTimKiem.equals("Tạm dừng")) trangThaiCongTrinh = TrangThaiCongTrinh.TAM_DUNG;
            congTrinhFindResult = congTrinhDAOImpl.findByTrangThai(trangThaiCongTrinh, pageIndex, 20);
            congTrinhs = FXCollections.observableArrayList(congTrinhFindResult.getListResult());
            TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
            if (tblCongTrinh.getRoot() != null) {
                tblCongTrinh.getRoot().getChildren().clear();
            }
            tblCongTrinh.setRoot(congTrinhTreeItem);
            tblCongTrinh.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();

        }
        return tblCongTrinh;
    }


    private Node createPageByLoaiCongTrinh(int pageIndex) {
        try {
            LoaiCongTrinhEnum loaiCongTrinhEnum = null;
            if (keyTimKiem.equals("Dân Dụng")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_DAN_DUNG;
            else if (keyTimKiem.equals("Giao thông")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_GIAO_THONG;
            else if (keyTimKiem.equals("Công Ngiệp")) loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_CONG_NGHIEP;
            else if (keyTimKiem.equals("Hạ tầng kĩ thuật"))
                loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_HA_TANG_KI_THUAT;
            else if (keyTimKiem.equals("Nông nghiệp"))
                loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_NONG_NGHIEP_VA_PHAT_TRIEN_NONG_THON;
            else if (keyTimKiem.equals("Xây dựng theo quy mô kết cấu"))
                loaiCongTrinhEnum = LoaiCongTrinhEnum.CONG_TRINH_XAY_DUNG_THEO_QUY_MO_KET_CAU;
            congTrinhFindResult = congTrinhDAOImpl.findByLoaiCongTrinh(loaiCongTrinhEnum, pageIndex, 20);

            congTrinhs = FXCollections.observableArrayList(congTrinhFindResult.getListResult());
            TreeItem<CongTrinh> congTrinhTreeItem = new RecursiveTreeItem<>(this.congTrinhs, RecursiveTreeObject::getChildren);
            if (tblCongTrinh.getRoot() != null) {
                tblCongTrinh.getRoot().getChildren().clear();
            }
            tblCongTrinh.setRoot(congTrinhTreeItem);
            tblCongTrinh.setShowRoot(false);

        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblCongTrinh;
    }
}
