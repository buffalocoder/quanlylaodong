package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.buffalocoder.quanlylaodong.constant.CongTrinhNhanVienThamGia;
import org.buffalocoder.quanlylaodong.daos.CongViecDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhanCongDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhongBanDAOImpl;
import org.buffalocoder.quanlylaodong.entities.*;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;
import org.buffalocoder.quanlylaodong.interfaces.ICongViecDAO;
import org.buffalocoder.quanlylaodong.interfaces.IPhanCongDAO;
import org.buffalocoder.quanlylaodong.interfaces.IPhongBanDAO;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class PhanCongController implements Initializable {
    private double x, y;
    private AlertController alertController;
    private IPhanCongDAO phanCongDAO;
    private IPhongBanDAO phongBanDAO;
    private ICongViecDAO congViecDAO;
    private ObservableList<NhanVien> listNhanVienChuaThamGia;
    private ObservableList<NhanVien> listNhanVienDaThamGia;
    private CongTrinh congTrinhSelected;
    private FindResult<NhanVien> nhanVienChuaThamGiaFindResult;
    private FindResult<NhanVien> nhanVienThamGiaFindResult;
    private FindResult<NhanVien> nhanVienThamGiaTheoCongViecFindResult;
    private List<NhanVien> nhanVienList;
    private CongViec congViecTrongCongTrinh;
    private List<NhanVien> nhanVienListThamGiaCongViecNull;
    private FindResult<CongViec> congViecFindResult;
    private ObservableList<CongViec> congViecs = FXCollections.observableArrayList();
    private List<PhanCong> phanCongs = new ArrayList<>();

    @FXML
    private JFXTreeTableView<CongViec> tblCongViec;
    @FXML
    private Pagination paginationCongViec;
    @FXML
    private TreeTableColumn colTenCongViec, colSoluongNhanVien, colChiPhi, colDuKien, colHoanThanh;
    @FXML
    private TreeTableColumn<CongViec, String> colMaPhongBan;
    @FXML
    private Label labCongviec;
    @FXML
    private JFXComboBox<String> cbPhongBan, cbCongViec;
    @FXML
    private Pagination paginationNhanVienChuaThamGia;
    @FXML
    private Pagination paginationNhanVienDaThamGia;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTreeTableView tblNhanVienDaThamGia, tblNhanVienChuaThamGia;
    @FXML
    private TreeTableColumn<NhanVien, String> colMaNhanVienDaThamGia;
    @FXML
    private TreeTableColumn<NhanVien, String> colHoTenNhanVienDaThamGia;
    @FXML
    private TreeTableColumn<NhanVien, String> colPhongBanNhanVienDaThamGia;
    @FXML
    private TreeTableColumn<NhanVien, String> colMaNhanVienChuaThamGia;
    @FXML
    private TreeTableColumn<NhanVien, String> colHoTenNhanVienChuaThamGia;
    @FXML
    private TreeTableColumn<NhanVien, String> colPhongBanNhanVienChuaThamGia;
    @FXML
    private JFXDatePicker dateBatDau, dateKetThuc;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        try {
            phanCongDAO = new PhanCongDAOImpl();
            phongBanDAO = new PhongBanDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        try {
            congViecDAO = new CongViecDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        nhanVienList = new ArrayList<>();

        // init table nhân viên chưa tham gia công trình
        colMaNhanVienChuaThamGia.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("maNhanVien"));
        colHoTenNhanVienChuaThamGia.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("hoTen"));
        colPhongBanNhanVienChuaThamGia.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getPhongBan().getTenPhongBan())
        );

        // init table nhân viên đã tham gia công trình
        colMaNhanVienDaThamGia.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("maNhanVien"));
        colHoTenNhanVienDaThamGia.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("hoTen"));
        colPhongBanNhanVienDaThamGia.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getPhongBan().getTenPhongBan())
        );

    }

    // region Event

    /**
     * Phương thức xử lý khi kéo cửa sổ
     *
     * @param event
     */
    @FXML
    private void onDragged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào cửa sổ
     *
     * @param event
     */
    @FXML
    private void onPressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    /**
     * Phương thức đóng cửa sổ
     *
     * @param actionEvent
     */
    @FXML
    private void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Phương thức xử lý tìm kiếm danh sách nhân viên chưa tham gia
     *
     * @param actionEvent
     */
    @FXML
    private void onTimKiemNhanVienChuaThamGia(ActionEvent actionEvent) {
    }

    /**
     * Phương thức xử lý tìm kiếm danh sách nhân viên đã tham gia
     *
     * @param actionEvent
     */
    @FXML
    private void onTimKiemNhanVienDaThamGia(ActionEvent actionEvent) {
    }

    /**
     * Phương thức xử lý phân công
     *
     * @param actionEvent
     */
    @FXML
    private void onPhanCong(ActionEvent actionEvent) throws DAOException {
        NhanVien nhanVien = getItemSelectedTableNhanVienChuaThamGia();
        if (nhanVien == null) return;
        else System.out.println(nhanVien);

        //List<>


        if (phanCongDAO.findDanhSachCongTrinhNhanVienDangThamGia(nhanVien.getMaNhanVien(), 0, Integer.MAX_VALUE) != null) {
            phanCongs = phanCongDAO.findDanhSachCongTrinhNhanVienDangThamGia(nhanVien.getMaNhanVien(), 0, Integer.MAX_VALUE).getListResult();
        }
        List<CongTrinh> congTrinhList = new ArrayList<>();
        if (phanCongs != null) {
            phanCongs.forEach(x -> {
                //System.out.println(x.getPkCongViecNhanVien().getCongViec().getCongTrinh());
                if (!congTrinhList.contains(x.getPkCongViecNhanVien().getCongViec().getCongTrinh())) {
                    congTrinhList.add(x.getPkCongViecNhanVien().getCongViec().getCongTrinh());
                }
            });

//            System.out.println("--------------------------------");
//            congTrinhList.forEach(x -> {
//                System.out.println(x);
//            });
        }
//        if(phanCongs.isEmpty()||congTrinhList.size()+1<=5){
//            System.out.println("Phù hợp");
//
//        }
        if (congTrinhList.size() + 1 >= CongTrinhNhanVienThamGia.SoLuongCongTrinh) {
            alertController.alertInfo("Nhân viên đã tham gia đủ công trình (tổng số công trình nhân viên không quá 5) ");
            return;
        }

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Nhắc nhở"));

        dialogLayout.setBody(new Text(
                String.format("Bạn có phân công nhân viên ?\n%s",
                        nhanVien.getHoTen(), "Thao tác này sẽ add nhân viên vào bảng phân công")));

        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(event -> {
            try {

                int soluong = phanCongDAO.findDanhSachThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec()).size();
//                System.out.println("số lượng nhân viên hiện có");
//                System.out.println(soluong);
//                System.out.println("số lương nhân viên cần có trong công ty");
//                System.out.println(congViecTrongCongTrinh.getSoLuongNhanVienToiDa());

                if (soluong + 1 <= congViecTrongCongTrinh.getSoLuongNhanVienToiDa()) {
                    listNhanVienChuaThamGia.remove(nhanVien);
                    listNhanVienDaThamGia.add(nhanVien);
//                    System.out.println("---------------------------------");
//                    System.out.println(congViecTrongCongTrinh);
                    PhanCong phanCong = new PhanCong(congViecTrongCongTrinh, nhanVien);
                    phanCongDAO.add(phanCong);
                } else {
                    dialog.close();
                    alertController.alertError("đã đủ nhân viên");
                    return;
                }

//                if(nhanVien.getPhongBan().getMaPhongBan().equals(congViecTrongCongTrinh.getPhongBan().getMaPhongBan())) {
//                    listNhanVienChuaThamGia.remove(nhanVien);
//                    listNhanVienDaThamGia.add(nhanVien);
//                    System.out.println("---------------------------------");
//                    System.out.println(congViecTrongCongTrinh);
//                    PhanCong phanCong= new PhanCong(congViecTrongCongTrinh,nhanVien);
//                    phanCongDAO.add(phanCong);
//                }else {
//                    alertController.alertError("nhân viên không thuộc phòng ban mà công việc cần");
//                    return;
//                }

                // PhanCong phanCong = new PhanCong(congViecTrongCongTrinh,nhanVien,Date.,Date.valueOf(LocalDate.now()));
                //phanCongDAO.add(phanCong);
            } catch (Exception e) {
                alertController.alertError(e.getMessage());
            }
            dialog.close();
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();
//
//        loadTableNhanVienDaThamGia(this.listNhanVienDaThamGia);
//        loadTableNhanVienChuaThamGia(this.listNhanVienChuaThamGia);
    }
    // endregion

    /**
     * Phương thức xử lý bỏ phân công
     *
     * @param actionEvent
     */
    @FXML
    private void onBoPhanCong(ActionEvent actionEvent) throws DAOException {
        NhanVien nhanVien = getItemSelectedTableNhanVienDaThamGia();
        if (nhanVien == null) return;

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Nhắc nhở"));

        dialogLayout.setBody(new Text(
                String.format("Bạn có bỏ phân công nhân viên ?\n%s",
                        nhanVien.getHoTen(), "Thao tác này sẽ bỏ nhân viên ra khỏi bảng phân công")));

        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(event -> {
            try {
                PhanCong phanCong = phanCongDAO.findByMaCongViecMaNhanVien(congViecTrongCongTrinh.getMaCongViec(), nhanVien.getMaNhanVien());
                System.out.println(phanCong);
                if (phanCongDAO.delete(phanCong)) {
                    listNhanVienDaThamGia.remove(nhanVien);
                    listNhanVienChuaThamGia.add(nhanVien);
                } else {
                    alertController.alertError("Không xóa được nhân viên");
                }
                //PhanCong phanCong= new PhanCong(congViecTrongCongTrinh,nhanVien);
                //phanCongDAO.delete(phanCong)


            } catch (Exception e) {
                alertController.alertError(e.getMessage());
            }
            dialog.close();
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();

//        loadTableNhanVienDaThamGia(this.listNhanVienDaThamGia);
//        loadTableNhanVienChuaThamGia(this.listNhanVienChuaThamGia);
    }

    /**
     * save danh sách nhân viên tham gia
     *
     * @param actionEvent
     */
    @FXML
    private void onClickSave(ActionEvent actionEvent) {
        List<NhanVien> nhanViens = null;

        // xử lý lưu phân công
        try {
            nhanViens = phanCongDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh());
            for (NhanVien nhanVien : listNhanVienDaThamGia) {
                if (!nhanViens.contains(nhanVien)) {
//                    PhanCong phanCong = new PhanCong(
//                            congTrinhSelected, nhanVien,
//                            congTrinhSelected.getNgayThiCong(), congTrinhSelected.getNgayDuKienHoanThanh());
//                    thamGiaCongTrinhDAO.add(phanCong);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // xử lý bỏ phân công
        try {
            nhanViens = phanCongDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh());
            for (NhanVien nhanVien : nhanViens) {
//                if (!listNhanVienDaThamGia.contains(nhanVien)) {
//                    System.out.println(nhanVien);
//                    thamGiaCongTrinhDAO.deleteByMaNhanVienMaCongTrinh(nhanVien.getMaNhanVien(), congTrinhSelected.getMaCongTrinh());
//                }
                System.out.println(nhanVien);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        alertController.alertInfo("Lưu thành công");
    }

    // region Methods

    /**
     * Phương thức lấy phần tử đang được chọn trên bảng nhân viên chưa tham gia
     *
     * @return NhanVien
     */
    private NhanVien getItemSelectedTableNhanVienChuaThamGia() {
        TreeTableView.TreeTableViewSelectionModel<NhanVien> congTrinhTreeTableViewSelectionModel = tblNhanVienChuaThamGia.getSelectionModel();
        if (congTrinhTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một nhân viên");
            return null;
        }
        int rowIndex = congTrinhTreeTableViewSelectionModel.getSelectedIndex();
        return congTrinhTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
    }

    /**
     * Phương thức lấy phần tử đang được chọn trên bảng nhân viên đã tham gia
     *
     * @return NhanVien
     */
    private NhanVien getItemSelectedTableNhanVienDaThamGia() {
        TreeTableView.TreeTableViewSelectionModel<NhanVien> congTrinhTreeTableViewSelectionModel = tblNhanVienDaThamGia.getSelectionModel();
        if (congTrinhTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một nhân viên");
            return null;
        }
        int rowIndex = congTrinhTreeTableViewSelectionModel.getSelectedIndex();
        return congTrinhTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
    }

    /**
     * Phương thức truyền công trình được chọn từ cha
     *
     * @param congTrinhSelected
     */
    public void setCongTrinhSelected(CongTrinh congTrinhSelected) throws DAOException {
        this.congTrinhSelected = congTrinhSelected;

        setTableCongViec();

//        congViecDAO.findAllByCongTrinh(congTrinhSelected.getMaCongTrinh()).forEach(congViec -> {
//            cbCongViec.getItems().add(congViec.getMaCongViec());
//            cbCongViec.setValue(congViec.getMaCongViec());
//        });
//        loadTableNhanVienDaThamGia(null);
//        loadTableNhanVienChuaThamGia(null);


    }

    private void setTableCongViec() {
        colTenCongViec.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, String>("tenCongViec"));
        colSoluongNhanVien.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, Integer>("soLuongNhanVienToiDa"));
        colChiPhi.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, Integer>("chiPhi"));
        colDuKien.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, LocalDate>("ngayDuKienHoanThanh"));
        colHoanThanh.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, LocalDate>("ngayHoanThanh"));
        colMaPhongBan.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getPhongBan().getTenPhongBan()));
        try {
            congViecFindResult = congViecDAO.findByCongTrinh(congTrinhSelected.getMaCongTrinh(), 0, 20);
            if (congViecFindResult != null) {
                paginationCongViec.setPageCount(congViecFindResult.getTotalPage());
                paginationCongViec.setPageFactory(this::createPageCongViec);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức hiển thị danh sách nhân viên đã tham gia lên table
     *
     * @param nhanViens
     */
    private void loadTableNhanVienDaThamGia(ObservableList<NhanVien> nhanViens) throws DAOException {
//        if (nhanViens == null) {
//            List<NhanVien> nhanVienList = thamGiaCongTrinhDAO.getListNhanVienDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh());
//            this.listNhanVienDaThamGia = FXCollections.observableList(nhanVienList);
//        } else {
//            this.listNhanVienDaThamGia = nhanViens;
//        }
//
//        TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienDaThamGia, RecursiveTreeObject::getChildren);
//
//        if (tblNhanVienDaThamGia.getRoot() != null) {
//            tblNhanVienDaThamGia.getRoot().getChildren().clear();
//        }
//        tblNhanVienDaThamGia.setRoot(nhanVienTreeItem);
//        tblNhanVienDaThamGia.setShowRoot(false);

//        try {
//            nhanVienThamGiaFindResult=phanCongDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(), 1, 40);
//            if(nhanVienThamGiaFindResult!=null) {
//                paginationNhanVienDaThamGia.setPageCount(nhanVienChuaThamGiaFindResult.getTotalPage());
//                paginationNhanVienDaThamGia.setPageFactory(this::createPageDaThamGia);
//            }else {
//                paginationNhanVienDaThamGia.setPageCount(1);
//                paginationNhanVienDaThamGia.setPageFactory(this::createPageDaThamGiaNull);
//            }
//        }catch (DAOException e){
//            e.printStackTrace();
//        }

        try {
            System.out.println(congTrinhSelected);

            //nhanVienThamGiaFindResult = phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20);
            congViecTrongCongTrinh = congViecDAO.find(cbCongViec.getValue());
            labCongviec.setText(congViecTrongCongTrinh.getTenCongViec());
            if (phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20) != null) {
                paginationNhanVienDaThamGia.setPageCount(phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20).getTotalPage());
                paginationNhanVienDaThamGia.setPageFactory(this::createNhanVienByCongViec);
            } else {
                paginationNhanVienDaThamGia.setPageCount(1);
                paginationNhanVienDaThamGia.setPageFactory(this::createNhanVienByCongViecNull);
            }
        } catch (DAOException | NotExistException e) {
            e.printStackTrace();
        }


    }

    /**
     * Phương thức hiển thị danh sách nhân viên chưa tham gia lên table
     *
     * @param nhanViens
     */
    private void loadTableNhanVienChuaThamGia(ObservableList<NhanVien> nhanViens) {
//        if (nhanViens == null) {
//            List<NhanVien> nhanVienList = thamGiaCongTrinhDAO.getListNhanVienChuaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh());
//            this.listNhanVienChuaThamGia = FXCollections.observableList(nhanVienList);
//        } else {
//            this.listNhanVienChuaThamGia = nhanViens;
//        }
//
//        TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienChuaThamGia, RecursiveTreeObject::getChildren);
//
//        if (tblNhanVienChuaThamGia.getRoot() != null) {
//            tblNhanVienChuaThamGia.getRoot().getChildren().clear();
//        }
//        tblNhanVienChuaThamGia.setRoot(nhanVienTreeItem);
//        tblNhanVienChuaThamGia.setShowRoot(false);
        String ma = congViecTrongCongTrinh.getPhongBan().getMaPhongBan();
        try {
            nhanVienChuaThamGiaFindResult = phanCongDAO.findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), ma, 0, 20);
            if (nhanVienChuaThamGiaFindResult != null) {
                paginationNhanVienChuaThamGia.setPageCount(nhanVienChuaThamGiaFindResult.getTotalPage());
                paginationNhanVienChuaThamGia.setPageFactory(this::createNhanVienByPhongBan);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    // endregion

    /**
     * Phân trang danh sách nhân viên chưa tham gia công trình
     *
     * @param indexPage
     * @return
     */
    private Node createPageChuaThamGia(int indexPage) {

        try {
            nhanVienChuaThamGiaFindResult = phanCongDAO.findDanhSachChuaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(), indexPage, 20);
            listNhanVienChuaThamGia = FXCollections.observableArrayList(nhanVienChuaThamGiaFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienChuaThamGia, RecursiveTreeObject::getChildren);

            if (tblNhanVienChuaThamGia.getRoot() != null) {
                tblNhanVienChuaThamGia.getRoot().getChildren().clear();
            }

            tblNhanVienChuaThamGia.setRoot(nhanVienTreeItem);
            tblNhanVienChuaThamGia.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVienChuaThamGia;
    }

    /**
     * phân trang danh sách nhân viên đã tham gia công trình
     *
     * @param indexPage
     * @return
     */
    private Node createPageDaThamGia(int indexPage) {
        try {
            nhanVienThamGiaFindResult = phanCongDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(), indexPage, 20);
            listNhanVienDaThamGia = FXCollections.observableArrayList(nhanVienThamGiaFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienDaThamGia, RecursiveTreeObject::getChildren);

            if (tblNhanVienDaThamGia.getRoot() != null) {
                tblNhanVienDaThamGia.getRoot().getChildren().clear();
            }

            tblNhanVienDaThamGia.setRoot(nhanVienTreeItem);
            tblNhanVienDaThamGia.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVienDaThamGia;
    }

    /**
     * danh sach nhân viên chua tham khi chưa có nhân vien nào
     *
     * @param indexPage
     * @return
     */
    private Node createPageDaThamGiaNull(int indexPage) {
        //nhanVienThamGiaFindResult = thamGiaCongTrinhDAO.findDanhSachDaThamGiaCongTrinh(congTrinhSelected.getMaCongTrinh(),indexPage,20);
        listNhanVienDaThamGia = FXCollections.observableArrayList(nhanVienList);
        TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienDaThamGia, RecursiveTreeObject::getChildren);

        if (tblNhanVienDaThamGia.getRoot() != null) {
            tblNhanVienDaThamGia.getRoot().getChildren().clear();
        }

        tblNhanVienDaThamGia.setRoot(nhanVienTreeItem);
        tblNhanVienDaThamGia.setShowRoot(false);
        return tblNhanVienDaThamGia;
    }


    /**
     * hiển thị công việc
     *
     * @param actionEvent
     */

    @FXML
    private void HienThiNhanVienChuaThamGiaTheoPhongBan(ActionEvent actionEvent) {
        try {

            System.out.println(congTrinhSelected);
            System.out.println(cbPhongBan.getValue());
            String ma = congViecTrongCongTrinh.getPhongBan().getMaPhongBan();
            congViecTrongCongTrinh = congViecDAO.find(cbCongViec.getValue());
            System.out.println(congViecTrongCongTrinh);
            paginationNhanVienChuaThamGia.setPageCount(phanCongDAO.findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), ma, 0, 20).getTotalPage());
            paginationNhanVienChuaThamGia.setPageFactory(this::createNhanVienByPhongBan);
        } catch (DAOException | NotExistException e) {
            e.printStackTrace();
        }

    }


    @FXML
    private void hienThiCongViec(ActionEvent actionEvent) {
//        try {
//            System.out.println(congTrinhSelected);
//
//            //nhanVienThamGiaFindResult = phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20);
//            congViecTrongCongTrinh= congViecDAO.find(cbCongViec.getValue());
//            System.out.println(congViecTrongCongTrinh.getPhongBan().getMaPhongBan());
//            labCongviec.setText(congViecTrongCongTrinh.getTenCongViec());
//            if(phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20)!=null) {
//                paginationNhanVienDaThamGia.setPageCount(phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20).getTotalPage());
//                paginationNhanVienDaThamGia.setPageFactory(this::createNhanVienByCongViec);
//
////                paginationNhanVienChuaThamGia.setPageCount(phanCongDAO.findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(congTrinhSelected.getMaCongTrinh(),congViecTrongCongTrinh.getMaCongViec(),congViecTrongCongTrinh.getPhongBan().getMaPhongBan(),0,20).getTotalPage());
////                paginationNhanVienChuaThamGia.setPageFactory(this::createNhanVienByPhongBan);
//
//            }else {
//                paginationNhanVienDaThamGia.setPageCount(1);
//                paginationNhanVienDaThamGia.setPageFactory(this::createNhanVienByCongViecNull);
//            }
//        }catch (DAOException | NotExistException e){
//            e.printStackTrace();
//        }
//
//        try {
//
//            System.out.println(congTrinhSelected);
//            System.out.println(cbPhongBan.getValue());
//            String ma= congViecTrongCongTrinh.getPhongBan().getMaPhongBan();
//            congViecTrongCongTrinh = congViecDAO.find(cbCongViec.getValue());
//            System.out.println(congViecTrongCongTrinh);
//            paginationNhanVienChuaThamGia.setPageCount(phanCongDAO.findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(congTrinhSelected.getMaCongTrinh(),congViecTrongCongTrinh.getMaCongViec(),ma,0,20).getTotalPage());
//            paginationNhanVienChuaThamGia.setPageFactory(this::createNhanVienByPhongBan);
//        }catch (DAOException | NotExistException e){
//            e.printStackTrace();
//        }
    }


    private CongViec getItemSelected() {
        TreeTableView.TreeTableViewSelectionModel<CongViec> congViecTreeTableViewSelectionModel = tblCongViec.getSelectionModel();
        if (congViecTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một công trình");
            return null;
        }

        int rowIndex = congViecTreeTableViewSelectionModel.getSelectedIndex();
        CongViec congViec = congViecTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
        return congViec;
    }

    @FXML
    private void clickTable(MouseEvent mouseEvent) {
        congViecTrongCongTrinh = getItemSelected();
        System.out.println(congViecTrongCongTrinh.getPhongBan().getMaPhongBan());
        try {
            System.out.println(congTrinhSelected);
            if (phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20) != null) {
                paginationNhanVienDaThamGia.setPageCount(phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), 0, 20).getTotalPage());
                paginationNhanVienDaThamGia.setPageFactory(this::createNhanVienByCongViec);
            } else {
                paginationNhanVienDaThamGia.setPageCount(1);
                paginationNhanVienDaThamGia.setPageFactory(this::createNhanVienByCongViecNull);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        try {

//            System.out.println(congTrinhSelected);
//            System.out.println(cbPhongBan.getValue());
            //String ma= congViecTrongCongTrinh.getPhongBan().getMaPhongBan();
            //congViecTrongCongTrinh = congViecDAO.find(cbCongViec.getValue());
            //System.out.println(congViecTrongCongTrinh);
            // System.out.println(congViecTrongCongTrinh.getPhongBan().getMaPhongBan());
            System.out.println(congViecTrongCongTrinh.getPhongBan().getMaPhongBan());
            paginationNhanVienChuaThamGia.setPageCount(phanCongDAO.findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), congViecTrongCongTrinh.getPhongBan().getMaPhongBan(), 0, 20).getTotalPage());
            paginationNhanVienChuaThamGia.setPageFactory(this::createNhanVienByPhongBan);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param indexPage
     * @return
     */
    private Node createNhanVienByPhongBan(int indexPage) {
        try {

            //congViecTrongCongTrinh = congViecDAO.find(cbCongViec.getValue());
            //String ma= congViecTrongCongTrinh.getPhongBan().getMaPhongBan();
            //System.out.println(congViecTrongCongTrinh.getPhongBan().getMaPhongBan());
            //System.out.println(cbPhongBan.getValue());
            congViecTrongCongTrinh = getItemSelected();
            nhanVienChuaThamGiaFindResult = phanCongDAO.findDanhSachChuaThamGiaCongTrinhByCongViecPhongBan(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), congViecTrongCongTrinh.getPhongBan().getMaPhongBan(), indexPage, 20);
            listNhanVienChuaThamGia = FXCollections.observableArrayList(nhanVienChuaThamGiaFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienChuaThamGia, RecursiveTreeObject::getChildren);
            if (tblNhanVienChuaThamGia.getRoot() != null) {
                tblNhanVienChuaThamGia.getRoot().getChildren().clear();
            }

            tblNhanVienChuaThamGia.setRoot(nhanVienTreeItem);
            tblNhanVienChuaThamGia.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVienChuaThamGia;
    }


    /**
     * hiển thị danh sách nhân viên theo công việc
     */
    private Node createNhanVienByCongViec(int indexPage) {

        try {
            nhanVienThamGiaFindResult = phanCongDAO.findDanhSachDaThamGiaCongTrinhByCongViec(congTrinhSelected.getMaCongTrinh(), congViecTrongCongTrinh.getMaCongViec(), indexPage, 20);
            listNhanVienDaThamGia = FXCollections.observableArrayList(nhanVienThamGiaFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienDaThamGia, RecursiveTreeObject::getChildren);

            if (tblNhanVienDaThamGia.getRoot() != null) {
                tblNhanVienDaThamGia.getRoot().getChildren().clear();
            }

            tblNhanVienDaThamGia.setRoot(nhanVienTreeItem);
            tblNhanVienDaThamGia.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVienDaThamGia;
//    }
    }

    private Node createNhanVienByCongViecNull(int indexPage) {

        nhanVienListThamGiaCongViecNull = new ArrayList<>();
        listNhanVienDaThamGia = FXCollections.observableArrayList(nhanVienListThamGiaCongViecNull);
        TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.listNhanVienDaThamGia, RecursiveTreeObject::getChildren);

        if (tblNhanVienDaThamGia.getRoot() != null) {
            tblNhanVienDaThamGia.getRoot().getChildren().clear();
        }

        tblNhanVienDaThamGia.setRoot(nhanVienTreeItem);
        tblNhanVienDaThamGia.setShowRoot(false);

        return tblNhanVienDaThamGia;
//    }
    }


    private Node createPageCongViec(int pageIndex) {
        try {
            congViecFindResult = congViecDAO.findByCongTrinh(congTrinhSelected.getMaCongTrinh(), pageIndex, 20);
            congViecs = FXCollections.observableArrayList(congViecFindResult.getListResult());
            TreeItem<CongViec> congViecTreeItem = new RecursiveTreeItem<>(this.congViecs, RecursiveTreeObject::getChildren);

            if (tblCongViec.getRoot() != null) {
                tblCongViec.getRoot().getChildren().clear();
            }
            tblCongViec.setRoot(congViecTreeItem);
            tblCongViec.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblCongViec;
    }
}
