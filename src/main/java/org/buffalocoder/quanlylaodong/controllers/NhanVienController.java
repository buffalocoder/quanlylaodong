package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TreeTableView.TreeTableViewSelectionModel;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.daos.PhongBanDAOImpl;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;
import org.buffalocoder.quanlylaodong.interfaces.IPhongBanDAO;
import org.buffalocoder.quanlylaodong.types.TrangThaiNhanVienEnum;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Class: NhanVienController
 *
 * @author: Đặng Lê Minh Trường, Nguyễn Chí Cường
 * @version: 1.1
 * @description: Dùng để giao tiếp giữa các class giao diện với các lớp DAO
 * @since : 05-10-2019
 */
public class NhanVienController implements Initializable {
    private double x, y;
    private Scene scene;
    private Stage stage;
    private Parent root;
    private ObservableList<NhanVien> nhanViens = FXCollections.observableArrayList();
    private AlertController alertController;
    private INhanVienDAO nhanVienDAO;
    private IPhongBanDAO phongBanDAO;
    private FindResult<NhanVien> nhanVienFindResult;


    @FXML
    private Pagination Pagination;

    @FXML
    private TextField txtTimKiem;
    @FXML
    private JFXComboBox<String> cbTimKiem;
    @FXML
    private JFXTreeTableView<NhanVien> tblNhanVien;
    @FXML
    private TreeTableColumn colMaNhanVien, colHoTen, colGioiTinh, colSoDienThoai, colDanToc, colCMND, colNgaySinh, colTrangThai;
    @FXML
    private TreeTableColumn<NhanVien, String> colPhongBan;
    @FXML
    private StackPane stackPane;
    @FXML
    private Pane pane, paneTrangThai;
    private String keyTimKiem;


    private int soTrang = 5;

    private final int soPhanTu = 20;

    // region Event
    @FXML
    public void onThemNhanVien(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
//            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/ThemNhanVienScene.fxml"));
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/ModifierNhanVienScene.fxml"));
            root = fxmlLoader.load();

            ModifierNhanVienController modifierNhanVienController = fxmlLoader.getController();
            modifierNhanVienController.setNhanViens(this.nhanViens);
            modifierNhanVienController.setTblNhanVien(tblNhanVien);
            modifierNhanVienController.setPaginationNhanVien(Pagination);

            scene = new Scene(root, 760, 800);
            stage = new Stage();
            stage.initOwner(((Node) (event.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onCapNhatNhanVien(ActionEvent event) {
        NhanVien nhanVien = getItemSelected();
        if (nhanVien == null) return;

        //System.out.println(nhanVien);

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/SuaNhanVienScene.fxml"));
            root = fxmlLoader.load();

            ModifierNhanVienController modifierNhanVienController = fxmlLoader.getController();
            modifierNhanVienController.setNhanViens(this.nhanViens);
            modifierNhanVienController.setNhanVienSelected(nhanVien, true, false);
            modifierNhanVienController.setTblNhanVien(tblNhanVien);


//            root = FXMLLoader.load(getClass().getResource("/fxml/popup/CapNhatNhanVienScene.fxml"));
//            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 760, 800);
            stage = new Stage();
            stage.initOwner(((Node) (event.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onXemChiTietNhanVien(ActionEvent actionEvent) {
        NhanVien nhanVien = getItemSelected();

        if (nhanVien == null) return;

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/XemCTNhanVienScene.fxml"));
            root = fxmlLoader.load();

            ModifierNhanVienController modifierNhanVienController = fxmlLoader.getController();
            try {
                modifierNhanVienController.setNhanVienSelected(nhanVien, false, true);
            } catch (Exception E) {
                E.printStackTrace();
            }


            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 900, 700);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức cập nhật data table
     *
     * @param nhanViens
     */

    public void updateDataTable(ObservableList<NhanVien> nhanViens) {
        System.out.println(nhanViens);
//        this.nhanViens = (nhanViens == null) ? FXCollections.observableArrayList(this.nhanVienDAO.find()) : nhanViens;
        this.nhanViens = nhanViens;
        TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

        if (tblNhanVien.getRoot() != null) {
            tblNhanVien.getRoot().getChildren().clear();
        }

        tblNhanVien.setRoot(nhanVienTreeItem);
        tblNhanVien.setShowRoot(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        try {
            nhanVienDAO = new NhanVienDAOImpl();
            phongBanDAO = new PhongBanDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        cbTimKiem.getItems().addAll("Mã nhân viên", "Tên nhân viên", "CMND", "Số điện thoại", "Phòng ban", "Trạng thái");
        cbTimKiem.setValue("Mã nhân viên");
        txtTimKiem.setOnKeyTyped(e -> {
            keyTimKiem = txtTimKiem.getText();
        });
        colMaNhanVien.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("maNhanVien"));
        colHoTen.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("hoTen"));
        colSoDienThoai.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("soDienThoai"));
        colCMND.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("cmnd"));
        colNgaySinh.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, LocalDate>("ngaySinh"));
        colGioiTinh.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, Boolean>("gioiTinh"));
        colPhongBan.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getPhongBan().getTenPhongBan()));
        colTrangThai.setCellValueFactory(new TreeItemPropertyValueFactory<NhanVien, String>("trangThai"));
//        soTrang=nhanVienDAO.totalPage(20);
//        System.out.println("---------------------------");
//        System.out.println(soTrang);

        try {
            nhanVienFindResult = nhanVienDAO.findAll(1, 20);
            System.out.println(nhanVienFindResult.getTotalPage());
        } catch (DAOException e) {
            e.printStackTrace();
        }
        if (nhanVienFindResult != null) {
            Pagination.setPageCount(nhanVienFindResult.getTotalPage());
            Pagination.setPageFactory(this::createPage);
        } else {
            Pagination.setPageCount(1);
        }
        //updateDataTable(null);
    }

    /**
     * Phương thức chuyển tìm kiếm sang trạng thái
     *
     * @param event
     * @return
     */
    @FXML
    public void onTimKiemTrangThai(ActionEvent event) {
        if (cbTimKiem.getValue().equals("Trạng thái")) {
            paneTrangThai.getChildren().remove(0);
            ComboBox<String> cb = new ComboBox<>();
            cb.getItems().addAll("Đã nghỉ việc", "Đang làm");
            cb.setValue("Đang làm");
            keyTimKiem = cb.getValue();
            cb.setPrefWidth(156);
            cb.setOnAction(e -> {
                try {
                    keyTimKiem = cb.getValue();
                    System.out.println(keyTimKiem);
                    TrangThaiNhanVienEnum trangThaiNhanVienEnum = null;
                    if (keyTimKiem.equals("Đã nghỉ việc")) trangThaiNhanVienEnum = TrangThaiNhanVienEnum.NGHI_VIEC;
                    else if (keyTimKiem.equals("Đang làm")) trangThaiNhanVienEnum = TrangThaiNhanVienEnum.DANG_LAM;
                    nhanVienFindResult = nhanVienDAO.findByTinhTrang(trangThaiNhanVienEnum, 0, 20);

                    if (nhanVienFindResult == null) {
                        alertController.alertInfo("Không tìm thấy nhân viên");
                    } else {
                        if (nhanVienFindResult.getListResult().isEmpty()) {
                            Pagination.setPageCount(1);
                        } else {
                            Pagination.setPageCount(nhanVienFindResult.getTotalPage());
                        }
                        Pagination.setPageFactory(this::createPageByTrangThai);
                    }

                } catch (DAOException ex) {
                    ex.printStackTrace();
                }
            });
            paneTrangThai.getChildren().add(cb);
        } else {
            paneTrangThai.getChildren().remove(0);
            TextField txtTimKiem = new TextField();
            txtTimKiem.setPrefWidth(211);
            txtTimKiem.setPrefHeight(31);
            txtTimKiem.setOnKeyTyped(e -> {
                keyTimKiem = txtTimKiem.getText();
            });
            paneTrangThai.getChildren().add(txtTimKiem);
        }
    }

    /**
     * Phương thức lấy nhân viên từ bảng
     *
     * @return
     */
    private NhanVien getItemSelected() {
        TreeTableViewSelectionModel<NhanVien> nhanVienTreeTableViewSelectionModel = tblNhanVien.getSelectionModel();
        if (nhanVienTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một nhân viên");
            return null;
        }
        int rowIndex = nhanVienTreeTableViewSelectionModel.getSelectedIndex();
        return nhanVienTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
    }

    /**
     * Phương thức xoa nhân viên
     *
     * @param actionEvent
     */

    @FXML
    public void onXoaNhanVien(ActionEvent actionEvent) throws DAOException {
        NhanVien nhanVien = getItemSelected();


        if (nhanVien != null) {
            stackPane.toFront();
            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            dialogLayout.setHeading(new Text("Thông báo"));
            dialogLayout.setBody(new Text("Bạn có muốn xóa nhân viên " + nhanVien.getHoTen() + " này không ?"));
            JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

            JFXButton btn_Ok = new JFXButton();
            btn_Ok.setText("Đồng ý");
            btn_Ok.setOnAction(event -> {
                try {
                    this.nhanVienDAO.delete(nhanVien);
                    this.nhanViens.remove(nhanVien);
                    updateDataTable(this.nhanViens);
                } catch (DAOException e) {
                    alertController.alertError(e.getMessage());
                }
                dialog.close();
            });
            JFXButton btn_Cancel = new JFXButton();
            btn_Cancel.setText("Huỷ");
            btn_Cancel.setOnAction(e -> {
                stackPane.toBack();
                dialog.close();
            });
            dialogLayout.setActions(btn_Ok, btn_Cancel);

            dialog.show();
        }
    }

    /**
     * Phương thức lấy nhân viên từ bảng
     *
     * @param actionEvent
     * @return
     */
    @FXML
    public void onTimKiem(ActionEvent actionEvent) {
        String keyword = txtTimKiem.getText().trim();
        List<NhanVien> nhanViens = null;

        //System.out.println(txtTimKiem.getText());
        System.out.println(keyTimKiem);
        keyword=keyTimKiem;

        try {
            if (keyword.isEmpty()) {
//                nhanViens = nhanVienDAO.find();
                nhanVienFindResult = nhanVienDAO.findAll(1, 20);
                Pagination.setPageCount(nhanVienFindResult.getTotalPage());
                Pagination.setPageFactory(this::createPage);
            } else if (cbTimKiem.getValue().equals("Mã nhân viên")) {
                System.out.println(cbTimKiem.getValue());
                Pagination.setPageCount(1);
                nhanViens = new ArrayList<>();
                NhanVien phongBan = nhanVienDAO.find(keyword);
                if (phongBan != null) nhanViens.add(phongBan);
                if (nhanViens.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy nhân viên");
                } else {
                    ObservableList<NhanVien> nhanVienObservableList = FXCollections.observableList(nhanViens);
                    updateDataTable(nhanVienObservableList);
                }
            } else if (cbTimKiem.getValue().equals("Tên nhân viên")) {
//                nhanViens = nhanVienDAO.findByHoTen(keyword);
                nhanVienFindResult = nhanVienDAO.findByHoTen(txtTimKiem.getText(), 0, 20);
                if (nhanVienFindResult == null) {
                    Pagination.setPageCount(1);
                    alertController.alertInfo("Không tìm thấy nhân viên");
                }
                if (!nhanVienFindResult.getListResult().isEmpty()) {
                    Pagination.setPageCount(nhanVienFindResult.getTotalPage());
                    Pagination.setPageFactory(this::createPageByName);
                } else {
                    Pagination.setPageCount(1);
                    alertController.alertInfo("Không tìm thấy nhân viên");
                }
            } else if (cbTimKiem.getValue().equals("CMND")) {
                System.out.println("cmnd");
                nhanVienFindResult = nhanVienDAO.findByCMND(txtTimKiem.getText(), 0, 20);
                if (nhanVienFindResult == null) {
                    Pagination.setPageCount(1);
                    alertController.alertInfo("Không tìm thấy nhân viên");
                } else {
                    System.out.println("số trang");
                    System.out.println(nhanVienFindResult.getTotalPage());
                    Pagination.setPageCount(nhanVienFindResult.getTotalPage());
                    Pagination.setPageFactory(this::createPageByCmnd);
                }
//                nhanViens = nhanVienDAO.findByCMND(keyword);
            } else if (cbTimKiem.getValue().equals("Số điện thoại")) {
                nhanVienFindResult = nhanVienDAO.findBySoDienThoai(txtTimKiem.getText(), 0, 20);
                if (nhanVienFindResult == null) {
                    Pagination.setPageCount(1);
                    alertController.alertInfo("Không tìm thấy nhân viên");
                }
                if (nhanVienFindResult.getListResult().isEmpty()) {
                    alertController.alertInfo("Không tìm thấy nhân viên");
                } else {
                    Pagination.setPageCount(nhanVienFindResult.getTotalPage());
                    Pagination.setPageFactory(this::createPageByPhone);
                }
            } else if (cbTimKiem.getValue().equals("Phòng ban")) {
                nhanVienFindResult = phongBanDAO.findDanhSachNhanVienPhongBan(txtTimKiem.getText(), 0, 20);
                if (nhanVienFindResult == null) {
                    Pagination.setPageCount(1);
                    alertController.alertInfo("Không tìm thấy nhân viên");
                }
                if (nhanVienFindResult.getListResult().isEmpty()) {
                    alertController.alertInfo("Không tìm thấy nhân viên");
                } else {
                    Pagination.setPageCount(nhanVienFindResult.getTotalPage());
                    Pagination.setPageFactory(this::createPageByPhongBan);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Node createPage(int pageIndex) {

        try {
            nhanVienFindResult = this.nhanVienDAO.findAll(pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(this.nhanVienDAO.findAll(pageIndex, 20).getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }

    /**
     * phân trang theo tên
     *
     * @param pageIndex
     * @return
     */
    private Node createPageByName(int pageIndex) {

        try {
            nhanVienFindResult = this.nhanVienDAO.findByHoTen(txtTimKiem.getText(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }

    /**
     * phân trang theo số cmnd
     *
     * @param pageIndex
     * @return
     */
    private Node createPageByCmnd(int pageIndex) {

        try {
            nhanVienFindResult = this.nhanVienDAO.findByCMND(txtTimKiem.getText(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }

    private Node createPageByPhone(int pageIndex) {

        try {
            nhanVienFindResult = this.nhanVienDAO.findBySoDienThoai(txtTimKiem.getText(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }

    /**
     * tìm kiếm theo phòng ban
     *
     * @param pageIndex
     * @return
     */
    private Node createPageByPhongBan(int pageIndex) {

        try {
            nhanVienFindResult = this.phongBanDAO.findDanhSachNhanVienPhongBan(txtTimKiem.getText(), pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }

    private Node createPageByTrangThai(int pageIndex) {
        try {

            TrangThaiNhanVienEnum trangThaiNhanVienEnum = null;
            if (keyTimKiem.equals("Đã nghỉ việc")) trangThaiNhanVienEnum = TrangThaiNhanVienEnum.NGHI_VIEC;
            else if (keyTimKiem.equals("Đang làm")) trangThaiNhanVienEnum = TrangThaiNhanVienEnum.DANG_LAM;
            nhanVienFindResult = nhanVienDAO.findByTinhTrang(trangThaiNhanVienEnum, pageIndex, 20);
            nhanViens = FXCollections.observableArrayList(nhanVienFindResult.getListResult());
            TreeItem<NhanVien> nhanVienTreeItem = new RecursiveTreeItem<>(this.nhanViens, RecursiveTreeObject::getChildren);

            if (tblNhanVien.getRoot() != null) {
                tblNhanVien.getRoot().getChildren().clear();
            }

            tblNhanVien.setRoot(nhanVienTreeItem);
            tblNhanVien.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblNhanVien;
    }


    /**
     * Phương thức mở scene tìm kiếm nâng cao
     *
     * @return
     */
    @FXML
    private void onOpenTimKiemNangCao(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/TimKiemNangCaoScene.fxml"));
            root = fxmlLoader.load();

            scene = new Scene(root, 600, 400);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

