package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import org.buffalocoder.quanlylaodong.daos.CongTrinhDAOImpl;
import org.buffalocoder.quanlylaodong.daos.CongViecDAOImpl;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.entities.CongTrinh;
import org.buffalocoder.quanlylaodong.entities.CongViec;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;
import org.buffalocoder.quanlylaodong.exceptions.ValidateException;
import org.buffalocoder.quanlylaodong.interfaces.ICongTrinhDAO;
import org.buffalocoder.quanlylaodong.interfaces.ICongViecDAO;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;
import org.buffalocoder.quanlylaodong.types.TrangThaiCongTrinh;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author: Đặng Lê Minh Trường
 * @version: 1.0
 */
public class ThongKeController implements Initializable {
    @FXML
    private PieChart pieChartCongTrinh;

    @FXML
    private Label lblDangThiCong, lblHoanThanh, lblTamDung, lblTreHan, lblTinhTrang, lblTongNhanVien, lblThoiGian, lblChiPhi;
    @FXML
    private Label lblNVXayDung, lblNVPhanTich, lblNVThietKe, lblNVQuanLy, lblNVTaiChinh, lblNVMarketing, lblTongTien;
    @FXML
    private JFXComboBox<String> cbMaCongTrinh;
    @FXML
    private JFXTreeTableView<CongViec> tblCongViec;
    @FXML
    private TreeTableColumn colTenCongViec, colSoluongNhanVien, colChiPhi;
    @FXML
    private TreeTableColumn<CongViec, String> colMaPhongBan;
    @FXML
    private Pagination paginationCongViec;

    private ICongTrinhDAO congTrinhDAO;
    private INhanVienDAO nhanVienDAO;
    private ICongViecDAO congViecDAO;
    private int congTrinhDangThiCong = 0;
    private int congTrinhTreHan = 0;
    private int congTrinhTamDung = 0;
    private int congTrinhHoanThanh = 0;
    private FindResult<CongViec> congViecFindResult;
    private List<CongViec> congViecList;
    private ObservableList<CongViec> congViecs = FXCollections.observableArrayList();
    private CongTrinh congTrinh;

    private void initContructionStaticsData() {
        pieChartCongTrinh.setData(getChartDataConstruction());
        pieChartCongTrinh.setLegendSide(Side.RIGHT);
    }

    @FXML
    private void onGetStaticsOfContruction(ActionEvent event) {

        String HienThi = cbMaCongTrinh.getValue();
        String[] maCongTrinh = HienThi.split("-");
        System.out.println(maCongTrinh[0]);
        getStaticsOfContruction(maCongTrinh[0]);
    }

    private void getStaticsOfContruction(String maCongTrinh) {

        try {
            congTrinh = congTrinhDAO.find(maCongTrinh);
            double TongTien = congViecDAO.tinhTongChiPhiCongViec(maCongTrinh);
            lblTongTien.setText(String.valueOf(TongTien));
            int tongSoCT = congTrinhDAO.thongKeTongSoLuongCongTrinh();
            int tongSoNVcuaCongTrinh = nhanVienDAO.thongKeTongSoNhanVienCuaCongTrinh(maCongTrinh);
            System.out.println(tongSoNVcuaCongTrinh);
            lblTongNhanVien.setText(String.valueOf(tongSoNVcuaCongTrinh));
            String trangThai = String.valueOf(congTrinhDAO.find(maCongTrinh).getTrangThaiCongTrinh());
            if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.HOAN_THANH))) {
                System.out.println(TrangThaiCongTrinh.HOAN_THANH);
                System.out.println(trangThai);
                trangThai = "Hoàn thành";
            } else if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.DANG_THI_CONG))) {
                trangThai = "Đang thi công";
            } else if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.TAM_DUNG))) {
                trangThai = "Tạm dựng";
            } else if (trangThai.equals(String.valueOf(TrangThaiCongTrinh.TRE_HAN))) {
                trangThai = "Trễ hạn";
            }

            lblTinhTrang.setText(trangThai);
//            Ngày
            String ngayThiCong = String.valueOf(congTrinhDAO.find(maCongTrinh).getNgayThiCong());
            String ngayHoanThanh = String.valueOf(congTrinhDAO.find(maCongTrinh).getNgayDuKienHoanThanh());
            lblThoiGian.setText(ngayThiCong + " - " + ngayHoanThanh);

            int soNhanVienXayDung = nhanVienDAO.thongKeLoaiNhanVienCuaCongTrinh(maCongTrinh, "PB0003");
            lblNVXayDung.setText(String.valueOf(soNhanVienXayDung));
            int soNhanVienThietKe = nhanVienDAO.thongKeLoaiNhanVienCuaCongTrinh(maCongTrinh, "PB0002");
            lblNVThietKe.setText(String.valueOf(soNhanVienThietKe));
            int soNhanVienTaiChinh = nhanVienDAO.thongKeLoaiNhanVienCuaCongTrinh(maCongTrinh, "PB0001");
            lblNVTaiChinh.setText(String.valueOf(soNhanVienTaiChinh));
            int soNhanVienQuanLy = nhanVienDAO.thongKeLoaiNhanVienCuaCongTrinh(maCongTrinh, "PB0004");
            lblNVQuanLy.setText(String.valueOf(soNhanVienQuanLy));
            int soNhanVienMarketing = nhanVienDAO.thongKeLoaiNhanVienCuaCongTrinh(maCongTrinh, "PB0007");
            lblNVMarketing.setText(String.valueOf(soNhanVienMarketing));

//            double tongChiPhi = congTrinhDAO.thongKeChiPhiCongTrinh(maCongTrinh);
//            lblChiPhi.setText(String.valueOf(tongChiPhi));

//          Tổng
            congTrinhDangThiCong = congTrinhDAO.thongKeTheoTrangThai(TrangThaiCongTrinh.DANG_THI_CONG);
            congTrinhTreHan = congTrinhDAO.thongKeTheoTrangThai(TrangThaiCongTrinh.TRE_HAN);
            congTrinhTamDung = congTrinhDAO.thongKeTheoTrangThai(TrangThaiCongTrinh.TAM_DUNG);
            congTrinhHoanThanh = congTrinhDAO.thongKeTheoTrangThai(TrangThaiCongTrinh.HOAN_THANH);

            lblHoanThanh.setText((congTrinhHoanThanh * tongSoCT) / 100 + " - " + congTrinhHoanThanh + "%");
            lblTreHan.setText((congTrinhTreHan * tongSoCT) / 100 + " - " + congTrinhTreHan + "%");
            lblDangThiCong.setText((congTrinhDangThiCong * tongSoCT) / 100 + " - " + congTrinhDangThiCong + "%");
            lblTamDung.setText((congTrinhTamDung * tongSoCT) / 100 + " - " + congTrinhTamDung + "%");

//            init table
            initTableCongViec();
        } catch (DAOException | NotExistException e) {
            e.printStackTrace();
        }

    }

    private ObservableList<PieChart.Data> getChartDataConstruction() {
        ObservableList<PieChart.Data> answer = FXCollections.observableArrayList();
        answer.addAll(new PieChart.Data("Đã hoàn thành", congTrinhHoanThanh),
                new PieChart.Data("Đang thi công", congTrinhDangThiCong),
                new PieChart.Data("Trễ hạn", congTrinhTreHan),
                new PieChart.Data("Tạm dừng", congTrinhTamDung));

        return answer;
    }

    private void initTableCongViec() throws DAOException, NotExistException {
        colTenCongViec.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, String>("tenCongViec"));
        colSoluongNhanVien.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, Integer>("soLuongNhanVienToiDa"));
        colMaPhongBan.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getValue().getPhongBan().getTenPhongBan()));
        colChiPhi.setCellValueFactory(new TreeItemPropertyValueFactory<CongViec, Integer>("chiPhi"));

        try {
            congViecFindResult = congViecDAO.findByCongTrinh(congTrinh.getMaCongTrinh(), 0, 20);
            if (congViecFindResult != null) {
                paginationCongViec.setPageCount(congViecFindResult.getTotalPage());
                paginationCongViec.setPageFactory(this::createPageCongViec);
            } else {
                paginationCongViec.setPageCount(1);
                paginationCongViec.setPageFactory(this::createPageCongViecnull);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * phân trang cho công việc
     *
     * @param pageIndex
     * @return
     */
    private Node createPageCongViec(int pageIndex) {
        try {
            congViecFindResult = congViecDAO.findByCongTrinh(congTrinh.getMaCongTrinh(), pageIndex, 20);
            congViecs = FXCollections.observableArrayList(congViecFindResult.getListResult());
            TreeItem<CongViec> congViecTreeItem = new RecursiveTreeItem<>(this.congViecs, RecursiveTreeObject::getChildren);

            if (tblCongViec.getRoot() != null) {
                tblCongViec.getRoot().getChildren().clear();
            }
            tblCongViec.setRoot(congViecTreeItem);
            tblCongViec.setShowRoot(false);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblCongViec;
    }

    /**
     * phân trang cho những danh sách chưa có
     *
     * @param pageIndex
     * @return
     */
    private Node createPageCongViecnull(int pageIndex) {

        congViecList = new ArrayList<>();
        congViecs = FXCollections.observableArrayList(congViecList);
        TreeItem<CongViec> congViecTreeItem = new RecursiveTreeItem<>(this.congViecs, RecursiveTreeObject::getChildren);

        if (tblCongViec.getRoot() != null) {
            tblCongViec.getRoot().getChildren().clear();
        }
        tblCongViec.setRoot(congViecTreeItem);
        tblCongViec.setShowRoot(false);

        return tblCongViec;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            congTrinhDAO = new CongTrinhDAOImpl();
            nhanVienDAO = new NhanVienDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        try {
            congViecDAO = new CongViecDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        congTrinh = new CongTrinh();
        try {
            congTrinh.setMaCongTrinh("CT0001");
        } catch (ValidateException e) {
            e.printStackTrace();
        }
        try {
            congTrinhDAO.findAll().forEach(n -> {
                String HienThi = String.format(n.getMaCongTrinh() + "-" + n.getTenCongTrinh());
                cbMaCongTrinh.getItems().add(HienThi);
                cbMaCongTrinh.setValue(HienThi);
            });

            String HienThi = cbMaCongTrinh.getValue();
            String[] maCongTrinh = HienThi.split("-");
            System.out.println(maCongTrinh[0]);
            getStaticsOfContruction(maCongTrinh[0]);


            //getStaticsOfContruction(cbMaCongTrinh.getValue());
        } catch (DAOException e) {
            e.printStackTrace();
        }
        initContructionStaticsData();

    }
}
