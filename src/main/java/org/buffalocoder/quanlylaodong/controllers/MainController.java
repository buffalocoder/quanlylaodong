package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class: MainController
 *
 * @author: Đặng Lê Minh Trường
 * @version: 1.0
 * @description: Dùng để giao tiếp giữa các class giao diện với các lớp DAO
 * @since : 05-10-2019
 */
public class MainController implements Initializable {

    private double x, y;
    @FXML
    private JFXButton btnCongTrinh, btnPhongBan, btnNhanVien, btnThongKe, btnDoiMatKhau;
    @FXML
    private Pane paneThongKeChart, pane_Tong;
    @FXML
    private StackPane stackPaneMain;

    @FXML
    private void handleClose(MouseEvent event) {
        AlertController alertController = new AlertController(stackPaneMain);
        alertController.alertYesNo("Thoát", "Bạn có chắc là muốn thoát chương trình?");
    }

    @FXML
    private void draged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void min(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void handleButtonMouse(ActionEvent event) throws IOException {
        stackPaneMain.toBack();
        String blueBG = "-fx-background-color: #1199C4; -fx-font-weight: bold";
        String darkBlueBG = "-fx-background-color: #0C1229; -fx-font-weight: bold";
        pane_Tong.heightProperty().removeListener(this.event);
        btnPhongBan.setStyle(blueBG);
        btnCongTrinh.setStyle(blueBG);
        btnThongKe.setStyle(blueBG);
        btnNhanVien.setStyle((blueBG));
        btnDoiMatKhau.setStyle(blueBG);
        Parent root = null;
        if (event.getSource() == btnPhongBan) {

            root = FXMLLoader.load(getClass().getResource("/fxml/PhongBanScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            btnPhongBan.setStyle(darkBlueBG);
        } else if (event.getSource() == btnCongTrinh) {

            root = FXMLLoader.load(getClass().getResource("/fxml/QuanLyCongTrinhScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());


            btnCongTrinh.setStyle(darkBlueBG);
        } else if (event.getSource() == btnNhanVien) {
            root = FXMLLoader.load(getClass().getResource("/fxml/NhanVienScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());

            btnNhanVien.setStyle(darkBlueBG);
        } else if (event.getSource() == btnThongKe) {
            root = FXMLLoader.load(getClass().getResource("/fxml/ThongKeScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());

            btnThongKe.setStyle(darkBlueBG);
        } else if (event.getSource() == btnDoiMatKhau) {
            root = FXMLLoader.load(getClass().getResource("/fxml/DoiMatKhauScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());

            btnDoiMatKhau.setStyle(darkBlueBG);
        }

        pane_Tong.getChildren().removeAll();
        pane_Tong.getChildren().setAll(root);

        AnchorPane anchorPane = (AnchorPane) pane_Tong.getChildren().get(0);
        anchorPane.prefHeightProperty().bind(pane_Tong.heightProperty());
        anchorPane.prefWidthProperty().bind(pane_Tong.widthProperty());

        pane_Tong.heightProperty().addListener(this.event);
    }

    ChangeListener<Number> event = new ChangeListener<Number>() {
        @Override
        public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
            AnchorPane anchorPane = (AnchorPane) pane_Tong.getChildren().get(0);
            anchorPane.prefHeightProperty().bind(pane_Tong.heightProperty());
            anchorPane.prefWidthProperty().bind(pane_Tong.widthProperty());
        }
    };

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/QuanLyCongTrinhScene.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
        pane_Tong.getChildren().removeAll();
        pane_Tong.getChildren().setAll(root);
        pane_Tong.heightProperty().addListener(event);
    }

    @FXML
    private void dangXuat(ActionEvent event) throws IOException {
// todo diaglog
        stackPaneMain.toFront();
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Đăng xuất"));
        dialogLayout.setBody(new Text("Bạn có muốn đăng xuất khỏi chương trình?"));
        JFXDialog dialog = new JFXDialog(stackPaneMain, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(e -> {
            //todo đóng stage
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.close();
            // todo mở đăng nhập
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/fxml/DangNhapScene.fxml"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            root.getStylesheets().add(getClass().getResource("/css/DangNhapScene.css").toExternalForm());
            Scene scene = new Scene(root, 855, 400);
            Stage primaryStage = new Stage();
            primaryStage.setScene(scene);
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.setResizable(false);
            primaryStage.show();
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> {
            stackPaneMain.toBack();
            dialog.close();
        });
        dialogLayout.setActions(btn_Ok, btn_Cancel);
        dialog.show();
    }
}
