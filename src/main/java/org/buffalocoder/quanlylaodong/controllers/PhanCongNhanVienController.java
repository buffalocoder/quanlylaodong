package org.buffalocoder.quanlylaodong.controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.entities.NhanVien;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PhanCongNhanVienController implements Initializable {

    @FXML
    private StackPane stackPane;

    private AlertController alertController;
    private ObservableList<NhanVien> nhanViens;
    private NhanVien nhanVienSelected;
    private NhanVienDAOImpl nhanVienDAO;
    private double x, y;

    // region Event
    @FXML
    private void onThemNhanVien(ActionEvent event) throws Exception {
    }

    @FXML
    private void onCapNhatNhanVien(ActionEvent event) throws Exception {

    }

    @FXML
    private void onDragged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void onPressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    public void onClose(MouseEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onChooseFiles(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("File", "*.*"));
        List<File> f = fc.showOpenMultipleDialog(null);
        for (File file : f) {
            System.out.println(file.getAbsolutePath());
        }
    }
    // endregion

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void setNhanViens(ObservableList<NhanVien> nhanViens) {
        this.nhanViens = nhanViens;
    }

    public void setNhanVienSelected(NhanVien nhanVienSelected) {
    }


    private void transferData() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/NhanVienScene.fxml"));
            fxmlLoader.load();
            //PhongBanController phongBanController = fxmlLoader.getController();
            NhanVienController nhanVienController = fxmlLoader.getController();
            nhanVienController.updateDataTable(this.nhanViens);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        return false;
    }
}
