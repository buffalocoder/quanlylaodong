package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView.TreeTableViewSelectionModel;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlylaodong.daos.PhongBanDAOImpl;
import org.buffalocoder.quanlylaodong.entities.FindResult;
import org.buffalocoder.quanlylaodong.entities.PhongBan;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Class: PhongBanController
 *
 * @author: Đặng Lê Minh Trường, Nguyễn Chí Cường
 * @version: 1.0
 * @description: Dùng để giao tiếp giữa các class giao diện với các lớp DAO
 * @since : 05-10-2019
 */
public class QuanLyPhongBanController implements Initializable {
    private PhongBanDAOImpl phongBanDAOImpl = null;
    private ObservableList<PhongBan> phongBans = FXCollections.observableArrayList();
    private AlertController alertController;
    private Scene scene;
    private Stage stage;
    private Parent root;
    private FindResult<PhongBan> phongBanFindResult;

    @FXML
    private Pagination pagination;
    @FXML
    private StackPane stackPane;
    @FXML
    private TextField txtTimKiem;
    @FXML
    private JFXTreeTableView tblPhongBan;
    @FXML
    private TreeTableColumn colMaPhongBan, colTenPhongBan, colSoDienThoai, colMoTa;
    @FXML
    private JFXComboBox<String> cbTimKiem;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        try {
            this.phongBanDAOImpl = new PhongBanDAOImpl();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        cbTimKiem.getItems().addAll("Mã phòng ban", "Tên phòng ban", "Số điện thoại");
        cbTimKiem.setValue("Mã phòng ban");
        // init table
        colMaPhongBan.setCellValueFactory(new TreeItemPropertyValueFactory<PhongBan, String>("maPhongBan"));
        colTenPhongBan.setCellValueFactory(new TreeItemPropertyValueFactory<PhongBan, String>("tenPhongBan"));
        colSoDienThoai.setCellValueFactory(new TreeItemPropertyValueFactory<PhongBan, String>("soDienThoai"));
        colMoTa.setCellValueFactory(new TreeItemPropertyValueFactory<PhongBan, String>("moTa"));

        try {
            phongBanFindResult = phongBanDAOImpl.findAll(0, 20);
            if (phongBanFindResult != null) {
                pagination.setPageCount(phongBanFindResult.getTotalPage());
                pagination.setPageFactory(this::createpage);
            } else {
                pagination.setPageCount(1);
            }
            if (phongBanFindResult.getListResult().isEmpty()) {
                pagination.setPageCount(1);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        //updateDataTable(null);
    }

    // region Event

    /**
     * Phương thức thêm phòng ban mới
     *
     * @param actionEvent
     */
    @FXML
    public void onThemPhongBan(ActionEvent actionEvent) {
        modifierPhongBan(actionEvent, false);
    }

    /**
     * Phương thức xóa phòng ban
     *
     * @param actionEvent
     */
    @FXML
    public void onXoaPhongBan(ActionEvent actionEvent) throws DAOException {
        PhongBan phongBan = getItemSelected();
//        this.phongBanDAOImpl.delete(phongBan);
//        this.phongBans.remove(phongBan);

        int soluong = phongBanDAOImpl.tongSoNhanVien(phongBan.getMaPhongBan());
        if (soluong > 0) {
            alertController.alertError("Phòng ban này đang có nhân viên không xóa được");
            return;
        }
        if (phongBan != null) {
            stackPane.toFront();
            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            dialogLayout.setHeading(new Text("Cảnh báo"));
            dialogLayout.setBody(new Text(String.format("Bạn có muốn xóa %s này không ?", phongBan.getTenPhongBan())));
            JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

            JFXButton btn_Ok = new JFXButton();
            btn_Ok.setText("Đồng ý");
            btn_Ok.setOnAction(event -> {
                try {
                    this.phongBanDAOImpl.delete(phongBan);
                    this.phongBans.remove(phongBan);
                    updateDataTable(this.phongBans);
                } catch (DAOException e) {
                    alertController.alertError(e.getMessage());
                }
                dialog.close();
            });
            JFXButton btn_Cancel = new JFXButton();
            btn_Cancel.setText("Huỷ");
            btn_Cancel.setOnAction(e -> dialog.close());
            dialogLayout.setActions(btn_Ok, btn_Cancel);

            dialog.show();
        }
    }

    /**
     * Phương thức cập nhật thông tin phòng ban
     *
     * @param actionEvent
     */
    @FXML
    public void onSuaPhongBan(ActionEvent actionEvent) {
        modifierPhongBan(actionEvent, true);
    }

    /**
     * Phương thức xem chi tiết phòng ban
     * @param actionEvent
     */

    /**
     * Phương thức tìm kiếm phòng ban
     *
     * @param actionEvent
     */
    @FXML
    public void onTimKiem(ActionEvent actionEvent) {
        String keyword = txtTimKiem.getText().trim();
        List<PhongBan> phongBans = null;

        try {
            if (keyword.isEmpty()) {
//                phongBans = phongBanDAOImpl.find();
                phongBanFindResult = phongBanDAOImpl.findAll(1, 20);
                pagination.setPageCount(phongBanFindResult.getTotalPage());
                pagination.setPageFactory(this::createpage);
            }
//            else if(cbTimKiem.getValue().equals("Tất cả")){
//                phongBanFindResult=phongBanDAOImpl.findAll(1,20);
//                pagination.setPageCount(phongBanFindResult.getTotalPage());
//                pagination.setPageFactory(this::createpage);
//            }
            else if (cbTimKiem.getValue().equals("Mã phòng ban")) {
                phongBans = new ArrayList<>();
                PhongBan phongBan = phongBanDAOImpl.find(keyword);
                if (phongBan != null) phongBans.add(phongBan);
                if (phongBans.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy phòng ban");
                } else {
                    ObservableList<PhongBan> phongBanObservableList = FXCollections.observableList(phongBans);
                    updateDataTable(phongBanObservableList);
                }
            } else if (cbTimKiem.getValue().equals("Tên phòng ban")) {
//                phongBans = phongBanDAOImpl.findByTenPhongBan(keyword);
                phongBanFindResult = phongBanDAOImpl.findByTenPhongBan(txtTimKiem.getText(), 1, 20);
                if (phongBanFindResult == null) {
                    alertController.alertInfo("Không tìm thấy phòng ban");
                } else {
                    if (phongBanFindResult.getListResult().isEmpty()) {
                        pagination.setPageCount(1);
                    } else {
                        pagination.setPageCount(phongBanFindResult.getTotalPage());
                    }
                    pagination.setPageFactory(this::createPageByName);
                }

            } else if (cbTimKiem.getValue().equals("Số điện thoại")) {
                phongBanFindResult = phongBanDAOImpl.findBySoDienThoai(txtTimKiem.getText(), 1, 20);
                if (phongBanFindResult == null) {
                    alertController.alertInfo("Không tìm thấy phòng ban");
                } else {
                    if (phongBanFindResult.getListResult().isEmpty()) {
                        pagination.setPageCount(1);
                    } else {
                        pagination.setPageCount(phongBanFindResult.getTotalPage());
                    }
                    pagination.setPageFactory(this::createPageByPhone);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // endregion

    /**
     * Xử lý màn hình thêm, sửa phòng ban
     *
     * @param actionEvent
     * @param isEdit
     */
    private void modifierPhongBan(ActionEvent actionEvent, boolean isEdit) {
        PhongBan phongBan = null;
        if (isEdit) {
            phongBan = getItemSelected();

            if (phongBan == null) return;
        }

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/ModifierPhongBanScene.fxml"));
            root = fxmlLoader.load();


            ModifierPhongBanController modifierPhongBanController = fxmlLoader.getController();
            modifierPhongBanController.setPhongBans(this.phongBans);


            if (isEdit) {
                modifierPhongBanController.setPhongBanSelected(phongBan, isEdit, false);
                modifierPhongBanController.setTblPhongBan(tblPhongBan);
            } else {
                modifierPhongBanController.setPaginationPhongBan(pagination);
            }

            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 603, 600);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức cập nhật data table
     *
     * @param phongBans
     */
    public void updateDataTable(ObservableList<PhongBan> phongBans) {
//        this.phongBans = (phongBans == null) ? FXCollections.observableArrayList(this.phongBanDAOImpl.find()) : phongBans;
        this.phongBans = phongBans;
        TreeItem<PhongBan> phongBanTreeItem = new RecursiveTreeItem<>(this.phongBans, RecursiveTreeObject::getChildren);

        if (tblPhongBan.getRoot() != null) {
            tblPhongBan.getRoot().getChildren().clear();
        }
        tblPhongBan.setRoot(phongBanTreeItem);
        tblPhongBan.setShowRoot(false);
    }

    /**
     * Phương thức lấy phong ban duoc chon
     *
     * @return PhongBan
     */
    private PhongBan getItemSelected() {
        TreeTableViewSelectionModel<PhongBan> phongBanTreeTableViewSelectionModel = tblPhongBan.getSelectionModel();

        if (phongBanTreeTableViewSelectionModel.isEmpty()) {
            alertController.alertError("Vui lòng chọn một phòng ban");
            return null;
        }

        int rowIndex = phongBanTreeTableViewSelectionModel.getSelectedIndex();
        return phongBanTreeTableViewSelectionModel.getModelItem(rowIndex).getValue();
    }

    /**
     * Phương thức mở scene tìm kiếm nâng cao
     *
     * @return
     */
    @FXML
    private void onOpenTimKiemNangCao(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/TimKiemNangCaoScene.fxml"));
            root = fxmlLoader.load();

            scene = new Scene(root, 600, 400);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onXemChiTiet(ActionEvent actionEvent) {
        PhongBan phongBan = getItemSelected();

        if (phongBan == null) return;

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/popup/XemCTPhongBanScene.fxml"));
            root = fxmlLoader.load();

            ModifierPhongBanController modifierPhongBanController = fxmlLoader.getController();
            modifierPhongBanController.setPhongBanSelected(phongBan, false, true);


            root.getStylesheets().add(getClass().getResource("/css/MainScene.css").toExternalForm());
            scene = new Scene(root, 900, 700);
            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Node createpage(int pageIndex) {
        try {
            phongBanFindResult = phongBanDAOImpl.findAll(pageIndex, 20);
            phongBans = FXCollections.observableArrayList(phongBanFindResult.getListResult());
            TreeItem<PhongBan> phongBanTreeItem = new RecursiveTreeItem<>(this.phongBans, RecursiveTreeObject::getChildren);
            if (tblPhongBan.getRoot() != null) {
                tblPhongBan.getRoot().getChildren().clear();
            }
            tblPhongBan.setRoot(phongBanTreeItem);
            tblPhongBan.setShowRoot(false);

        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblPhongBan;
    }

    private Node createPageByName(int pageIndex) {
        try {
            String timkiem = txtTimKiem.getText();
            phongBanFindResult = phongBanDAOImpl.findByTenPhongBan(timkiem, pageIndex, 20);
            phongBans = FXCollections.observableArrayList(phongBanFindResult.getListResult());
            TreeItem<PhongBan> phongBanTreeItem = new RecursiveTreeItem<>(this.phongBans, RecursiveTreeObject::getChildren);
            if (tblPhongBan.getRoot() != null) {
                tblPhongBan.getRoot().getChildren().clear();
            }
            tblPhongBan.setRoot(phongBanTreeItem);
            tblPhongBan.setShowRoot(false);

        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblPhongBan;
    }

    private Node createPageByPhone(int pageIndex) {
        try {
            String timkiem = txtTimKiem.getText();
            phongBanFindResult = phongBanDAOImpl.findBySoDienThoai(timkiem, pageIndex, 20);
            phongBans = FXCollections.observableArrayList(phongBanFindResult.getListResult());
            TreeItem<PhongBan> phongBanTreeItem = new RecursiveTreeItem<>(this.phongBans, RecursiveTreeObject::getChildren);
            if (tblPhongBan.getRoot() != null) {
                tblPhongBan.getRoot().getChildren().clear();
            }
            tblPhongBan.setRoot(phongBanTreeItem);
            tblPhongBan.setShowRoot(false);

        } catch (DAOException e) {
            e.printStackTrace();
        }
        return tblPhongBan;
    }
}

