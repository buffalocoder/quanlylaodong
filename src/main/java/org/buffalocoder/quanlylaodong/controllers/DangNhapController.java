package org.buffalocoder.quanlylaodong.controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlylaodong.daos.NhanVienDAOImpl;
import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.DAOException;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;
import org.buffalocoder.quanlylaodong.interfaces.INhanVienDAO;
import org.buffalocoder.quanlylaodong.utils.StorageAccount;
import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class: LoginController
 *
 * @author: Đặng Lê Minh Trường, Nguyễn Chí Cường,Phạm Minh Khoa
 * @version: 1.1
 * @description: Dùng để giao tiếp giữa các class giao diện với các lớp DAO
 * @since : 05-10-2019
 */
public class DangNhapController implements Initializable {
    double x, y;
    @FXML
    private JFXTextField txtTaiKhoan;
    @FXML
    private JFXPasswordField txtMatKhau;
    @FXML
    private StackPane pane_Login;

    // region Event
    @FXML
    private void handleClose(MouseEvent event) {
        AlertController alertController = new AlertController(pane_Login);
        alertController.alertYesNo("Thoát", "Bạn có chắc là muốn thoát chương trình?");
    }

    @FXML
    private void onDangNhap(ActionEvent event) throws IOException, NotExistException, DAOException {

        ////////////////////////////////////////////////////////////////////////////
        if (txtTaiKhoan.getText().equals("admin") && txtMatKhau.getText().equals("123")) {
            try {
                Node node = (Node) event.getSource();
                Stage stage = (Stage) node.getScene().getWindow();
                stage.close();
                openMainScene(stage);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        INhanVienDAO nhanVienDAO = new NhanVienDAOImpl();

        NhanVien nhanVien = nhanVienDAO.find(txtTaiKhoan.getText());
        System.out.println(nhanVien);
        nhanVien.getMatKhau();
        String pass = "123456789";
        if (BCrypt.checkpw(txtMatKhau.getText(), nhanVien.getMatKhau())) {
            System.out.println("mã đúng");
        }
        AlertController alertController = new AlertController(pane_Login);

        if (!txtTaiKhoan.getText().equals(nhanVien.getMaNhanVien())) {
            alertController.alertError("Tài khoản không tồn tại");
            return;
        }
        if (txtTaiKhoan.getText().equals(nhanVien.getMaNhanVien()) && !BCrypt.checkpw(txtMatKhau.getText(), nhanVien.getMatKhau())) {
            alertController.alertError("Sai mật khẩu");
            return;
        }


        if (txtTaiKhoan.getText().equals(nhanVien.getMaNhanVien()) && BCrypt.checkpw(txtMatKhau.getText(), nhanVien.getMatKhau())) {
            StorageAccount.getInstance().setNhanVien(nhanVien);
            try {
                Node node = (Node) event.getSource();
                Stage stage = (Stage) node.getScene().getWindow();
                stage.close();
                openMainScene(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    private void draged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }
    // endregion

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    @FXML
    private AnchorPane paneMain;

    private void openMainScene(Stage stage) throws IOException {
        // todo mở đăng nhập
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/MainScene.fxml"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        root.getStylesheets().add(getClass().getResource("/css/DangNhapScene.css").toExternalForm());
        Scene scene = new Scene(root, 1500, 800);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Phần mền quản lý lao động Hòa Bình");
        stage.getIcons().add(new Image("image/logo.jpg"));
//        stage.setResizable(false);
        stage.setMaximized(true);
        stage.show();
    }

    private NhanVien dangNhap(String maNhanVien, String matKhau) throws NotExistException, DAOException {
        INhanVienDAO nhanVienDAO = new NhanVienDAOImpl();
        return nhanVienDAO.find(maNhanVien);
    }

    private boolean validate(String maNhanVien, String matKhau) {
        return false;
    }
}
