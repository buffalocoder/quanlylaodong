package org.buffalocoder.quanlylaodong.constant;

public class PatternIdConst {
    public static final String PREFIX_NHAN_VIEN = "NV";
    public static final String PREFIX_PHONG_BAN = "PB";
    public static final String PREFIX_CONG_TRINH = "CT";
    public static final String PREFIX_CONG_VIEC = "CV";
}
