/**
 * Lưu trữ tên các bảng trong DB
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 05-10-2019
 */

package org.buffalocoder.quanlylaodong.constant;

public class TableNameConst {
    public static final String CONG_TRINH = "CONGTRINH";
    public static final String NHAN_VIEN = "NHANVIEN";
    public static final String PHONG_BAN = "PHONGBAN";
    public static final String BANG_PHAN_CONG = "BANGPHANCONG";
    public static final String BANG_CONG_VIEC = "BANGCONGVIEC";
}
