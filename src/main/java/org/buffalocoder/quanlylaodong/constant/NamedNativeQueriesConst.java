/**
 * Lưu trữ tên các native query
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.2
 * @since : 05-10-2019
 */

package org.buffalocoder.quanlylaodong.constant;

public class NamedNativeQueriesConst {
    // region Phòng ban
    public static final String PHONGBAN_FIND_LAST_ITEM = "PHONGBAN_FIND_LAST_ITEM";
    public static final String PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN = "PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN";
    public static final String PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN_COUNT = "PHONGBAN_FIND_DANHSACHNHANVIEN_PHONGBAN_COUNT";
    public static final String PHONGBAN_FIND_DANHSACHNHANVIEN_COUNT_ALL = "PHONGBAN_FIND_DANHSACHNHANVIEN_COUNT_ALL";
    public static final String PHONGBAN_FIND_DANHSACHNHANVIEN_GIOITINH_COUNT = "PHONGBAN_FIND_DANHSACHNHANVIEN_COUNT_NAM";
    public static final String PHONGBAN_FIND_ALL_NO_OFFSET = "PHONGBAN_FIND_ALL_NO_OFFSET";
    public static final String PHONGBAN_FIND_ALL = "PHONGBAN_FIND_ALL";
    public static final String PHONGBAN_FIND_ALL_COUNT = "PHONGBAN_FIND_ALL_COUNT";
    public static final String PHONGBAN_FIND_BY_TEN = "PHONGBAN_FIND_BY_TEN";
    public static final String PHONGBAN_FIND_BY_TEN_COUNT = "PHONGBAN_FIND_BY_TEN_COUNT";
    public static final String PHONGBAN_FIND_BY_SODIENTHOAI = "PHONGBAN_FIND_BY_SODIENTHOAI";
    public static final String PHONGBAN_FIND_BY_SODIENTHOAI_COUNT = "PHONGBAN_FIND_BY_SODIENTHOAI_COUNT";
    // endregion

    // region Công trình
    public static final String CONGTRINH_FIND_LAST_ITEM = "CONGTRINH_FIND_LAST_ITEM";
    public static final String CONGTRINH_FIND_ALL_NO_OFFSET = "CONGTRINH_FIND_ALL_NO_OFFSET";
    public static final String CONGTRINH_FIND_ALL = "CONGTRINH_FIND_ALL";
    public static final String CONGTRINH_FIND_ALL_COUNT = "CONGTRINH_FIND_ALL_COUNT";
    public static final String CONGTRINH_FIND_BY_TEN = "CONGTRINH_FIND_BY_TEN";
    public static final String CONGTRINH_FIND_BY_TEN_COUNT = "CONGTRINH_FIND_BY_TEN_COUNT";
    public static final String CONGTRINH_FIND_BY_TRANGTHAI = "CONGTRINH_FIND_BY_TRANGTHAI";
    public static final String CONGTRINH_FIND_BY_TRANGTHAI_COUNT = "CONGTRINH_FIND_BY_TRANGTHAI_COUNT";
    public static final String CONGTRINH_FIND_BY_LOAICONGTRINH = "CONGTRINH_FIND_BY_LOAICONGTRINH";
    public static final String CONGTRINH_FIND_BY_LOAICONGTRINH_COUNT = "CONGTRINH_FIND_BY_LOAICONGTRINH_COUNT";
    public static final String CONGTRINH_FIND_DANHSACHTREHAN = "CONGTRINH_FIND_DANHSACHTREHAN";
    public static final String CONGTRINH_FIND_DANHSACHTREHAN_COUNT = "CONGTRINH_FIND_DANHSACHTREHAN_COUNT";
    public static final String CONGTRINH_FIND_DANHSACHHOANTHANH = "CONGTRINH_FIND_DANHSACHHOANTHANH";
    public static final String CONGTRINH_FIND_DANHSACHHOANTHANH_COUNT = "CONGTRINH_FIND_DANHSACHHOANTHANH_COUNT";
    public static final String CONGTRINH_FIND_DANHSACHDANGTHICONG = "CONGTRINH_FIND_DANHSACHDANGTHICONG";
    public static final String CONGTRINH_FIND_DANHSACHDANGTHICONG_COUNT = "CONGTRINH_FIND_DANHSACHDANGTHICONG_COUNT";
    public static final String CONGTRINH_THONGKE_TINHTRANG_COUNT = "CONGTRINH_THONGKE_TINHTRANG_COUNT";
    public static final String CONGTRINH_THONGKE_TONGCHIPHI = "CONGTRINH_THONGKE_TONGCHIPHI";
    // endregion

    // region Nhân viên
    public static final String NHANVIEN_FIND_LAST_ITEM = "NHANVIEN_FIND_LAST_ITEM";
    public static final String NHANVIEN_FIND_ALL = "NHANVIEN_FIND_ALL";
    public static final String NHANVIEN_FIND_ALL_COUNT = "NHANVIEN_FIND_ALL_COUNT";
    public static final String NHANVIEN_FIND_BY_HOTEN = "NHANVIEN_FIND_BY_TEN";
    public static final String NHANVIEN_FIND_BY_HOTEN_COUNT = "NHANVIEN_FIND_BY_HOTEN_COUNT";
    public static final String NHANVIEN_FIND_BY_CMND = "NHANVIEN_FIND_BY_CMND";
    public static final String NHANVIEN_FIND_BY_CMND_COUNT = "NHANVIEN_FIND_BY_CMND_COUNT";
    public static final String NHANVIEN_FIND_BY_SDT = "NHANVIEN_FIND_BY_SDT";
    public static final String NHANVIEN_FIND_BY_SDT_COUNT = "NHANVIEN_FIND_BY_SDT_COUNT";
    public static final String NHANVIEN_FIND_BY_TRANGTHAI = "NHANVIEN_FIND_BY_TRANGTHAI";
    public static final String NHANVIEN_FIND_BY_TRANGTHAI_COUNT = "NHANVIEN_FIND_BY_TRANGTHAI_COUNT";
    public static final String NHANVIEN_FIND_ALL_OF_CONGTRINH = "NHANVIEN_FIND_ALL_OF_CONGTRINH";
    public static final String NHANVIEN_FIND_ALL_XAYDUNG_OF_CONGTRINH = "NHANVIEN_FIND_ALL_XAYDUNG_OF_CONGTRINH";
    // endregion

    // region Công việc
    public static final String CONGVIEC_FIND_LAST_ITEM = "CONGVIEC_FIND_LAST_ITEM";
    public static final String CONGVIEC_FIND_ALL_NO_OFFSET = "CONGVIEC_FIND_ALL_NO_OFFSET";
    public static final String CONGVIEC_FIND_ALL = "CONGVIEC_FIND_ALL";
    public static final String CONGVIEC_FIND_ALL_COUNT = "CONGVIEC_FIND_ALL_COUNT";
    public static final String CONGVIEC_FIND_BY_CONGTRINH = "CONGVIEC_FIND_BY_CONGTRINH";
    public static final String CONGVIEC_FIND_ALL_BY_CONGTRINH = "CONGVIEC_FIND_ALL_BY_CONGTRINH";
    public static final String CONGVIEC_FIND_BY_CONGTRINH_COUNT = "CONGVIEC_FIND_BY_CONGTRINH_COUNT";
    public static final String CONGVIEC_FIND_BY_PHONGBAN = "CONGVIEC_FIND_BY_PHONGBAN";
    public static final String CONGVIEC_FIND_BY_PHONGBAN_COUNT = "CONGVIEC_FIND_BY_PHONGBAN_COUNT";
    public static final String CONGVIEC_TONG_CHI_PHI = "CONGVIEC_TONG_CHI_PHI";
    // endregion

    // region Phân công
    public static final String PHANCONG_FIND_CHUATHAMGIA = "PHANCONG_FIND_CHUATHAMGIA";
    public static final String PHANCONG_FIND_CHUATHAMGIA_COUNT = "PHANCONG_FIND_CHUATHAMGIA_COUNT";
    public static final String PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN = "PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN";
    public static final String PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN_COUNT = "PHANCONG_FIND_CHUATHAMGIA_BY_PHONGBAN_COUNT";
    public static final String PHANCONG_FIND_DATHAMGIA = "PHANCONG_FIND_DATHAMGIA";
    public static final String PHANCONG_FIND_DATHAMGIA_COUNT = "PHANCONG_FIND_DATHAMGIA_COUNT";
    public static final String PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC = "PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC";
    public static final String PHANCONG_FIND_DATHAMGIA_BY_CONGVIEC_COUNT = "PHANCONG_FIND_DATHAMGIA_BY_CO NGVIEC_COUNT";
    public static final String PHANCONG_FIND_CONGTRINHDANGTHAMGIA = "PHANCONG_FIND_CONGTRINHDANGTHAMGIA";
    public static final String PHANCONG_FIND_CONGTRINHDANGTHAMGIA_COUNT = "PHANCONG_FIND_CONGTRINHDANGTHAMGIA_COUNT";
    public static final String PHANCONG_DELETE_BY_NHANVIEN_CONGTRINH = "PHANCONG_DELETE_BY_NHANVIEN_CONGTRINH";
    public static final String PHANCONG_FIND_BY_MACONGTRINH = "PHANCONG_FIND_BY_MACONGTRINH";
    public static final String PHANCONG_FIND_DANHSACHNHANVIEN = "PHANCONG_FIND_DANHSACHNHANVIEN";
    public static final String PHANCONG_DELETE_BY_MACONGVIEC_MANHANVIEN = "PHANCONG_DELETE_BY_MACONGVIEC_MANHANVIEN";
    public static final String PHANCONG_FIND_BY_MACONGVIEC_MANHANVIEN = "PHANCONG_FIND_BY_MACONGVIEC_MANHANVIEN";
    public static final String PHANCONG_FIND_DANHSACH_BY_CONGVIEC = "PHANCONG_FIND_DANHSACH_BY_CONGVIEC";
    // endregion
}
