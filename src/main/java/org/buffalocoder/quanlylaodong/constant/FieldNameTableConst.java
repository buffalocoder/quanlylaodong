/**
 * Lưu trữ tên các trường trong DB
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 05-10-2019
 */

package org.buffalocoder.quanlylaodong.constant;

public class FieldNameTableConst {
    // region Phòng ban
    public static final String PHONGBAN_MAPHONGBAN = "MAPHONGBAN";
    public static final String PHONGBAN_TENPHONGBAN = "TENPHONGBAN";
    public static final String PHONGBAN_MOTA = "MOTA";
    public static final String PHONGBAN_SODIENTHOAI = "SODIENTHOAI";
    // endregion

    // region Công trình
    public static final String CONGTRINH_MACONGTRINH = "MACONGTRINH";
    public static final String CONGTRINH_NGAYCAPGIAYPHEP = "NGAYCAPGIAYPHEP";
    public static final String CONGTRINH_NGAYDUKIENHOANTHANH = "NGAYDUKIENHOANTHANH";
    public static final String CONGTRINH_NGAYTHICONG = "NGAYTHICONG";
    public static final String CONGTRINH_NGAYHOANTHANH = "NGAYHOANTHANH";
    public static final String CONGTRINH_TENCONGTRINH = "TENCONGTRINH";
    public static final String CONGTRINH_LOAICONGTRINH = "LOAICONGTRINH";
    public static final String CONGTRINH_TRANGTHAI = "TRANGTHAI";
    public static final String CONGTRINH_DIACHI = "DIACHI";
    // endregion

    // region Nhân viên
    public static final String NHANVIEN_MANHANVIEN = "MANHANVIEN";
    public static final String NHANVIEN_ANHCANHAN = "ANHCANHAN";
    public static final String NHANVIEN_CMND = "CMND";
    public static final String NHANVIEN_DANTOC = "DANTOC";
    public static final String NHANVIEN_GIOITINH = "GIOITINH";
    public static final String NHANVIEN_HOTEN = "HOTEN";
    public static final String NHANVIEN_MATKHAU = "MATKHAU";
    public static final String NHANVIEN_NGAYSINH = "NGAYSINH";
    public static final String NHANVIEN_SODIENTHOAI = "SODIENTHOAI";
    public static final String NHANVIEN_DIACHI = "DIACHI";
    public static final String NHANVIEN_TRANGTHAI = "TRANGTHAI";
    // endregion

    // region Phân công
    public static final String PHANCONG_NGAYBATDAU = "NGAYBATDAU";
    public static final String PHANCONG_NGAYKETTHUC = "NGAYKETTHUC";
    // endregion

    // region Công việc
    public static final String CONGVIEC_MACONGVIEC = "MACONGVIEC";
    public static final String CONGVIEC_TENCONGVIEC = "TENCONGVIEC";
    public static final String CONGVIEC_SOLUONGNHANVIENTOIDA = "SOLUONGNHANVIENTOIDA";
    public static final String CONGVIEC_CHIPHI = "CHIPHI";
    public static final String CONGVIEC_NGAYDUKIENHOANTHANH = "NGAYDUKIENHOANTHANH";
    public static final String CONGVIEC_NGAYHOANTHANH = "NGAYHOANTHANH";
    // endregion

    // region Địa chỉ
    public static final String DIACHI_DIACHICHITIET = "DIACHICHITIET";
    public static final String DIACHI_KHUVUC = "KHUVUC";
    public static final String DIACHI_QUANHUYEN = "QUANHUYEN";
    public static final String DIACHI_TINHTHANHPHO = "TINHTHANHPHO";
    // endregion
}
