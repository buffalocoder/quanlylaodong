package org.buffalocoder.quanlylaodong.utils;

import org.buffalocoder.quanlylaodong.exceptions.DAOException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

/**
 * Đối tượng khởi tạo JPA Persistence
 * Sử dụng Design Pattern Singleton synchronized Thread
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 05-10-2019
 */
public class JpaPersistenceHelper {
    private final String DB_USER = "DB_USER";
    private final String DB_PASSWORD = "DB_PASSWORD";

    private static JpaPersistenceHelper _instance;
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    private JpaPersistenceHelper() throws DAOException {
        // Override config persistence
        Map<String, String> env = System.getenv();
        Map<String, Object> configOverrides = new HashMap<String, Object>();
        for (String envName : env.keySet()) {
            if (envName.contains(DB_USER)) {
                configOverrides.put("javax.persistence.jdbc.user", env.get(envName));
            } else if (envName.contains(DB_PASSWORD)) {
                configOverrides.put("javax.persistence.jdbc.password", env.get(envName));
            }
        }

        try {
            this.entityManager = Persistence.createEntityManagerFactory("QuanLyLaoDong", configOverrides)
                    .createEntityManager();
        } catch (Exception e) {
            throw new DAOException("Không thể kết nối đến database");
        }

    }

    public static JpaPersistenceHelper getInstance() throws DAOException {
        if (_instance == null) {
            synchronized (JpaPersistenceHelper.class) {
                if (null == _instance) {
                    _instance = new JpaPersistenceHelper();
                }
            }
        }
        return _instance;
    }
}
