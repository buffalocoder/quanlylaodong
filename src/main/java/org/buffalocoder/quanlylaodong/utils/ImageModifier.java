package org.buffalocoder.quanlylaodong.utils;

import javafx.scene.image.Image;
import org.buffalocoder.quanlylaodong.Main;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class ImageModifier {
    private static final String imageDirectory = "/images";
    private static final String workingDirectory = "C:/QuanLyLaoDong";

    private static ImageModifier _instance;

    private ImageModifier() throws Exception {
        File dirPath = new File(workingDirectory + imageDirectory);

        if (!dirPath.exists()) {
            boolean created = dirPath.mkdir();

            if (!created)
                throw new Exception("Can't create directory image");
        }
    }

    public static ImageModifier getInstance() throws Exception {
        if (_instance == null) {
            synchronized (ImageModifier.class) {
                if (null == _instance) {
                    _instance = new ImageModifier();
                }
            }
        }
        return _instance;
    }

    public Image read(String fileName) throws NotExistException, MalformedURLException {
        String filePath = String.format("%s%s/%s.jpg", workingDirectory, imageDirectory, fileName);
        File file = new File(filePath);
        System.out.println(filePath);
        Image img = new Image(file.toURI().toURL().toExternalForm());
        return img;
    }

    public int getLenthPathImageDirectory() throws Exception {
        return new File(workingDirectory + imageDirectory).getPath().length();
    }

    public boolean write(BufferedImage image, String fileName) throws Exception {
        String filePath = String.format("%s%s/%s.jpg", workingDirectory, imageDirectory, fileName);

        try {
            File file = new File(filePath);
            return ImageIO.write(image, "png", file);
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception(String.format("Không thể lưu file %s.jpg", fileName));
        }
    }
}
