package org.buffalocoder.quanlylaodong.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class dùng để kiểm tra dữ liệu đầu vào
 *
 * @author: Nguyễn Chí Cường
 * @version: 1.0
 * @since : 05-10-2019
 */
public class Validate {
    /**
     * Phương thức kiểm tra email
     *
     * @param email
     * @return boolean
     */
    public static boolean isEmail(String email) {
        String regex = "^((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Phương thức kiểm tra mã theo regex
     *
     * @param id
     * @param prefix
     * @return boolean
     */
    public static boolean isPatternId(String id, String prefix) {
        String regex = String.format("^%s\\d{%d}$", prefix, 4);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(id);
        return matcher.matches();
    }

    /**
     * Phương thức kiểm tra chứng minh nhân dân
     *
     * @param cmnd
     * @return boolean
     */
    public static boolean isCMND(String cmnd) {
        String regex = "^\\d{12}|\\d{9}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(cmnd);
        return matcher.matches();
    }

    /**
     * Phương thức kiểm tra số điện thoại
     *
     * @param soDienThoai
     * @return boolean
     */
    public static boolean isSoDienThoai(String soDienThoai) {
        String regex = "^0\\d{9}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(soDienThoai);
        return matcher.matches();
    }

    public static boolean isEmpty(String str) {
        return str.trim().isEmpty();
    }
}
