package org.buffalocoder.quanlylaodong.utils;

import org.buffalocoder.quanlylaodong.entities.NhanVien;
import org.buffalocoder.quanlylaodong.exceptions.NotExistException;

/**
 * @author: Đặng Lê Minh Trường
 * @version: 1.0
 */
public class StorageAccount {
    private static StorageAccount _instance;
    private static NhanVien nhanVien;

    private StorageAccount() {
    }

    public static StorageAccount getInstance() {
        if (_instance == null) {
            synchronized (StorageAccount.class) {
                if (null == _instance) {
                    _instance = new StorageAccount();
                }
            }
        }

        return _instance;
    }

    public void setNhanVien(NhanVien nhanVien1) {
        nhanVien = nhanVien1;
    }

    public NhanVien getNhanVien() throws NotExistException {
        if (nhanVien == null) {
            throw new NotExistException("Vui lòng đăng nhập");
        }
        return nhanVien;
    }
}
