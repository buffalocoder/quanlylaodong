package org.buffalocoder.quanlylaodong.types;

public enum TrangThaiCongTrinh {
    DANG_THI_CONG,
    HOAN_THANH,
    TRE_HAN,
    TAM_DUNG
}
