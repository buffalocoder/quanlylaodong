package org.buffalocoder.quanlylaodong.views;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Class: NhanVienController
 *
 * @author: Đặng Lê Minh Trường
 * @version: 1.0
 * @description: Mở giao diện đăng nhập
 * @since : 05-10-2019
 */
public class LoginScene extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/DangNhapScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/DangNhapScene.css").toExternalForm());
            Scene scene = new Scene(root, 855, 400);
            primaryStage.setScene(scene);
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
